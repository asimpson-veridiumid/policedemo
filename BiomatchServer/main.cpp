//
//  main.cpp
//  BiomatchServer
//
//  Created by Andrew Simpson on 09/11/2016.
//  Copyright © 2016 aiusepsi. All rights reserved.
//

#include <iostream>
#include <grpc++/grpc++.h>
#include <dlfcn.h>
#include <biomatch_api.h>
#include <string>
#include <memory>
#include <thread>
#include <fstream>

#include "biomatch.pb.h"
#include "biomatch.grpc.pb.h"
#include "MappedFileData.h"

class MatchingLib
{
public:
    
    static std::unique_ptr<MatchingLib> loadLib(const char* lib_path)
    {
        std::unique_ptr<MatchingLib> lib_ptr(new MatchingLib());
        MatchingLib& lib = *lib_ptr;
        
        lib.lib_handle = dlopen(lib_path, RTLD_NOW);
        
        if(lib.lib_handle == nullptr)
        {
            std::cerr << "Couldn't load library: " << lib_path << std::endl;
            return nullptr;
        }
        
        lib.init_callback = reinterpret_cast<decltype(lib.init_callback)>(dlsym(lib.lib_handle, "init_callback"));
        
        if(lib.init_callback == nullptr)
        {
            std::cerr << "init_callback function missing!" << std::endl;
            return nullptr;
        }
        
        lib.bio_lib_describe = reinterpret_cast<decltype(lib.bio_lib_describe)>(dlsym(lib.lib_handle, "bio_lib_describe"));
        
        if(lib.bio_lib_describe == nullptr)
        {
            std::cerr << "bio_lib_describe function missing!" << std::endl;
            return nullptr;
        }
        
        lib.bio_match = reinterpret_cast<decltype(lib.bio_match)>(dlsym(lib.lib_handle, "bio_match"));
        
        if(lib.bio_match == nullptr)
        {
            std::cerr << "bio_match function missing!" << std::endl;
            return nullptr;
        }
        
        return lib_ptr;
    }
    
    int (*init_callback)(const char*);
    BioLibInfo (*bio_lib_describe)(void);
    BioMatchRes* (*bio_match)(void*, size_t, void*, size_t);
    
    ~MatchingLib()
    {
        if(lib_handle != nullptr)
        {
            dlclose(lib_handle);
        }
    }
    
private:
    
    void* lib_handle = nullptr;
    
    MatchingLib()
    {
    }
    
    MatchingLib& operator=(const MatchingLib& other) = delete;
    MatchingLib(MatchingLib& other) = delete;
};

class PoliceDemoDatabase
{
public:
    
    PoliceDemoDatabase()
    {
        // Do the database set-up work here
        // Load the database protocol buffers file
        MappedFileData db_src("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/db.bin");
        db.ParseFromArray(db_src.getData(), db_src.getSize());
    }
    
    std::unique_ptr<Veridium::PoliceDemo::Database_DBEntry> getDBEntry(std::string name) const
    {
        if(db.entries().count(name) == 1)
        {
            const Veridium::PoliceDemo::Database_DBEntry& db_entry = db.entries().at(name);
            return std::unique_ptr<Veridium::PoliceDemo::Database_DBEntry>(new Veridium::PoliceDemo::Database_DBEntry(db_entry));
        }
        else
        {
            std::cerr << "No database entry for name: " << name << std::endl;
            return nullptr;
        }
    }
    
    Veridium::PoliceDemo::Database db;
};

template<typename T>
class SingleItemSlot
{
private:
    std::unique_ptr<T> slot_item;
    std::mutex mtx;
    std::condition_variable pop_cv;
    
public:
    
    SingleItemSlot() : slot_item(nullptr)
    {
    }
    
    /// Push item into the slot. If the slot was already occupied, return false.
    bool push(std::unique_ptr<T>& item)
    {
        std::unique_lock<std::mutex> lock(mtx);
        slot_item.swap(item);
        
        if(item == nullptr)
        {
            lock.unlock();
            pop_cv.notify_one();
            return true;
        }
        else
        {
            std::cerr << "Slot was already full!" << std::endl;
            return false;
        }
    }
    
    std::unique_ptr<T> pop(bool blocking = true)
    {
        // Acquire the lock on the slot.
        std::unique_lock<std::mutex> lock(mtx);
        
        if(slot_item != nullptr)
        {
            std::unique_ptr<T> to_return(nullptr);
            slot_item.swap(to_return);
            return to_return;
        }
        else if(blocking == true)
        {
            pop_cv.wait(lock, [this]{return slot_item != nullptr;});
            std::unique_ptr<T> to_return(nullptr);
            slot_item.swap(to_return);
            return to_return;
        }
        else
        {
            return nullptr;
        }
        
    }

};

class ObserverImpl final : public Veridium::PoliceDemo::Observer::Service
{
public:
    ObserverImpl()
    {
    }
    
    grpc::Status StartObserving(grpc::ServerContext* context, const ::Veridium::PoliceDemo::ObserveRequest* request, grpc::ServerWriter<Veridium::PoliceDemo::Observation>* writer) override
    {
        std::cout << "Observer connected." << std::endl;
        std::unique_ptr<Veridium::PoliceDemo::Observation> observation = nullptr;
        
        do
        {
            observation = queue.pop();
        }
        while(observation != nullptr && writer->Write(*observation));
        
        // Since the write failed, push the observation back into the slot.
        if(observation != nullptr)
        {
            queue.push(observation);
        }
        
        std::cout << "Failed to write to observer. Ending..." << std::endl;
        return grpc::Status::OK;
    }
    
    void NotifyObserver(std::unique_ptr<Veridium::PoliceDemo::Observation>&& observation)
    {
        if(observation != nullptr)
        {
            queue.push(observation);
        }
    }
    
private:
    SingleItemSlot<Veridium::PoliceDemo::Observation> queue;
};

class IdentifierImpl final : public Veridium::PoliceDemo::Identifier::Service
{
public:
    IdentifierImpl(const PoliceDemoDatabase& _db, const MatchingLib& _lib, ObserverImpl& _observer) : db(_db), lib(_lib), observer(_observer)
    {
    }
    
    grpc::Status Identify(grpc::ServerContext* ctx, const Veridium::PoliceDemo::IdentifyRequest* request, Veridium::PoliceDemo::IdentifyResponse* response) override
    {
        std::cout << "Server received IdentifyRequest!" << std::endl;
        std::cout << "Name: " << request->name() << std::endl;
        std::cout << "DOB: " << request->dob() << std::endl;
        
        // Do db lookup:
        std::unique_ptr<Veridium::PoliceDemo::Database_DBEntry> db_entry = db.getDBEntry(request->name());
        
        bool success = false;
        
        if(db_entry != nullptr)
        {
            if(db_entry->dob().compare(request->dob()) == 0)
            {
                if(db_entry->biometric_template_filename().size() > 0)
                {
                    MappedFileData biometric_template(db_entry->biometric_template_filename());
                    
                    void* captured_data = const_cast<void*>(reinterpret_cast<const void*>(request->biometric_template().data()));
                    
                    // Dump captured data to disk
                    /*
                    std::ofstream out("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/biometric.vidb", std::ios::binary);
                    
                    for(char c : request->biometric_template())
                    {
                        out << c;
                    }
                    
                    out.close();
                    */
                    
                    // Do a match!
                    //BioMatchRes* bio_match(const void *captured, size_t captured_len,const void *decrypted_reference, size_t decrypted_len);
                    BioMatchRes* result = lib.bio_match(captured_data, request->biometric_template().size(),
                                                        biometric_template.getData(), biometric_template.getSize());
                    
                    if(result->authorized == 1)
                    {
                        success = true;
                    }
                }
                else
                {
                    std::cerr << "DB entry missing biometric" << std::endl;
                }
            }
            else
            {
                std::cerr << "Wrong DOB" << std::endl;
            }
        }
        else
        {
            std::cerr << "No database entry!" << std::endl;
        }
        
        // Compose response.
        response->set_success(success);
        
        if(success)
        {
            Veridium::PoliceDemo::Image* face_image = response->mutable_face_image();
            face_image->set_type(db_entry->image_type());
            
            MappedFileData img(db_entry->image_filename());
            face_image->set_image_data(img.getData(), img.getSize());
        }
        
        // Notify Observer:
        std::unique_ptr<Veridium::PoliceDemo::Observation> observation(new Veridium::PoliceDemo::Observation());
        observation->set_name(request->name());
        observation->set_dob(request->dob());
        observation->mutable_identify_response()->CopyFrom(*response);
        observer.NotifyObserver(std::move(observation));
    
        return grpc::Status::OK;
    }
    
public:
    const PoliceDemoDatabase& db;
    const MatchingLib& lib;
    ObserverImpl& observer;
};

int main(int argc, const char * argv[]) {
    
    const char* ffid_lib_path = "/Users/andy/hoyos_code/4fid/release/host_Darwin/lib/libffid_dynamic.dylib";
    auto ffid = MatchingLib::loadLib(ffid_lib_path);
    
    if(ffid == nullptr)
    {
        return 1;
    }
    
    ffid->init_callback("");
    
    BioLibInfo bio_lib_info = ffid->bio_lib_describe();
    std::cout << bio_lib_info.name << std::endl;
    
    PoliceDemoDatabase db;
    
    // Initialise the server stuff here
    ObserverImpl observer;
    IdentifierImpl identifier(db, *ffid, observer);
    
    grpc::ServerBuilder builder;
    grpc::SslServerCredentialsOptions ssl_options;
    
    MappedFileData server_key("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/pki/server.key");
    MappedFileData server_cert("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/pki/server.crt");
    MappedFileData root_cert("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/pki/ca.crt");
    
    std::string server_key_string = std::string(reinterpret_cast<char*>(server_key.getData()), server_key.getSize());
    std::string server_cert_string = std::string(reinterpret_cast<char*>(server_cert.getData()), server_cert.getSize());
    std::string root_cert_string = std::string(reinterpret_cast<char*>(root_cert.getData()), root_cert.getSize());
    
    grpc::SslServerCredentialsOptions::PemKeyCertPair pkcp = {server_key_string, server_cert_string};
    ssl_options.pem_key_cert_pairs.push_back(pkcp);
    ssl_options.pem_root_certs = root_cert_string;

    builder.AddListeningPort("0.0.0.0:50051", grpc::SslServerCredentials(ssl_options));
    builder.AddListeningPort("0.0.0.0:50052", grpc::InsecureServerCredentials());
    
    builder.RegisterService(&identifier);
    builder.RegisterService(&observer);
    
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    
    std::cout << "Server listening..." << std::endl;
    server->Wait();
    return 0;
}
