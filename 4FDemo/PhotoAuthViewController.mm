//
//  PhotoAuthViewController.m
//  4F
//
//  Created by razvan on 8/7/15.
//  Copyright (c) 2015 Hoyos Labs. All rights reserved.
//

#import "PhotoAuthViewController.h"

//#include "FFIDMinutiae.h"
//#include "FfidVector.h"
#import <libffid/FlatInterface.h>
//#include "common.h"
#import "OpenCVUtils.h"
//#import "HoyosMatcher.h"

using namespace cv;

@interface PhotoAuthViewController (){
  dataBlock successBlock;
  voidBlock failBlock;
  voidBlock cancelBlock;
  stringBlock errorBlock;
  NSString* welcomeMessage;
  UIBarButtonItem* autoStartBarButton;
}

@property (weak, nonatomic) IBOutlet UIButton *autoStartButton;

@end

@implementation PhotoAuthViewController


-(id)initWithNibName:(NSString *) nibNameOrNil
      welcomeMessage:(NSString*) message
           onSuccess:(dataBlock) successBlk
              onFail:(voidBlock)failBlk
            onCancel:(voidBlock)cancelBlk
             onError:(stringBlock)errorBlk
{
  self = [super initWithNibName:nibNameOrNil bundle:[NSBundle mainBundle]];
  if (self) {
    // Custom initialization
    successBlock = successBlk;
    cancelBlock = cancelBlk;
    errorBlock = errorBlk;
    failBlock = failBlk;
    welcomeMessage = message;
  }
  return self;
}


- (void)viewDidLoad {
  
  
  [super viewDidLoad];
  self.title = Local(@"4F Fingerprint Capture");
  self.navigationItem.hidesBackButton = YES;
  
  
  autoStartBarButton = [[UIBarButtonItem alloc] initWithTitle:Local(@"Autostart") style:UIBarButtonItemStylePlain target:self action:@selector(onAutoStart:)];
  [autoStartBarButton setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12 weight:UIFontWeightLight]} forState:UIControlStateNormal];
  
  if (nil != self.navigationController) {
    self.autoStartButton.hidden = YES;
    self.navigationItem.leftBarButtonItem = autoStartBarButton;
  }
    /*
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        successBlock();
    });
    */
  
}

-(void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.autoStartButton.enabled = NO;
  autoStartBarButton.enabled = NO;
  [self updateAutoStartButton];
}

-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void) updateAutoStartButton {
    BOOL autostart = YES;
  
  if (nil != self.navigationController) {
    autoStartBarButton.title = autostart ? Local(@"Auto start ON"):Local(@"Auto start OFF");
    autoStartBarButton.tintColor = autostart ? [UIColor blackColor] : [UIColor darkGrayColor];
  }else{
    [self.autoStartButton setTitle: autostart?Local(@"Auto start ON"):Local(@"Auto start OFF") forState:UIControlStateNormal];
    [self.autoStartButton setTitleColor:autostart?[UIColor blackColor]:[UIColor darkGrayColor] forState:UIControlStateNormal];
  }
}

- (IBAction)onAutoStart:(id)sender {
  [self updateAutoStartButton];
}

- (IBAction)onCancel:(id)sender {
  [self stopCapturing];
  [self shutdown4F];
    
    if(cancelBlock != nil) {
        cancelBlock();
    }
}

-(void)onInit {
  self.processingImage.image = nil;
  self.messageLabel.text = welcomeMessage;
  
  useLeftHand = true;
  
  self.overlayView.image = self.overlayImage;
  
  self.progressView.leftHand = useLeftHand;
}


-(void)onCameraStarted {
  self.autoStartButton.enabled = YES;
  autoStartBarButton.enabled = YES;
}

-(void) processPhotoHandMat:(FourF::Interface::ImageHandle) hImage withROIs:(const int[16]) rois withLivenessImage: (FourF::Interface::ImageHandle) hImageLiveness {
    
    FourF::Vector v;
    FourF::Interface::ImagedHand hand = useLeftHand ? FourF::Interface::ImagedHand::Left : FourF::Interface::ImagedHand::Right;
    
    auto result = FourF::Interface::getVector(hImage, hand, rois, v);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(result == FourF::GetVectorResult::SUCCESS) {
            if(successBlock != nil) {
                
                std::vector<char> vector_data_std_vec = v.save();
                NSData* vector_data = [NSData dataWithBytes: vector_data_std_vec.data() length: vector_data_std_vec.size()];
                
                successBlock(vector_data);
            }
        } else {
                //[MatcherUtil writePNGImage:self.processingImage.image toFile:[NSString stringWithFormat:@"%@-AuthImage-FAILED-score[%d].png",[dateFormatter stringFromDate:[NSDate date]], raw_score]];
                
            NSLog(@"Get vector failed!" );
            if(errorBlock != nil)
            {
                errorBlock(@"FAILED! Oh, the horror!");
            }
        }
        
        [self stopCapturing];
        [self shutdown4F];
    });
}


@end
