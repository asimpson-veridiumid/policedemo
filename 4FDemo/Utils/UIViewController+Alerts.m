//
//  UIViewController+Alerts.m
//  TempestSDK-Demo
//
//  Created by razvan on 12/11/14.
//  Copyright (c) 2014 Hoyos. All rights reserved.
//

#import "UIViewController+Alerts.h"

@implementation UIViewController (Alerts)


- (UIAlertView*)alert:(NSString*)message withTitle:(NSString*) title {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
    return alert;
}

@end
