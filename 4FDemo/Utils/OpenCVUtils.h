//
//  GlobalShit.h
//  OpenCVTest
//
//  Created by razvan on 11/12/14.
//  Copyright (c) 2014 Hoyos. All rights reserved.
//

#import <opencv2/core.hpp>
using namespace cv;

#pragma mark UIImage Mat Utils

//void DrawROIs(const int* arrayRois, Mat& img);
Mat cvMatFromUIImage(UIImage * image);
UIImage* UIImageFromCVMat(Mat cvMat);

