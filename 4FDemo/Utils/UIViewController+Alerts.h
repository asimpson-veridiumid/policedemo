//
//  UIViewController+Alerts.h
//  TempestSDK-Demo
//
//  Created by razvan on 12/11/14.
//  Copyright (c) 2014 Hoyos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (Alerts)

- (UIAlertView*)alert:(NSString*)message withTitle:(NSString*) title;

@end
