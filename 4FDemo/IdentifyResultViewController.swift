//
//  IdentifyResultViewController.swift
//  4FDemo
//
//  Created by Andrew Simpson on 15/11/2016.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

import UIKit
import PoliceDemoGrpc

@objc class IdentifyResultViewController: UIViewController {
    
    var name: String = ""
    var dob: String = ""
    var vector_data: Data?
    
    let server_address = "192.168.15.10:50052"
    
    @IBOutlet weak var faceImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.leftBarButtonItem = navBar.topItem?.leftBarButtonItem
        
        GRPCCall.useInsecureConnections(forHost: server_address)
        
        let service = VIDPIdentifier(host: server_address)
        let request = VIDPIdentifyRequest()
        
        request.name = name
        request.dob = dob
        
        if vector_data != nil {
            request.biometricTemplate = vector_data
        }
        
        service.identify(with: request, handler: {
            (resp: VIDPIdentifyResponse?, err: Error?) -> Void in
    
            if let response = resp {
                print("Success: \(response.success)")
                
                if response.success {
                    self.nameLabel.text = self.name
                    self.dobLabel.text = self.dob
                    let image = UIImage.init(data: response.faceImage.imageData)
                    self.faceImage.image = image
                    // Somehow present the picture into the image...
                }
                else {
                    self.nameLabel.text = "Unidentified."
                    self.dobLabel.text = ""
                }
            }
            
            if let error = err {
                
                if let nav = self.navigationController {
                    nav.popToRootViewController(animated: true)
                }
                
                let alert = UIAlertController.init(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction.init(title: "Ok", style: .default, handler: { (UIAlertAction) in

                })
                
                alert.addAction(action)
                self.present(alert, animated: true)
            }
            
        })
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func donePress(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
