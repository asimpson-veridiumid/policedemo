//
//  HoyosMatcher.m
//  HoyosMatcher
//
//  Created by Sergiu Vlad on 09/07/14.
//  Copyright (c) 2014 HoyosLabs. All rights reserved.
//

#import "HoyosMatcher.h"
#import "Util.h"
#import "Welcome4FViewController.h"
#import "PhotoAuthViewController.h"
#import "Enroll4FConfirmViewController.h"

@implementation HoyosMatcher

/*
+(void)enroll4FFrom:(UIViewController*)fromVC
              forID:(NSString*)accountID
          onSuccess:(voidBlock) successBlock
           onCancel:(voidBlock) cancelBlock
          onTimeout:(voidBlock) timeoutBlock
            onError:(stringBlock) errorBlock
{
  
  BOOL standAlone = YES;
  
  __block UINavigationController* nav = standAlone ? [[UINavigationController alloc] init] : fromVC.navigationController;
  [nav.navigationBar setTranslucent:YES];
  
  __block Welcome4FViewController* welcomeController;
  __block Enroll4FConfirmViewController* confirmViewController;
  __block PhotoAuthViewController* authController;
  __block PhotoEnrollViewController* enrollController;

  [MatcherUtil deleteEnrollmentDataForID:accountID];
  
  voidBlock overallCancelBlock = ^{
    if (standAlone) {
      [fromVC dismissViewControllerAnimated:YES completion:cancelBlock];
    }else{
      [MatcherUtil safeCallVoidBlock:cancelBlock];
    }
  };
  voidBlock overallTimeoutBlock = ^{
    [MatcherUtil deleteEnrollmentDataForID:accountID];
    if (standAlone) {
      [fromVC dismissViewControllerAnimated:YES completion:timeoutBlock];
    }else{
      [MatcherUtil safeCallVoidBlock:timeoutBlock];
    }
  };
  
  stringBlock overallErrorBlock = ^(NSString* message){
    [MatcherUtil deleteEnrollmentDataForID:accountID];
    if (standAlone) {
      [fromVC dismissViewControllerAnimated:YES completion:^{
        [MatcherUtil safeCallStringBlock:errorBlock withString:message];
      }];
    }else{
      [MatcherUtil safeCallStringBlock:errorBlock withString:message];
    }
  };
  
  voidBlock overallSuccessBlock = ^{
    [MatcherUtil commit4FEnrollmentForID:accountID];
    if (standAlone) {
      [fromVC dismissViewControllerAnimated:YES completion:successBlock];
    }else{
      [MatcherUtil safeCallVoidBlock:successBlock];
    }
  };

  
  enrollController =
  [[PhotoEnrollViewController alloc] initWithNibName:@"Enroll4F"
                                               forID:accountID
                                           onSuccess:^{
                                             [nav pushViewController:confirmViewController animated:YES];
//                                             [nav pushViewController:authController animated:YES];
                                           }
                                           onTimeout:overallTimeoutBlock
                                            onCancel:overallCancelBlock
                                             onError:overallErrorBlock
                           numberOfTemplatesToEnroll:3];
  
  
  welcomeController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  
  
  
  confirmViewController =
  [[Enroll4FConfirmViewController alloc] initWithNibName:@"Enroll4FConfirm"
                                                  onOk:^{
                                                    [nav pushViewController:authController animated:YES];
                                                  }
                                              onRetake:^{
                                                [enrollController reset];
                                                [nav popViewControllerAnimated:YES];
                                              }];
  
    
  
  authController =
  [[PhotoAuthViewController alloc] initWithNibName:@"Auth4F"
                                        forID:accountID
                               welcomeMessage:Local(@"Ok. Let's test it out.")
                                    onSuccess: overallSuccessBlock
                                       onFail:^{
                                         [nav popViewControllerAnimated:YES];
                                         [MatcherUtil alert:Local(@"4F Authentication failed.") withTitle:Local(@"Fail")];
                                       }
                                     onCancel:^{
                                       [nav popViewControllerAnimated:YES];
                                       [MatcherUtil alert:Local(@"4F Authentication cancelled by you.") withTitle:Local(@"Cancelled")];
                                     }
                                      onError:^(NSString* message){
                                        [nav popViewControllerAnimated:YES];
                                        [MatcherUtil alert:Local(@"4F Authentication error.") withTitle:Local(@"Error")];
                                      }];

  
  
  
  if (standAlone) {
    welcomeController =
    [[Welcome4FViewController alloc] initWithNibName:@"Welcome4F"
                                              onOk:^{
                                                [nav pushViewController:enrollController animated:YES];
                                              } onCancel:overallCancelBlock];
    nav.viewControllers = @[welcomeController];
    
    [fromVC presentViewController:nav animated:YES completion:nil];

  }else{
    [nav pushViewController:enrollController animated:YES];
  }
  
  
  
  
}
*/

+(void) authenticateFrom:(UIViewController*) presentingViewController
                   forID:(NSString*)accountID
          welcomeMessage:(NSString*) welcomeMessage
               onSuccess:(voidBlock) successBlock
                  onFail:(voidBlock) failBlock
                onCancel:(voidBlock) cancelBlock
                 onError:(stringBlock) errorBlock
{
  
  BOOL autoDismiss = YES;
  
  if (![self assertAudio:NO andVideo:YES withBlock:errorBlock])
    return;
  
  __block UINavigationController* nav = [[UINavigationController alloc] init];
  [nav.navigationBar setTranslucent:YES];
  
  __block PhotoAuthViewController* auth4FController;
  
  voidBlock overallSuccessBlock = !autoDismiss ? successBlock: ^{
    [presentingViewController dismissViewControllerAnimated:YES completion:successBlock];
  };
  voidBlock overallFailBlock = !autoDismiss ? failBlock: ^{
    [presentingViewController dismissViewControllerAnimated:YES completion:failBlock];
  };
  voidBlock overallCancelBlock = ^{
    [presentingViewController dismissViewControllerAnimated:YES completion:cancelBlock];
  };
  stringBlock overallErrorBlock = !autoDismiss ? errorBlock : ^(NSString* message){
    [presentingViewController dismissViewControllerAnimated:YES completion:^{
      [MatcherUtil safeCallStringBlock:errorBlock withString:message];
    }];
  };
  
  voidBlock auth4FBlock = ^ {
    auth4FController =
    [[PhotoAuthViewController alloc] initWithNibName: @"Auth4F"
                                      welcomeMessage: welcomeMessage
                                           onSuccess: overallSuccessBlock
                                              onFail: overallFailBlock
                                            onCancel: overallCancelBlock
                                             onError: overallErrorBlock];
    
    auth4FController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    @try {
      nav.viewControllers = @[auth4FController];
      [presentingViewController presentViewController:nav animated:YES completion:nil];
    }
    @catch (NSException *exception) {
      DebugLog(@"Auth Presentation Exception: %@",exception);
      errorBlock(@"Cannot initialize 4f authentication");
    }
    @finally {
    }
    
  };
  
  auth4FBlock();
  
}

//+(void) setAuthenticationAutoStart:(BOOL)autoStart {
//    [self setAuthenticationAutoStart:autoStart forID:nil];
//}
+(void) setAuthenticationAutoStart:(BOOL)autoStart forID:(NSString*) accountId{
  [MatcherUtil setAutostart:autoStart forID:accountId];
}

static BOOL videoAllowed = NO;
static BOOL audioAllowed = NO;


+(void) checkVideoAccess{
  videoAllowed = NO;
  AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
  if(authStatus == AVAuthorizationStatusAuthorized) {
    videoAllowed = YES;
  } else if(authStatus == AVAuthorizationStatusDenied){
  } else if(authStatus == AVAuthorizationStatusRestricted){
  } else if(authStatus == AVAuthorizationStatusNotDetermined){
    // not determined?!
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
      videoAllowed = granted;
    }];
  } else {
    // impossible, unknown authorization status
  }
}

+(void) checkAudioAccess{
  audioAllowed = NO;
  AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
  if(authStatus == AVAuthorizationStatusAuthorized) {
    audioAllowed = YES;
  } else if(authStatus == AVAuthorizationStatusDenied){
  } else if(authStatus == AVAuthorizationStatusRestricted){
  } else if(authStatus == AVAuthorizationStatusNotDetermined){
    // not determined?!
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
      audioAllowed = granted;
    }];
  } else {
    // impossible, unknown authorization status
  }
}



+(NSArray*) getEnrolledIDs {
  return  [MatcherUtil getEnrolledIDs];
}



+(BOOL) isIDEnrolledFor4F:(NSString*)accountId{
  return nil != [MatcherUtil get4FEnrolledTemplatesForID:accountId];
}


+ (NSInteger)getRandomNumberBetween:(NSInteger)min maxNumber:(NSInteger)max
{
  return min + arc4random() % (max - min + 1);
}

+(void) resetAllBiometrics {
  [MatcherUtil deleteAllEnrollmentData];
}

+(void) resetBiometricsForID:(NSString*) accountId {
  [MatcherUtil deleteEnrollmentDataForID:accountId];
}


static NSBundle* resBundle = nil;

+(void) setResourcesBundle:(NSBundle*)bundle{
  resBundle = bundle;
}

+(NSBundle*) getResourcesBundle{
  return ;
}


@end
