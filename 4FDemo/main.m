//
//  main.m
//  4FDemo
//
//  Created by razvan on 8/13/15.
//  Copyright (c) 2015 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
