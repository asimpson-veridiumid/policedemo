//
//  ViewController.m
//  4FDemo
//
//  Created by razvan on 8/13/15.
//  Copyright (c) 2015 HoyosLabs. All rights reserved.
//

#import "ViewController.h"
//#import "HoyosMatcher.h"
#import "UIViewController+Alerts.h"
#import "PhotoAuthViewController.h"
#import "_FDemo-Swift.h"

#import <libffid/FlatInterface.h>

#define COLOR_LIGHT_BLUE(ALPHA) [UIColor colorWithRed:19/255.0 green:155/255.0 blue:235/255.0 alpha:ALPHA]

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *authButton;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIDatePicker *dobPicker;

@end

@implementation ViewController

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [_nameField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    
}

- (void)viewDidLoad {
  [super viewDidLoad];
    
  _nameField.delegate = self;

  self.authButton.layer.borderColor = COLOR_LIGHT_BLUE(1.0).CGColor;
  self.authButton.layer.borderWidth = 1.0;
  self.authButton.layer.cornerRadius = self.authButton.frame.size.height / 2.0;
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
    } else if(authStatus == AVAuthorizationStatusDenied){
    } else if(authStatus == AVAuthorizationStatusRestricted){
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        }];
    } else {
        // impossible, unknown authorization status
    }
}

-(void)viewWillAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)onAuth:(id)sender {
    // Need to replicate this functionality, preferably on first app launch...
    PhotoAuthViewController* auth4FController  =
        [[PhotoAuthViewController alloc] initWithNibName: @"Auth4F"
                                        welcomeMessage: @"Test authentication"
                                        onSuccess: ^(NSData* vector_data){
                                            //[self dismissViewControllerAnimated:YES completion:nil];
                                            UIStoryboard* sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            IdentifyResultViewController* result = [sb instantiateViewControllerWithIdentifier:@"IdentifyResultController"];
                                            //IdentifyResultViewController* result = [[IdentifyResultViewController alloc] init];
                                            
                                            result.name = self.nameField.text;
                                            
                                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                            dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                                            dateFormatter.dateFormat = @"yyyy-MM-dd";
                                            dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                                            result.dob = [dateFormatter stringFromDate:self.dobPicker.date];
                                            
                                            result.vector_data = vector_data;
                                            
                                            [[self navigationController] pushViewController:result animated:YES];
                                        }
                                        onFail: ^{
                                            //[self dismissViewControllerAnimated:YES completion:nil];
                                            [[self navigationController] popToViewController: self animated:YES];
                                            [self alert:@"4F Authentication Failed" withTitle:@"Info"];
                                        }
                                        onCancel: ^{
                                            [[self navigationController] popToViewController: self animated:YES];
                                            // [self alert:@"4F Authentication cancelled" withTitle:@"Info"];
                                        }
                                        onError: ^(NSString* message){
                                            [[self navigationController] popToViewController: self animated:YES];
                                            [self alert:message withTitle:@"Error"];
                                        }];
    
    auth4FController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    @try {
        //UINavigationController* nav = [[UINavigationController alloc] init];
        //[[self navigationController] presentViewController:auth4FController animated:YES completion:nil];
        //nav.viewControllers = @[auth4FController];
        
        [[self navigationController] pushViewController:auth4FController animated:YES];
    }
    @catch (NSException *exception) {
        DebugLog(@"Auth Presentation Exception: %@",exception);
    }
    
    @finally {
    }
    

}

@end
