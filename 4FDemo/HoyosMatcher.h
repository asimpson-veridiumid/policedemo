//
//  HoyosMatcher.h
//  TempestSDK
//
//  Created by Razvan Pocaznoi on 27.01.2014.
//  Copyright (c) 2014 Hoyos Labs. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HoyosMatcher : NSObject


/** Enroll a biometric template\n
 * There are 3 steps in the biometric enrollment process: 1. Aquisition, 2. Confirmation, 3. Verification (a test authentication with the newly aquired biometrics (with no liveness detection))

 * \param presentingViewController Controller that presents the enrollment scene (usually the current VC)
 * \param forID an optional custom id for storing enrollment data under (for multiple biometric enrollments on the same device scenario) NOTE!: it's your responsability to keep track of the ids you provide. - pass nil to use default
 * \param successBlock Called when the test authentiction finished with success
 * \param cancelBlock Called when the user hits the Cancel button in the Aquisition step
 * \param timeoutBlock Called if the aquisition times out (e.g. the subject face cannot be detected)
 * \param errorBlock Called when there was an error in any of the steps
 * \returns
 */
+(void)enroll4FFrom:(UIViewController*)presentingViewController
              forID:(NSString*)accountID
          onSuccess:(void(^)(void)) successBlock
           onCancel:(void(^)(void)) cancelBlock
          onTimeout:(void(^)(void)) timeoutBlock
            onError:(void(^)(NSString* message)) errorBlock;


/** Authenticate with registered biometrics.

 * There are 2 authetication modes: Biometrics only and Biometrics + Liveness Detection Mode

 * Biometrics only Mode: in this mode TempestSDK performs only a verification of the face features

 * Biometrics + Liveness Mode: in this mode in addition to biometric verfication, the user is required to perform a sequence of gestures (to prevent spoofing)

 * Liveness verification has 2 ways of detecting the gestures: @b Sequential and @b Parallel

 * In @b Sequential detection the liveness engine expects that the gestures in the gestures array passed to the matcher are performed by the subject in sequence. For example if the sequence is SMILE, EYEBROW RAISE, the engine expects the user to smile and it keeps detecting until smile is detected then advances to EYEBROW RAISE (or timeout is reached and the match fails)

 * In @b Parallel detection the engine doesn't expect a certain gesture, it keeps detecting (with a timeout per gesture) and a positive match occurs when all the gestures in the sequence are performed in the right order (even if there were some other gestures in between). For example if the gesture sequence is SMILE, RIGHT GAZE, if the user performs EYEBROW RAISE, SMILE, NOD, HEAD SHAKE it is a succesful match, while RIGHT GAZE, SMILE, NOD will be a fail

 * In the Sequential detection the user is ALWAYS prompted visually with images what gesture to do next, while for the parallel detection the prompts are displayed only if you call with testMode = YES, therefore the parallel detection mode is also called "Signature Mode" because the user can choose a secret sequence of gestures that only he knows, you can help him train for a few times with @c testMode = YES (so he can see the visual gesture hints) and then when authenticating put @c testMode = NO and he will see no prompts (only "Perform your next gesture")

 * \param presentingViewController Controller that presents the authentication scene (usually the current VC)
  * \param forID an optional custom id that identifies the biometric data (stored at enrollment) to be used for this authentication (for multiple biometric enrollments on the same device scenario) NOTE!: it's your responsability to keep track of the ids you provide. - pass nil to use default
 * \param withTouchID use TouchID authentication
 * \param withFace use face biometrics
 * \param livenessGesturesSequence Sequence of gestures for liveness (only when withFace is true). Empty or nil for no liveness
 * \param signatureMode Signature verification mode; YES = parallel detection, NO = sequential detection (and testMode is ignored)
 * \param testMode Specify if the user should get visual prompts of what gesture to perform next (only has effect in signature mode)
 * \param motionDetection Specify if should use motion detection for stabilization
 * \param autoDismiss Specify if it should dissmiss itself on finish (if you set this to NO, the controller will not dismiss when done and you will have to do it in one of the ending blocks (succes,fail, error)
 * \param hintsDelegate Allows you to specify delegate for handling user clicks on the liveness hints buttons and play demo button
 * \param welcomeMessage The string specified here will be displayed in the top message label in the auth screen before the authentication starts. You could put here the porpose the user is authenticating for
 * \param displayBlock Called when the authentication controller is displayed and is passed back to you for aditional display configuration
 * \param successBlock Called when the authentication finished with success
 * \param failBlock Called when the authentication finished with fail
 * \param errorBlock Called whe nthere was an error of authentication initialization
 * \returns
 
 * @b Examples:
 @code
 // Face Biometrics only
 [HoyosMatcher authenticateFrom:yourcontroller
                    withTouchID:YES
                       withFace:YES
                   withGestures:nil
                  signatureMode:NO
                       testMode:NO //does't matter
             useMotionDetection:NO //not needed usually
            autodismissOnFinish:NO // depends on whether you want to controll the dismissal or not
                      onDisplay:^(AuthViewController* authController){ .. do additional customizations} //can be nil
                      onSuccess:^{ .. do whatever yuo want when authentication suceeds}
                         onFail:^{ .. do whatever yuo want when authentication fails}
                        onError:^(NSString* message){ .. handle error}]
 @endcode
 @code
 // Face Biometrics only
 [HoyosMatcher authenticateFrom:yourcontroller
                    withTouchID:YES
                       withFace:YES
                   withGestures:nil
                  signatureMode:NO
                       testMode:NO //does't matter
             useMotionDetection:NO //not needed usually
            autodismissOnFinish:NO // depends on whether you want to controll the dismissal or not
                      onDisplay:^(AuthViewController* authController){ .. do additional customizations} //can be nil
                      onSuccess:^{ .. do whatever yuo want when authentication suceeds}
                         onFail:^{ .. do whatever yuo want when authentication fails}
                        onError:^(NSString* message){ .. handle error}]
 @endcode
 @code
 // Sequential mode
 [HoyosMatcher authenticateFrom:yourcontroller
                    withTouchID:YES
                       withFace:YES
                   withGestures:[generateRandomGesturesArrayWithNumberOfGestures:3] //can be also an array crafted by you using the defined constants
                  signatureMode:NO
                       testMode:NO //does't matter
             useMotionDetection:NO //not needed usually
            autodismissOnFinish:NO // depends on whether you want to controll the dismissal or not
                      onDisplay:^(AuthViewController* authController){ .. do additional customizations} //can be nil
                      onSuccess:^{ .. do whatever yuo want when authentication suceeds}
                         onFail:^{ .. do whatever yuo want when authentication fails}
                        onError:^(NSString* message){ .. handle error}]
 @endcode
 @code
 // Parallel mode
 [HoyosMatcher authenticateFrom:yourcontroller
                    withTouchID:YES
                       withFace:YES
                   withGestures:[..array of gestures chosen by the user as his signature]
                  signatureMode:YES
                       testMode:NO //set to YES to see the visual prompts (e.g. while the user trains on his sinature)
             useMotionDetection:NO //not needed usually
            autodismissOnFinish:NO // depends on whether you want to controll the dismissal or not
                      onDisplay:^(AuthViewController* authController){ .. do additional customizations} //can be nil
                      onSuccess:^{ .. do whatever yuo want when authentication suceeds}
                         onFail:^{ .. do whatever yuo want when authentication fails}
                        onError:^(NSString* message){ .. handle error}]
 @endcode
 */

+(void) authenticateFrom:(UIViewController*) presentingViewController
                   forID:(NSString*)accountID
          welcomeMessage:(NSString*) welcomeMessage
               onSuccess:(void(^)(void)) successBlock
                  onFail:(void(^)(void)) failBlock
                  onCancel:(void(^)(void)) cancelBlock
                 onError:(void(^)(NSString* message)) errorBlock;

/*!
 * Utility methods that returns YES only if an enrollment has been sucessfully performed for the account ID provided and the user can authenticate; pass nil for default account
 */
+(BOOL) isIDEnrolledFor4F:(NSString*)accountId;

/*!
 * Utility method that resets ALL the enrolled biometrics USE WITH CARE
 */
+(void) resetAllBiometrics;

/*!
 * Utility method that resets the enrolled biometrics for the account ID provided
 */
+(void) resetBiometricsForID:(NSString*) accountId;


+(void) setResourcesBundle:(NSBundle*)bundle;
+(NSBundle*) getResourcesBundle;

+(NSArray*) getEnrolledIDs;

@end
