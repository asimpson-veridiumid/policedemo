//
//  FourFCommonViewController.h
//  4F
//
//  Created by razvan on 8/7/15.
//  Copyright (c) 2015 Hoyos Labs. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#include <iostream>
#import "Progress4F.h"

@interface FourFCommonViewController : UIViewController < AVCaptureVideoDataOutputSampleBufferDelegate> {
  NSDateFormatter* dateFormatter;
  BOOL useLeftHand;
  NSString* accountIdToProcess;
}

- (IBAction)onStart:(id)sender;


@property (readonly, nonatomic) UIImage* overlayImage;

- (void) startCapturing;
- (void) stopCapturing;
- (void) startHandSearch;
- (void) continueByRestartingVideo;
-(void) shutdown4F;


-(void) showLivenessHint:(NSString*)text textColor:(UIColor*) textColor bgColor:(UIColor*) bgColor;
-(void) hideLivenessHint;

-(void) onCameraStarted;
-(void) onInit;

@property (weak, nonatomic) IBOutlet UIView *previewWrapper;
@property (weak, nonatomic) IBOutlet UIImageView *processingImage;
@property (weak, nonatomic) IBOutlet Progress4F *progressView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *bottomMessageLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *centerHintLabel;

@property (weak, nonatomic) IBOutlet UIImageView *overlayView;
@property (nonatomic, strong) AVCaptureSession* session;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIView *roisView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@end
