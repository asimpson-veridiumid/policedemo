//
//  AppDelegate.h
//  4FDemo
//
//  Created by razvan on 8/13/15.
//  Copyright (c) 2015 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

