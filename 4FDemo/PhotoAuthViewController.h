//
//  PhotoAuthViewController.h
//  4F
//
//  Created by razvan on 8/7/15.
//  Copyright (c) 2015 Hoyos Labs. All rights reserved.
//

//#import "MatcherUtil.h"
#import "FourFCommonViewController.h"

typedef void (^stringBlock)(NSString*);
typedef void (^voidBlock)(void);
typedef void (^dataBlock)(NSData*);
typedef void (^idBlock)(id);

@interface PhotoAuthViewController : FourFCommonViewController

-(id)initWithNibName:(NSString *)nibNameOrNil
      welcomeMessage:(NSString*) message
           onSuccess:(dataBlock) successBlk
              onFail:(voidBlock) failBlk
            onCancel:(voidBlock) cancelBlk
             onError:(stringBlock) errorBlk;

@end
