//
//  Progress4F.h
//  4FDemo
//
//  Created by razvan on 8/13/15.
//  Copyright (c) 2015 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Progress4F : UIView {
  NSMutableArray* layers;
  CAShapeLayer* bgShapeLayer;
}


@property UIColor* bgShapeColor;
@property UIColor* trackColor;
@property CGFloat trackWidth;

@property BOOL leftHand;

-(void) stop;
-(void) startLoader;
-(void) setProgress:(CGFloat) progress;
@end
