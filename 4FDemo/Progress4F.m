//
//  Progress4F.m
//  4FDemo
//
//  Created by razvan on 8/13/15.
//  Copyright (c) 2015 HoyosLabs. All rights reserved.
//

#import "Progress4F.h"

#define COLOR_LIGHT_BLUE(ALPHA) [UIColor colorWithRed:19/255.0 green:155/255.0 blue:235/255.0 alpha:ALPHA]

@interface Progress4F() {
  
  BOOL animating;
  BOOL layerCreated;

}

@end

@implementation Progress4F

-(id)initWithCoder:(NSCoder *)aDecoder{
  self = [super initWithCoder:aDecoder];
  animating = NO;
  layerCreated = NO;
  self.trackColor = COLOR_LIGHT_BLUE(1.0);
  self.trackWidth = 3.0;
  self.bgShapeColor = COLOR_LIGHT_BLUE(1.0);
  self.leftHand = YES;
  [self initLayers];
  return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
  self = [super initWithFrame:frame];
  animating = NO;
  layerCreated = NO;
  
  self.trackColor = COLOR_LIGHT_BLUE(1.0);
  self.trackWidth = 3.0;
  self.bgShapeColor = COLOR_LIGHT_BLUE(1.0);
  self.leftHand = YES;
  [self initLayers];
  return self;
}

-(void)updateConstraints {
  [self updateLayers];
  [super updateConstraints];
}

//func animateProgress(progress: CGFloat, duration: NSTimeInterval) {}

-(void) stop {
  
  [self resetAllAnimations];
  animating = NO;
}

-(void) initLayers {
  if (!layerCreated) {
    [self createLayers];
    layerCreated = true;
  }
  [self updateLayers];
}

-(void) startLoader{
  [self initLayers];
  [self stop];
  [self startLoadingAnimations];
}

-(void) setProgress:(CGFloat) progress {
  [self stop];
  for (CAShapeLayer* l in layers) {
    l.strokeEnd = progress;
    l.strokeStart = 0.0;
  }
  bgShapeLayer.opacity = progress;
}

-(void)animationDidStart:(CAAnimation *)anim{
  animating = true;
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{

}


-(void) createLayers {
  self.backgroundColor = [UIColor clearColor];

  bgShapeLayer =  [CAShapeLayer new];
  [self.layer addSublayer: bgShapeLayer];

  layers = [[NSMutableArray alloc] init];
  
  for (int i=0; i<15; i++) {
    [layers addObject:[CAShapeLayer new]];
    [self.layer addSublayer:layers[i]];
  }
  
}


-(void) resetAllAnimations {
  for (CAShapeLayer* l in layers) {
    l.strokeEnd = 0.0;
    l.strokeStart = 0.0;
    [l removeAllAnimations];
  }
  
  [bgShapeLayer removeAllAnimations];

}

-(void) startLoadingAnimations {
  
  CABasicAnimation* animation1 = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
  //    animation1.fromValue = CGFloat(0.0)
  animation1.toValue = @(1.0);
  animation1.duration = 1.0;
  //    animation1.beginTime = 0.1
  //    animation1.delegate = self
  animation1.removedOnCompletion = true;
  animation1.additive = true;
  //    animation1.autoreverses = true
  animation1.fillMode = kCAFillModeForwards;
  //    progressLayer.addAnimation(animation1, forKey: "strokeEnd")
  
  CABasicAnimation* animation2 = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
  //    animation2.fromValue = CGFloat(0.0)
  animation2.toValue = @(1.0);
  animation2.duration = 1.0;
  animation2.beginTime = 1.0;
  //    animation2.delegate = self
  animation2.removedOnCompletion = true;
  animation2.additive = true;
  //    animation2.autoreverses = true
  animation2.fillMode = kCAFillModeForwards;
  //    progressLayer.addAnimation(animation2, forKey: "strokeStart")
  
  
  CAAnimationGroup* mainAnim = [CAAnimationGroup new];
  mainAnim.animations = @[animation1, animation2];
  //    mainAnim.beginTime = 0.2
  mainAnim.duration = 2.0;
  mainAnim.repeatCount = HUGE;
  //    mainAnim.autoreverses = true
  mainAnim.delegate = self;
  
  for (CAShapeLayer* l in layers) {
    l.strokeEnd = 0.0;
    l.strokeStart = 0.0;
    
    [l addAnimation:mainAnim forKey:@"mainAnimation"];
  }
  
  bgShapeLayer.opacity = 0;
  
  CABasicAnimation* alphaAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
  alphaAnim.toValue = @(0.7);
  alphaAnim.duration = 1.0;
  alphaAnim.autoreverses = true;
  alphaAnim.repeatCount = HUGE;
  alphaAnim.delegate = self;
  
  [bgShapeLayer addAnimation:alphaAnim forKey:@"opacity"];
  
}


 - (void)drawRect:(CGRect)rect {
   [super drawRect:rect];
//   [self drawHollow:rect];
 }

-(void) updateLayers {
  
  if( self.leftHand ) {
    self.layer.transform = CATransform3DScale(CATransform3DMakeRotation(M_PI * 2.0f, 0, 0, 1),
                                            -1, 1, 1);
  }else {
    self.layer.transform = CATransform3DIdentity;
  }
  
  bgShapeLayer.fillColor = self.bgShapeColor.CGColor;
  bgShapeLayer.opacity = 0;
  bgShapeLayer.path = [self getOutlinePath: self.bounds];
  
  
  for (CAShapeLayer* l in layers) {
    l.backgroundColor = [UIColor clearColor].CGColor;
    l.fillColor = nil;
    l.strokeColor = self.trackColor.CGColor;
    l.lineWidth = self.trackWidth;
    l.strokeStart = 0.0;
    l.strokeEnd = 0.0;
    l.frame = self.bounds;
    l.lineCap = kCALineCapRound;
  }
  
  CGRect frame = self.bounds;
  
  //// Broken4F 1 Drawing
  UIBezierPath* broken4F1Path = UIBezierPath.bezierPath;
  [broken4F1Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51841 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.16804 * CGRectGetHeight(frame))];
  [broken4F1Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.54055 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.10898 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.52814 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.12883 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.54055 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.10898 * CGRectGetHeight(frame))];
  [broken4F1Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.82484 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.09102 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.54055 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.10898 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.64222 * CGRectGetWidth(frame), CGRectGetMinY(frame) + -0.06877 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 2 Drawing
  UIBezierPath* broken4F2Path = UIBezierPath.bezierPath;
  [broken4F2Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51735 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22299 * CGRectGetHeight(frame))];
  [broken4F2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16275 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20773 * CGRectGetHeight(frame))];
  [broken4F2Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11408 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.27457 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.13391 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20773 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.11408 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.23523 * CGRectGetHeight(frame))];
  [broken4F2Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16435 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.34141 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.11408 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.31390 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.13492 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.34139 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 3 Drawing
  UIBezierPath* broken4F3Path = UIBezierPath.bezierPath;
  [broken4F3Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11578 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37167 * CGRectGetHeight(frame))];
  [broken4F3Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.08072 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.27452 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.09431 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35106 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.08072 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.31595 * CGRectGetHeight(frame))];
  [broken4F3Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16275 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.15765 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.08072 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20788 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.11584 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.15765 * CGRectGetHeight(frame))];
  [broken4F3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51841 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.16804 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 4 Drawing
  UIBezierPath* broken4F4Path = UIBezierPath.bezierPath;
  [broken4F4Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11866 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame))];
  [broken4F4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame))];
  [broken4F4Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.01538 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.48996 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.04886 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.01538 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55312 * CGRectGetHeight(frame))];
  [broken4F4Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09120 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37263 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.01539 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42562 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.04888 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37263 * CGRectGetHeight(frame))];
  [broken4F4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11578 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37167 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 5 Drawing
  UIBezierPath* broken4F5Path = UIBezierPath.bezierPath;
  [broken4F5Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.25231 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80526 * CGRectGetHeight(frame))];
  [broken4F5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16785 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80989 * CGRectGetHeight(frame))];
  [broken4F5Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.69438 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.12581 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80989 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75878 * CGRectGetHeight(frame))];
  [broken4F5Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11866 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.65811 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.10228 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61828 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 6 Drawing
  UIBezierPath* broken4F6Path = UIBezierPath.bezierPath;
  [broken4F6Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.84490 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87270 * CGRectGetHeight(frame))];
  [broken4F6Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.62645 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93643 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.78481 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.91871 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.73518 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92427 * CGRectGetHeight(frame))];
  [broken4F6Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.57749 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94201 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.61135 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93813 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.59511 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93995 * CGRectGetHeight(frame))];
  [broken4F6Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29982 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97543 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.43595 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.95870 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.29982 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97543 * CGRectGetHeight(frame))];
  [broken4F6Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29853 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97561 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.29940 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97548 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.29896 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97561 * CGRectGetHeight(frame))];
  [broken4F6Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.23318 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87566 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.26248 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97561 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.23318 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93085 * CGRectGetHeight(frame))];
  [broken4F6Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.25231 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80526 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.23318 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.84805 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.24050 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82338 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 7 Drawing
  UIBezierPath* broken4F7Path = UIBezierPath.bezierPath;
  [broken4F7Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.82484 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.09102 * CGRectGetHeight(frame))];
  [broken4F7Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.98462 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.53726 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.93683 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20491 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.98462 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.33835 * CGRectGetHeight(frame))];
  [broken4F7Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.84490 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87270 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.98462 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.74800 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.91559 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.81852 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 8 Drawing
  UIBezierPath* broken4F8Path = UIBezierPath.bezierPath;
  [broken4F8Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16435 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.34141 * CGRectGetHeight(frame))];
  [broken4F8Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.44305 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35815 * CGRectGetHeight(frame))];
  [broken4F8Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.46152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.38339 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.45185 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35863 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.46152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37074 * CGRectGetHeight(frame))];
  [broken4F8Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.44288 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.40845 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.46152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.39604 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.45171 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.40811 * CGRectGetHeight(frame))];
  [broken4F8Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09162 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42344 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 9 Drawing
  UIBezierPath* broken4F9Path = UIBezierPath.bezierPath;
  [broken4F9Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09162 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42344 * CGRectGetHeight(frame))];
  [broken4F9Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.04805 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.48883 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.06718 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42344 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.04805 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.45259 * CGRectGetHeight(frame))];
  [broken4F9Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09162 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.04805 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52449 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.06679 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame))];
  [broken4F9Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.40706 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame))];
  [broken4F9Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.42885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.57645 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.41592 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.42885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56182 * CGRectGetHeight(frame))];
  [broken4F9Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.41062 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59995 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.42885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59109 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.41933 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59927 * CGRectGetHeight(frame))];
  [broken4F9Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16785 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62085 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 10 Drawing
  UIBezierPath* broken4F10Path = UIBezierPath.bezierPath;
  [broken4F10Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16785 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62085 * CGRectGetHeight(frame))];
  [broken4F10Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.12429 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.69177 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.14464 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62085 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.12429 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.65400 * CGRectGetHeight(frame))];
  [broken4F10Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16735 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75854 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.12429 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.72859 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.14382 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75852 * CGRectGetHeight(frame))];
  [broken4F10Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.49368 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.74184 * CGRectGetHeight(frame))];
  [broken4F10Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51049 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.76527 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.50249 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.74144 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.50994 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75178 * CGRectGetHeight(frame))];
  [broken4F10Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.49578 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.79178 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.51102 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.77849 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.50454 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.79044 * CGRectGetHeight(frame))];
  [broken4F10Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.30012 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82515 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 11 Drawing
  UIBezierPath* broken4F11Path = UIBezierPath.bezierPath;
  [broken4F11Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.30012 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82515 * CGRectGetHeight(frame))];
  [broken4F11Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29855 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82527 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.29961 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82524 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.29907 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82527 * CGRectGetHeight(frame))];
  [broken4F11Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.26587 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87532 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.28052 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82527 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.26587 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.84773 * CGRectGetHeight(frame))];
  [broken4F11Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29795 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92537 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.26587 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.90261 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.28021 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92489 * CGRectGetHeight(frame))];
  [broken4F11Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.57500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.89209 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.30872 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92406 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.43936 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.90807 * CGRectGetHeight(frame))];
  [broken4F11Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.62412 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.88648 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.59265 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.89000 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.60897 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.88818 * CGRectGetHeight(frame))];
  [broken4F11Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.83031 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82789 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.72840 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87482 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.77600 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.86949 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 12 Drawing
  UIBezierPath* broken4F12Path = UIBezierPath.bezierPath;
  [broken4F12Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.83031 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82789 * CGRectGetHeight(frame))];
  [broken4F12Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.95197 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.53727 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.89688 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.77690 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.95197 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.71987 * CGRectGetHeight(frame))];
  [broken4F12Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.80681 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.13274 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.95197 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35479 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.90758 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.24283 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 13 Drawing
  UIBezierPath* broken4F13Path = UIBezierPath.bezierPath;
  [broken4F13Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.80681 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.13274 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.56475 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.14265 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.74171 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.06163 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.62042 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.04222 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55153 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22444 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.54849 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.17198 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.55153 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22444 * CGRectGetHeight(frame))];
  [broken4F13Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22444 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55203 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22740 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.55168 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22543 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.55186 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22640 * CGRectGetHeight(frame))];
  [broken4F13Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55692 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.25559 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.61271 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55048 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.58612 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42466 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.61218 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.54791 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.63914 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60330 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.61657 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.57293 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.62598 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59168 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68248 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61207 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.65234 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61494 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.66772 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61806 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71705 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.57182 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.69718 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60611 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.70947 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59180 * CGRectGetHeight(frame))];
  [broken4F13Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.72285 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.50635 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.72464 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55182 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.72667 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52838 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 14 Drawing
  UIBezierPath* broken4F14Path = UIBezierPath.bezierPath;
  [broken4F14Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.72285 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.50635 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68455 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29745 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68453 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29745 * CGRectGetHeight(frame))];
  [broken4F14Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29722 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.68452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29738 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.68452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29730 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68445 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29693 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68448 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29691 * CGRectGetHeight(frame))];
  [broken4F14Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.69641 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26574 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.68256 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28443 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.68832 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26902 * CGRectGetHeight(frame))];
  [broken4F14Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71683 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28380 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.70450 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26246 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.71448 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.27150 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71687 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28379 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71696 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28432 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71697 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28433 * CGRectGetHeight(frame))];
  [broken4F14Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.75452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.49300 * CGRectGetHeight(frame))];
  [broken4F14Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.74541 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59709 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.76065 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52852 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.75742 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56545 * CGRectGetHeight(frame))];
  [broken4F14Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.69087 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.66066 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.73342 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62868 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.71405 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.65126 * CGRectGetHeight(frame))];

  
  
  //// Broken4F 15 Drawing
  UIBezierPath* broken4F15Path = UIBezierPath.bezierPath;
  [broken4F15Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.69087 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.66066 * CGRectGetHeight(frame))];
  [broken4F15Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.58125 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56468 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.64292 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.68011 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.59362 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.63645 * CGRectGetHeight(frame))];
  [broken4F15Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.52522 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26844 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.58100 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56344 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.55465 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.43896 * CGRectGetHeight(frame))];
  [broken4F15Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.52035 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.24040 * CGRectGetHeight(frame))];
  [broken4F15Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51735 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22299 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.51932 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.23452 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.51837 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22882 * CGRectGetHeight(frame))];

  
  
  ((CAShapeLayer*)layers[0]).path = broken4F1Path.CGPath;
  ((CAShapeLayer*)layers[1]).path = broken4F2Path.CGPath;
  ((CAShapeLayer*)layers[2]).path = broken4F3Path.CGPath;
  ((CAShapeLayer*)layers[3]).path = broken4F4Path.CGPath;
  ((CAShapeLayer*)layers[4]).path = broken4F5Path.CGPath;
  ((CAShapeLayer*)layers[5]).path = broken4F6Path.CGPath;
  ((CAShapeLayer*)layers[6]).path = broken4F7Path.CGPath;
  ((CAShapeLayer*)layers[7]).path = broken4F8Path.CGPath;
  ((CAShapeLayer*)layers[8]).path = broken4F9Path.CGPath;
  ((CAShapeLayer*)layers[9]).path = broken4F10Path.CGPath;
  ((CAShapeLayer*)layers[10]).path = broken4F11Path.CGPath;
  ((CAShapeLayer*)layers[11]).path = broken4F12Path.CGPath;
  ((CAShapeLayer*)layers[12]).path = broken4F13Path.CGPath;
  ((CAShapeLayer*)layers[13]).path = broken4F14Path.CGPath;
  ((CAShapeLayer*)layers[14]).path = broken4F15Path.CGPath;
  
}




- (void)drawHollow:(CGRect)frame {
  
  UIBezierPath* full4FPath = [UIBezierPath bezierPathWithCGPath:[self getOutlinePath:frame]];
  full4FPath.miterLimit = 4;
  
  [self.bgShapeColor setFill];
  [full4FPath fill];
}

-(CGPathRef) getOutlinePath:(CGRect) frame {
  
  UIBezierPath* full4FPath = UIBezierPath.bezierPath;
  [full4FPath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09120 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37263 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11578 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37167 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.08072 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.27452 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.09431 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35106 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.08072 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.31595 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16275 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.15765 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.08072 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20788 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.11584 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.15765 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51841 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.16804 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.54055 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.10898 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.52814 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.12883 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.54055 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.10898 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.82484 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.09102 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.54055 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.10898 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.64222 * CGRectGetWidth(frame), CGRectGetMinY(frame) + -0.06877 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.98462 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.53726 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.93683 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20491 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.98462 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.33835 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.84490 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87270 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.98462 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.74800 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.91559 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.81852 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.62645 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93643 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.78481 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.91871 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.73518 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92427 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.57749 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94201 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.61135 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93813 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.59511 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93995 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29982 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97543 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.43595 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.95870 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.29982 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97543 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29853 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97561 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.29940 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97548 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.29896 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97561 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.23318 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87566 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.26248 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.97561 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.23318 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.93085 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.25231 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80526 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.23318 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.84805 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.24050 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82338 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16785 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80989 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.69438 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.12581 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.80989 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75878 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11866 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.65811 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.10228 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61828 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09161 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.01538 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.48996 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.04886 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60521 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.01538 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55312 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09120 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37263 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.01539 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42562 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.04888 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37263 * CGRectGetHeight(frame))];
  [full4FPath closePath];
  [full4FPath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09162 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.40706 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.42885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.57645 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.41592 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.42885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56182 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.41062 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59995 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.42885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59109 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.41933 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59927 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16785 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62085 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.12429 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.69177 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.14464 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62085 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.12429 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.65400 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16735 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75854 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.12429 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.72859 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.14382 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75852 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.49368 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.74184 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51049 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.76527 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.50249 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.74144 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.50994 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.75178 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.49578 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.79178 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.51102 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.77849 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.50454 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.79044 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.30012 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82515 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29855 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82527 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.29961 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82524 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.29907 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82527 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.26587 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87532 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.28052 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82527 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.26587 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.84773 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29795 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92537 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.26587 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.90261 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.28021 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92489 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.57500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.89209 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.30872 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.92406 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.43936 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.90807 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.62412 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.88648 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.59265 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.89000 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.60897 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.88818 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.83031 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.82789 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.72840 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.87482 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.77600 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.86949 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.95197 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.53727 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.89688 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.77690 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.95197 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.71987 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.80681 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.13274 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.95197 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35479 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.90758 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.24283 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.56475 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.14265 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.74171 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.06163 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.62042 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.04222 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55153 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22444 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.54849 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.17198 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.55153 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22444 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22444 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55203 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22740 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.55168 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22543 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.55186 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22640 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.55692 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.25559 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.61271 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55048 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.58612 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42466 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.61218 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.54791 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.63914 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60330 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.61657 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.57293 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.62598 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59168 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68248 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61207 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.65234 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61494 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.66772 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.61806 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71705 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.57182 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.69718 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.60611 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.70947 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59180 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.72285 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.50635 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.72464 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55182 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.72667 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52838 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68455 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29745 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68453 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29745 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29722 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.68452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29738 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.68452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29730 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68445 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29693 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.68448 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.29691 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.69641 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26574 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.68256 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28443 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.68832 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26902 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71683 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28380 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.70450 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26246 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.71448 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.27150 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71687 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28379 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71696 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28432 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.71697 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.28433 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.75452 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.49300 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.74541 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.59709 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.76065 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52852 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.75742 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56545 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.69087 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.66066 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.73342 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.62868 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.71405 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.65126 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.58125 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56468 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.64292 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.68011 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.59362 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.63645 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.52522 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.26844 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.58100 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56344 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.55465 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.43896 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.52035 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.24040 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.51735 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22299 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.51932 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.23452 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.51837 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.22882 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16275 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20773 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.11408 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.27457 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.13391 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.20773 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.11408 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.23523 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.16435 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.34141 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.11408 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.31390 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.13492 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.34139 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.44305 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35815 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.46152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.38339 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.45185 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.35863 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.46152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.37074 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.44288 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.40845 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.46152 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.39604 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.45171 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.40811 * CGRectGetHeight(frame))];
  [full4FPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09162 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42344 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.04805 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.48883 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.06718 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.42344 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.04805 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.45259 * CGRectGetHeight(frame))];
  [full4FPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.09162 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.04805 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52449 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.06679 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55288 * CGRectGetHeight(frame))];
  [full4FPath closePath];

  return full4FPath.CGPath;
}

@end


////
////  CustomOneULoadingView.swift
////  CustomProgressBar
////
////  Created by razvan on 6/12/15.
////  Copyright (c) 2015 Sztanyi Szabolcs. All rights reserved.
////
//
//import UIKit
//
//public class CustomOneULoadingView: AbstractLoadingView {
//  
//  
//  
//  private func drawLogoHollow(#rect: CGRect) {
//    //// General Declarations
//    let context = UIGraphicsGetCurrentContext()
//    
//    //// Color Declarations
//    let gradientColor = gradientBottom.CGColor
//    let gradientColor2 = gradientTop.CGColor
//    
//    //// Gradient Declarations
//    let sVGID_1_ = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [gradientColor!, gradientColor2!], [0, 1])
//    
//    //// Bezier Drawing
//    var bezierPath = UIBezierPath()
//    bezierPath.moveToPoint(CGPointMake(rect.minX + 0.49998 * rect.width, rect.minY + 0.00000 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.00000 * rect.width, rect.minY + 0.49998 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.22384 * rect.width, rect.minY + 0.00000 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.00000 * rect.width, rect.minY + 0.22387 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.49998 * rect.width, rect.minY + 1.00000 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.00000 * rect.width, rect.minY + 0.77614 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.22384 * rect.width, rect.minY + 1.00000 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 1.00000 * rect.width, rect.minY + 0.49998 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.77611 * rect.width, rect.minY + 1.00000 * rect.height), controlPoint2: CGPointMake(rect.minX + 1.00000 * rect.width, rect.minY + 0.77614 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.49998 * rect.width, rect.minY + 0.00000 * rect.height), controlPoint1: CGPointMake(rect.minX + 1.00000 * rect.width, rect.minY + 0.22387 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.77611 * rect.width, rect.minY + 0.00000 * rect.height))
//    bezierPath.closePath()
//    bezierPath.moveToPoint(CGPointMake(rect.minX + 0.34872 * rect.width, rect.minY + 0.27554 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.34868 * rect.width, rect.minY + 0.27550 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.47158 * rect.width, rect.minY + 0.19179 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.48368 * rect.width, rect.minY + 0.18472 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.47518 * rect.width, rect.minY + 0.18882 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.47927 * rect.width, rect.minY + 0.18644 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.48657 * rect.width, rect.minY + 0.18366 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.48463 * rect.width, rect.minY + 0.18432 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.48562 * rect.width, rect.minY + 0.18399 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.48737 * rect.width, rect.minY + 0.18338 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.48681 * rect.width, rect.minY + 0.18358 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.48706 * rect.width, rect.minY + 0.18345 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.50950 * rect.width, rect.minY + 0.18215 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.49459 * rect.width, rect.minY + 0.18112 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.50219 * rect.width, rect.minY + 0.18074 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.54732 * rect.width, rect.minY + 0.22671 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.53105 * rect.width, rect.minY + 0.18604 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.54731 * rect.width, rect.minY + 0.20451 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.54732 * rect.width, rect.minY + 0.55549 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.50108 * rect.width, rect.minY + 0.60084 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.54732 * rect.width, rect.minY + 0.58052 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.52662 * rect.width, rect.minY + 0.60084 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.45482 * rect.width, rect.minY + 0.55549 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.47550 * rect.width, rect.minY + 0.60084 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.45482 * rect.width, rect.minY + 0.58052 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.45477 * rect.width, rect.minY + 0.55549 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.45477 * rect.width, rect.minY + 0.31526 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.40076 * rect.width, rect.minY + 0.35206 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.40072 * rect.width, rect.minY + 0.35202 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.33721 * rect.width, rect.minY + 0.33926 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.38004 * rect.width, rect.minY + 0.36610 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.35161 * rect.width, rect.minY + 0.36040 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.34872 * rect.width, rect.minY + 0.27554 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.32287 * rect.width, rect.minY + 0.31813 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.32803 * rect.width, rect.minY + 0.28961 * rect.height))
//    bezierPath.closePath()
//    bezierPath.moveToPoint(CGPointMake(rect.minX + 0.73737 * rect.width, rect.minY + 0.56932 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.50107 * rect.width, rect.minY + 0.80620 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.73737 * rect.width, rect.minY + 0.69973 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.63146 * rect.width, rect.minY + 0.80620 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.26476 * rect.width, rect.minY + 0.56932 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.37065 * rect.width, rect.minY + 0.80620 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.26476 * rect.width, rect.minY + 0.69973 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.26476 * rect.width, rect.minY + 0.47289 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.26479 * rect.width, rect.minY + 0.47289 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.31104 * rect.width, rect.minY + 0.42661 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.26479 * rect.width, rect.minY + 0.44735 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.28549 * rect.width, rect.minY + 0.42661 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.35733 * rect.width, rect.minY + 0.47289 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.33662 * rect.width, rect.minY + 0.42661 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.35733 * rect.width, rect.minY + 0.44735 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.35733 * rect.width, rect.minY + 0.56932 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.50107 * rect.width, rect.minY + 0.71392 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.35733 * rect.width, rect.minY + 0.64871 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.42164 * rect.width, rect.minY + 0.71392 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.64481 * rect.width, rect.minY + 0.56932 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.58049 * rect.width, rect.minY + 0.71392 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.64481 * rect.width, rect.minY + 0.64871 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.64481 * rect.width, rect.minY + 0.47289 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.69105 * rect.width, rect.minY + 0.42661 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.64481 * rect.width, rect.minY + 0.44735 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.66552 * rect.width, rect.minY + 0.42661 * rect.height))
//    bezierPath.addCurveToPoint(CGPointMake(rect.minX + 0.73735 * rect.width, rect.minY + 0.47289 * rect.height), controlPoint1: CGPointMake(rect.minX + 0.71664 * rect.width, rect.minY + 0.42661 * rect.height), controlPoint2: CGPointMake(rect.minX + 0.73735 * rect.width, rect.minY + 0.44735 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.73737 * rect.width, rect.minY + 0.47289 * rect.height))
//    bezierPath.addLineToPoint(CGPointMake(rect.minX + 0.73737 * rect.width, rect.minY + 0.56932 * rect.height))
//    bezierPath.closePath()
//    bezierPath.miterLimit = 4;
//    
//    CGContextSaveGState(context)
//    bezierPath.addClip()
//    let bezierBounds: CGRect = CGPathGetPathBoundingBox(bezierPath.CGPath)
//    CGContextDrawLinearGradient(context, sVGID_1_,
//                                CGPointMake(bezierBounds.midX + 0 * bezierBounds.width / 521, bezierBounds.midY + 260.72 * bezierBounds.height / 521),
//                                CGPointMake(bezierBounds.midX + 0 * bezierBounds.width / 521, bezierBounds.midY + -260.72 * bezierBounds.height / 521),
//                                UInt32(kCGGradientDrawsBeforeStartLocation) | UInt32(kCGGradientDrawsAfterEndLocation))
//    CGContextRestoreGState(context)
//  }
//  
//  
//  
//  private func getOneOutline(#customFrame:CGRect) -> CGPath {
//    
//    
//    let oneuoutline: CGRect = CGRectMake(customFrame.minX, customFrame.minY, customFrame.width, customFrame.height)
//    
//    //// oneuoutline
//    //// Bezier 2 Drawing
//    var bezier2Path = UIBezierPath()
//    bezier2Path.moveToPoint(CGPointMake(oneuoutline.minX + 0.50997 * oneuoutline.width, oneuoutline.minY + 0.18231 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.48781 * oneuoutline.width, oneuoutline.minY + 0.18354 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.50265 * oneuoutline.width, oneuoutline.minY + 0.18090 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.49504 * oneuoutline.width, oneuoutline.minY + 0.18128 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.48701 * oneuoutline.width, oneuoutline.minY + 0.18382 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.48751 * oneuoutline.width, oneuoutline.minY + 0.18362 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.48725 * oneuoutline.width, oneuoutline.minY + 0.18375 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.48412 * oneuoutline.width, oneuoutline.minY + 0.18489 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.48606 * oneuoutline.width, oneuoutline.minY + 0.18415 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.48507 * oneuoutline.width, oneuoutline.minY + 0.18448 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.47201 * oneuoutline.width, oneuoutline.minY + 0.19196 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.47971 * oneuoutline.width, oneuoutline.minY + 0.18661 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.47561 * oneuoutline.width, oneuoutline.minY + 0.18899 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.34900 * oneuoutline.width, oneuoutline.minY + 0.27575 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.34904 * oneuoutline.width, oneuoutline.minY + 0.27578 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.33752 * oneuoutline.width, oneuoutline.minY + 0.33956 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.32833 * oneuoutline.width, oneuoutline.minY + 0.28987 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.32317 * oneuoutline.width, oneuoutline.minY + 0.31841 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.40109 * oneuoutline.width, oneuoutline.minY + 0.35234 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.35193 * oneuoutline.width, oneuoutline.minY + 0.36072 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.38039 * oneuoutline.width, oneuoutline.minY + 0.36643 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.40113 * oneuoutline.width, oneuoutline.minY + 0.35238 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.45518 * oneuoutline.width, oneuoutline.minY + 0.31554 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.45518 * oneuoutline.width, oneuoutline.minY + 0.55599 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.45523 * oneuoutline.width, oneuoutline.minY + 0.55599 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.50153 * oneuoutline.width, oneuoutline.minY + 0.60138 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.45523 * oneuoutline.width, oneuoutline.minY + 0.58104 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.47593 * oneuoutline.width, oneuoutline.minY + 0.60138 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.54781 * oneuoutline.width, oneuoutline.minY + 0.55599 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.52710 * oneuoutline.width, oneuoutline.minY + 0.60138 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.54781 * oneuoutline.width, oneuoutline.minY + 0.58104 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.54781 * oneuoutline.width, oneuoutline.minY + 0.55599 * oneuoutline.height))
//    bezier2Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.54781 * oneuoutline.width, oneuoutline.minY + 0.22691 * oneuoutline.height))
//    bezier2Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.50997 * oneuoutline.width, oneuoutline.minY + 0.18231 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.54781 * oneuoutline.width, oneuoutline.minY + 0.20470 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.53154 * oneuoutline.width, oneuoutline.minY + 0.18621 * oneuoutline.height))
//    bezier2Path.closePath()
//    
//    
//    //// Bezier 3 Drawing
//    var bezier3Path = UIBezierPath()
//    bezier3Path.moveToPoint(CGPointMake(oneuoutline.minX + 0.50153 * oneuoutline.width, oneuoutline.minY + 0.80692 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.73804 * oneuoutline.width, oneuoutline.minY + 0.56983 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.63204 * oneuoutline.width, oneuoutline.minY + 0.80692 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.73804 * oneuoutline.width, oneuoutline.minY + 0.70035 * oneuoutline.height))
//    bezier3Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.73804 * oneuoutline.width, oneuoutline.minY + 0.47331 * oneuoutline.height))
//    bezier3Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.73802 * oneuoutline.width, oneuoutline.minY + 0.47331 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.69168 * oneuoutline.width, oneuoutline.minY + 0.42699 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.73802 * oneuoutline.width, oneuoutline.minY + 0.44775 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.71729 * oneuoutline.width, oneuoutline.minY + 0.42699 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.64540 * oneuoutline.width, oneuoutline.minY + 0.47331 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.66612 * oneuoutline.width, oneuoutline.minY + 0.42699 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.64540 * oneuoutline.width, oneuoutline.minY + 0.44775 * oneuoutline.height))
//    bezier3Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.64540 * oneuoutline.width, oneuoutline.minY + 0.56983 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.50153 * oneuoutline.width, oneuoutline.minY + 0.71456 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.64540 * oneuoutline.width, oneuoutline.minY + 0.64929 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.58102 * oneuoutline.width, oneuoutline.minY + 0.71456 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.35766 * oneuoutline.width, oneuoutline.minY + 0.56983 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.42203 * oneuoutline.width, oneuoutline.minY + 0.71456 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.35766 * oneuoutline.width, oneuoutline.minY + 0.64929 * oneuoutline.height))
//    bezier3Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.35766 * oneuoutline.width, oneuoutline.minY + 0.47331 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.31132 * oneuoutline.width, oneuoutline.minY + 0.42699 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.35766 * oneuoutline.width, oneuoutline.minY + 0.44775 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.33692 * oneuoutline.width, oneuoutline.minY + 0.42699 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.26503 * oneuoutline.width, oneuoutline.minY + 0.47331 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.28575 * oneuoutline.width, oneuoutline.minY + 0.42699 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.26503 * oneuoutline.width, oneuoutline.minY + 0.44775 * oneuoutline.height))
//    bezier3Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.26500 * oneuoutline.width, oneuoutline.minY + 0.47331 * oneuoutline.height))
//    bezier3Path.addLineToPoint(CGPointMake(oneuoutline.minX + 0.26500 * oneuoutline.width, oneuoutline.minY + 0.56983 * oneuoutline.height))
//    bezier3Path.addCurveToPoint(CGPointMake(oneuoutline.minX + 0.50153 * oneuoutline.width, oneuoutline.minY + 0.80692 * oneuoutline.height), controlPoint1: CGPointMake(oneuoutline.minX + 0.26500 * oneuoutline.width, oneuoutline.minY + 0.70035 * oneuoutline.height), controlPoint2: CGPointMake(oneuoutline.minX + 0.37099 * oneuoutline.width, oneuoutline.minY + 0.80692 * oneuoutline.height))
//    bezier3Path.closePath()
//    
//    var copyfullPath = CGPathCreateMutableCopy(bezier2Path.CGPath)
//    CGPathAddPath(copyfullPath, nil, bezier3Path.CGPath)
//    return copyfullPath
//  }
//  
//  
//  
//  }
