//
//  FourFCommonViewController.m
//  4F
//
//  Created by razvan on 8/7/15.
//  Copyright (c) 2015 Hoyos Labs. All rights reserved.
//

#import "FourFCommonViewController.h"
#import <AVFoundation/AVFoundation.h>

#include <iostream>
#import  <libffid/FlatInterface.h>
//#include "FFIDMinutiae.h"
//#include "common.h"
//#include "KeychainUtils.h"
#include "OpenCVUtils.h"
//#include "HoyosMatcher.h"

typedef void (^stringBlock)(NSString*);
typedef void (^voidBlock)(void);

#define STAGE_INIT 0
#define STAGE_CAMERA_STARTED 1
#define STAGE_HAND_DETECTION 2
#define STAGE_HAND_DETECTED 3
#define STAGE_PHOTO_STARTED 4
#define STAGE_PHOTO_PROCESSING 5

#define COLOR_LIGHT_BLUE(ALPHA) [UIColor colorWithRed:19/255.0 green:155/255.0 blue:235/255.0 alpha:ALPHA]
#define COLOR_DETECTED_YELLOW_ALPHA(ALPHA) [UIColor colorWithRed:252/255.0 green:176/255.0 blue:43/255.0 alpha:ALPHA]
#define COLOR_CAPTURE_GREEN_ALPHA(ALPHA) [UIColor colorWithRed:20/255.0 green:204/255.0 blue:111/255.0 alpha:ALPHA]

#define COLOR_DETECTED_YELLOW COLOR_DETECTED_YELLOW_ALPHA(1.0)

using namespace cv;

@interface FourFCommonViewController (){
  AVCaptureVideoPreviewLayer *previewLayer;
  NSInteger currentStage;
  AVCaptureStillImageOutput* photoDataOutput;
  
  UIImagePickerController* picker;
  
  ///
  //  std::shared_ptr<unsigned char> cameraBuffer;
  dispatch_queue_t backgroundROISQueue;
  
  BOOL canProcessNext;
  int rois[16], last_rois[16], photoRois[16];
  int stillStrikes;
  double stillStartTime; // start time of still
  double lastRoiTime;    // time of last roi
    
  NSArray* roisLayers;
  BOOL shouldStartVideoOnAppear;
  
  BOOL is4FInitialized;
  UIImage* overlayLeftHandImge;
  UIImage* overlayRightHandImge;
  NSTimer* captureTimer;
  BOOL pictureTaken;
    
  BOOL pictureBeingTaken;
  //BOOL autoFocusStarted;
    
  BOOL failureMoratorium;
  double firstOutOfFocusTime;
    
  UIColor* roisColor;
  CGSize roiSourceSize;
  BOOL roisShouldDraw;
  //double roisLastDrawTime;
    
  CADisplayLink *displayLink;
}

@end

@implementation FourFCommonViewController


+(UIImage*) fullImageFromBuffer:(CMSampleBufferRef) sampleBuffer orientation:(UIImageOrientation) orientation{
    CGImageRef imgRef = [FourFCommonViewController imageFromSampleBuffer:sampleBuffer];
    UIImage* img = [UIImage imageWithCGImage:imgRef scale:1.0 orientation: orientation];
    CGImageRelease(imgRef);
    return img;
}

+ (CGImageRef) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer // Create a CGImageRef from sample buffer data
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the base address
    void *baseAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    
    
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // We should return CGImageRef rather than UIImage because QR Scan needs CGImageRef
    return quartzImage;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  dateFormatter = [[NSDateFormatter alloc] init] ;
  [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];

  roisLayers = @[[CAShapeLayer new],[CAShapeLayer new],[CAShapeLayer new],[CAShapeLayer new]];
  
  for(int i=0;i<4;i++)
  {
    [self.roisView.layer addSublayer:roisLayers[i]];
  }
  
  backgroundROISQueue = dispatch_queue_create("com.hoyoslabs.backgroundROISQUEUE", 0);
  canProcessNext = NO;
  
  shouldStartVideoOnAppear = YES;
  
  self.previewWrapper.layer.borderColor = COLOR_LIGHT_BLUE(1.0).CGColor;
  self.previewWrapper.layer.borderWidth = 1.0;
  self.previewWrapper.layer.cornerRadius = 5.0;
  is4FInitialized = NO;
  overlayLeftHandImge = [UIImage imageNamed:@"ffid_hint" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
  overlayRightHandImge = [UIImage imageWithCGImage:overlayLeftHandImge.CGImage
                                             scale:overlayLeftHandImge.scale
                                       orientation:UIImageOrientationUpMirrored];

  failureMoratorium = NO;
  firstOutOfFocusTime = -1.0;
    
  roisShouldDraw = NO;
}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

void* autofocuscontext = &autofocuscontext;

-(void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  if (shouldStartVideoOnAppear) {
    [self.view layoutIfNeeded];
    [self goToStage:STAGE_INIT];
  }else{
    shouldStartVideoOnAppear = YES;
    NSLog(@"Skipping Init on ViewWillAppear");
  }
    
    /*
    // Set up Focus KVO
    pictureBeingTaken = NO;
    autoFocusStarted = NO;
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device addObserver:self forKeyPath:@"adjustingFocus" options:NSKeyValueObservingOptionNew context:autofocuscontext];
    */
    
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(roisDraw)];
    [displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}

-(void)viewDidLayoutSubviews {
  if (nil != previewLayer) //fuckin hack/fix for video preview offset
    previewLayer.frame = self.videoView.bounds;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [displayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    displayLink = nil;
    
    // Stop KVO
    //AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    //[device removeObserver:self forKeyPath:@"adjustingFocus" context:autofocuscontext];
}

-(UIImage *)overlayImage{
  return useLeftHand ? overlayLeftHandImge : overlayRightHandImge;
}

-(void) init4F {
  if(!is4FInitialized) {
    
    /*
    const std::string handCascadePath  = [[[NSBundle mainBundle] pathForResource:@"cascade_hand.xml" ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding];
    const std::string tipCascadePathA  = [[[NSBundle mainBundle] pathForResource:@"cascade_fingertips_a.xml" ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding];
    const std::string tipCascadePathB  = [[[NSBundle mainBundle] pathForResource:@"cascade_fingertips_b.xml" ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding];
    const std::string tipCascadePathC  = [[[NSBundle mainBundle] pathForResource:@"cascade_fingertips_c.xml" ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding];
    const std::string tipCascadePathD  = [[[NSBundle mainBundle] pathForResource:@"cascade_fingertips_d.xml" ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding];
    */
      
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    const float fov = device.activeFormat.videoFieldOfView;
      
    FourF::Interface::InitSettings002 settings = {"", "", "", "", "", 0, 0, fov, 13};
    
    FourF::Interface::init(settings);
    is4FInitialized = YES;
      
  }
}

-(void) shutdown4F {
  if (is4FInitialized) {
    FourF::Interface::shutdown();
    is4FInitialized = NO;
  }
}


- (IBAction)onStart:(id)sender {
  //  [self goToStage:STAGE_HAND_DETECTION];
  //[self stopCapturing];
  canProcessNext = NO;
  dispatch_sync(backgroundROISQueue, ^{});
  [self goToStage:STAGE_PHOTO_STARTED];
  [self drawFreezedRoisWithParentView:self.roisView];
    
  [self av_capture];
  //[self oldAvCapture];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    /*
    if(context == autofocuscontext)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
            NSLog(@"Focus KVO");
            BOOL adjustingFocus = [ [change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1] ];
            NSLog(@"Is adjusting focus? %@", adjustingFocus ? @"YES" : @"NO" );
            NSLog(@"Property value: %@", device.adjustingFocus ? @"YES" : @"NO");
            NSLog(@"Change dictionary: %@", change);
            
            if(pictureBeingTaken == YES)
            {
                if(adjustingFocus == YES)
                {
                    autoFocusStarted = YES;
                }
                else
                {
                    if(autoFocusStarted == YES)
                    {
                        autoFocusStarted = NO;
                        pictureBeingTaken = NO;
                        [self avDoCapture];
                    }
                }
            }
        });
    }
    */
}

#include <sys/types.h>
#include <sys/sysctl.h>

- (NSString *) platform{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = reinterpret_cast<char*>(malloc(size));
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

/// Get a FourF image handle from a CMSampleBufferRef
+ (FourF::Interface::ImageHandle) imageHandleFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // Explicitly retain the image buffer so that it stays valid while we need it.
    CVBufferRetain(imageBuffer);
    
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the base address
    void * const baseAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    
    // Get the pixel buffer width and height
    const size_t width = CVPixelBufferGetWidth(imageBuffer);
    const size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a deallocation callback that will be called when the ImageHandle is destroyed.
    // This will unlock the base address and release the buffer.
    auto dealloc_callback = [=](void* data, void* state)
    {
        // Unlock the pixel buffer
        CVPixelBufferUnlockBaseAddress(imageBuffer,0);
        
        // And release the buffer.
        CVBufferRelease(imageBuffer);
        
        NSLog(@"Deallocated ImageHandle that came from CMSampleBufferRef.");
    };
    
    const int intWidth = static_cast<int>(width);
    const int intHeight = static_cast<int>(height);
    
    NSLog(@"Allocated ImageHandle from CMSampleBufferRef.");
    return FourF::Interface::ImageHandle::loadBGRA8888Image(baseAddress, intWidth, intHeight, nullptr, dealloc_callback);
}

-(void) oldAvCapture {
    NSError* error = nil;
    
    NSString* prev_preset = [self session].sessionPreset;
    
    [self.session beginConfiguration];
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if([device lockForConfiguration:&error] == YES)
    {
        [device setTorchMode:AVCaptureTorchModeOn];
        CGPoint point = CGPointMake(0.5f, 0.67f);
        
        if([device isExposurePointOfInterestSupported])
        {
            [device setExposurePointOfInterest: point];
        }
        
        if([device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
        {
            [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        }

        if([device isAutoFocusRangeRestrictionSupported])
        {
            [device setAutoFocusRangeRestriction:AVCaptureAutoFocusRangeRestrictionNear];
        }
        
        if([device isFocusModeSupported:AVCaptureFocusModeAutoFocus])
        {
            [device setFocusPointOfInterest: point];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
        }
        
        [device unlockForConfiguration];
        [self.session commitConfiguration];
        
        pictureBeingTaken = YES;
        
        NSLog(@"Waiting second.");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(pictureBeingTaken == YES) {
                pictureBeingTaken = NO;
                [self.session beginConfiguration];
                [self session].sessionPreset = AVCaptureSessionPresetPhoto;
                
                [photoDataOutput connectionWithMediaType:AVMediaTypeVideo].videoOrientation = AVCaptureVideoOrientationLandscapeRight;
                AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                
                [self.session commitConfiguration];
                
                [device lockForConfiguration: nil];
                [device setFlashMode: AVCaptureFlashModeOn];
                [device setFocusMode: AVCaptureFocusModeLocked];
                [device unlockForConfiguration];
                
                [photoDataOutput captureStillImageAsynchronouslyFromConnection:[photoDataOutput connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                    
                    NSLog(@"Picture taken.");

                    dispatch_async(dispatch_get_main_queue(), ^{
                        // undo setting to photo mode
                        NSLog(@"Resetting camera from photo mode...");
                        [self stopCapturing];
                        [[self session] beginConfiguration];
                        
                        [self session].sessionPreset = prev_preset;
                        
                        NSError *error = nil;
                        
                        if([device lockForConfiguration:&error])
                        {
                            AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                            if([device isFocusModeSupported: AVCaptureFocusModeContinuousAutoFocus]) // unlock the focus
                            {
                                [device setFocusMode: AVCaptureFocusModeContinuousAutoFocus];
                            }
                            
                            [device unlockForConfiguration];
                        }
                        else
                        {
                            NSLog(@"Failed to configure device!");
                        }
                        
                        [[self session] commitConfiguration];
                        NSLog(@"Finished resetting camera from photo mode...");
                    });
                    
                    //Mat photoMat = [self cvMatFromSamplBGRASampleBuffer:imageDataSampleBuffer];
                    //UIImage* image = [MatcherUtil fullImageFromBuffer: imageDataSampleBuffer orientation:UIImageOrientationRight];
                    //Mat photoMat = [self grayMatFromBGRAMat:];
                    
                    [self goToStage:STAGE_PHOTO_PROCESSING];
                    //[self performSelectorInBackground:@selector(processPhotoHandImage:) withObject:image];
                    
                    FourF::Interface::ImageHandle hImage = [FourFCommonViewController imageHandleFromSampleBuffer: imageDataSampleBuffer];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        // TODO: fix oldAvVapture
                        [self processPhotoHandImageHandle:hImage withLivenessImage:hImage];
                    });
                    
                    
                    UIImage* image = [FourFCommonViewController fullImageFromBuffer: imageDataSampleBuffer orientation:UIImageOrientationRight];
                    
                    [self performSelectorInBackground:@selector(processPhotoHandImage:) withObject:image];
                }];
        }
        });
    }
    else
    {
        NSLog(@"Failed to configure device!");
        [self.session commitConfiguration];
    }
}

-(void) av_capture {
    // taking the picture
    
    NSString* prev_preset = [self session].sessionPreset;
  
    NSError *error = nil;
    
    [self.session beginConfiguration];
    
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [self session].sessionPreset = AVCaptureSessionPresetPhoto;
    
    if([device lockForConfiguration:&error])
    {
        [device setFlashMode: AVCaptureFlashModeOff];
        
        CGPoint point = CGPointMake(0.5f, 0.5f);
        
        if([device isExposurePointOfInterestSupported])
        {
            [device setExposurePointOfInterest: point];
        }
        
        /*
        if([device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
        {
            [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        }
        */
        
        [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        
        // We check for focus before this point, so might as well lock the focus here.
        if([device isFocusModeSupported:AVCaptureFocusModeLocked])
        {
            [device setFocusMode:AVCaptureFocusModeLocked];
            //[device setFocusMode:AVCaptureFocusModeAutoFocus];
        }
        
        [device unlockForConfiguration];
    }
    else
    {
        NSLog(@"Failed to configure device!");
    }
  
    //CGAffineTransform original_trans = previewLayer.affineTransform;
    //previewLayer.affineTransform = CGAffineTransformScale(original_trans, 0.5, 0.5);
    
    [photoDataOutput connectionWithMediaType:AVMediaTypeVideo].videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    
    [self.session commitConfiguration];
    
    __block void (^checkExposureBlock)() = ^void {
        
        if([device isAdjustingExposure] || [device isAdjustingFocus])
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.005 * NSEC_PER_SEC)),dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), checkExposureBlock);
        }
        else
        {
            [photoDataOutput captureStillImageAsynchronouslyFromConnection:[photoDataOutput connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer_nonflash, NSError *error1) {
                NSLog(@"Picture taken.");
                
                if([device lockForConfiguration: nil])
                {
                    [device setFlashMode: AVCaptureFlashModeOn];
                    [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
                    
                    // We check for focus before this point, so might as well lock the focus here.
                    if([device isFocusModeSupported:AVCaptureFocusModeLocked])
                    {
                        [device setFocusMode:AVCaptureFocusModeLocked];
                    }
                    
                    [device unlockForConfiguration];
                }
                else
                {
                    NSLog(@"Failed to configure device!");
                }
                
                // Unfortunately, this copy seems to be necessary. Attempts to retain the buffer after the following image capture call
                // seem to cause it to fail.
                NSLog(@"Before copy.");
                Mat nonflashImageCopy = [self cvMatFromSamplBGRASampleBuffer: imageDataSampleBuffer_nonflash];
                FourF::Interface::ImageHandle hImage_nonflash = FourF::Interface::ImageHandle::loadCvMat(nonflashImageCopy, FourF::Interface::ImageFormat::BGRA);
                NSLog(@"After copy.");
                
                [photoDataOutput captureStillImageAsynchronouslyFromConnection:[photoDataOutput connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer_flash, NSError *error2) {
                    
                    NSLog(@"Picture 2 taken.");
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self resetCameraFromPhoto:prev_preset];
                    });
                    
                    [self goToStage:STAGE_PHOTO_PROCESSING];
                
                    UIImage* image = [FourFCommonViewController fullImageFromBuffer: imageDataSampleBuffer_flash orientation:UIImageOrientationRight];
                    [self performSelectorInBackground:@selector(processPhotoHandImage:) withObject:image];
                    
                    FourF::Interface::ImageHandle hImage_flash = [FourFCommonViewController imageHandleFromSampleBuffer: imageDataSampleBuffer_flash];

                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        [self processPhotoHandImageHandle: hImage_flash withLivenessImage: hImage_nonflash];
                    });
                }];

                
            }];
            
            
        }
        
    };
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), checkExposureBlock);
}

-(void) resetCameraFromPhoto: (NSString*) prev_preset
{
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // undo setting to photo mode
    NSLog(@"Resetting camera from photo mode...");
    [self stopCapturing];
    [[self session] beginConfiguration];
    
    [self session].sessionPreset = prev_preset;
    
    NSError *error = nil;
    
    if([device lockForConfiguration:&error])
    {
        AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if([device isFocusModeSupported: AVCaptureFocusModeContinuousAutoFocus]) // unlock the focus
        {
            [device setFocusMode: AVCaptureFocusModeContinuousAutoFocus];
        }
        
        [device unlockForConfiguration];
    }
    else
    {
        NSLog(@"Failed to configure device!");
    }
    
    [[self session] commitConfiguration];
    NSLog(@"Finished resetting camera from photo mode...");
}

-(void) processPhotoHandImage:(UIImage*) image{
  
  dispatch_async(dispatch_get_main_queue(), ^{
    self.processingImage.image = image;
  });
  
}

-(void) processPhotoHandImageHandle: (FourF::Interface::ImageHandle) hImage withLivenessImage: (FourF::Interface::ImageHandle) hImageLiveness {
    [self processBGRImage: hImage isPreview: NO
                     onOK:^{
                         dispatch_sync(dispatch_get_main_queue(), ^{
                             
                             // These are flipped 'cause the image is rotated 90 degrees.
                             int width = hImage.getConstMat().rows;
                             int height = hImage.getConstMat().cols;
                             
                             [self drawProcessingRoisWithParentView:self.roisView sourceSize:CGSizeMake(height, width)];
                         });
                         
                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                             [self processPhotoHandMat: hImage withROIs: photoRois withLivenessImage: hImageLiveness];
                         });
                     }
                   onFail:^{
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [self continueByRestartingVideo];
                       });
                   }];
}

/*
-(void) processPhotoHandImageHandle: (FourF::Interface::ImageHandle) hImage {
    [self processPhotoHandImageHandle: hImage withLivenessImage: nil]; // Won't compile... yet. Need to come up with a nil analogue for ImageHandles.
}
*/



-(void) processPhotoHandMat:(FourF::Interface::ImageHandle) hImage withROIs:(const int[16]) rois withLivenessImage: (FourF::Interface::ImageHandle) hImageLiveness {
}

-(void) goToStage:(NSInteger)stage{
  @synchronized(self) {
    switch(stage){
        
      case STAGE_INIT:
        NSLog(@"STAGE_INIT");
        [self init4F];
        
        canProcessNext = NO;
        self.cancelButton.hidden = NO;
        
        self.startButton.enabled = NO;
        [self hideLivenessHint];
        
        self.bottomMessageLabel.text = @"";
        [self.progressView stop];
        
        self.cancelButton.hidden = YES;
        [self startCapturing];
        [self.spinner stopAnimating];
        
        [self hideROIs];
        
        [self onInit];
        break;
      case STAGE_CAMERA_STARTED:
        NSLog(@"STAGE_CAMERA_STARTED");
        
        [self onCameraStarted];
        
        
        self.startButton.enabled = NO;
        self.cancelButton.hidden = NO;
        
        currentStage = STAGE_CAMERA_STARTED;
        canProcessNext = YES;
        [self resetLastRois];
        break;
      case STAGE_HAND_DETECTION:
        if (currentStage != STAGE_CAMERA_STARTED && currentStage != STAGE_HAND_DETECTION && currentStage != STAGE_HAND_DETECTED)
          return;
        DebugLog(@"STAGE_HAND_DETECTION, fromStage: %d",currentStage);
        [self hideROIs];
        [self resetLastRois];
        
        canProcessNext = YES;
        self.startButton.enabled = NO;
        self.cancelButton.hidden = NO;
        
        [self hideLivenessHint];
        
        self.bottomMessageLabel.text = Local(@"Please place your hand inside the guide");
        [self showLivenessHint:Local(@"Searching hand...") textColor:nil bgColor:COLOR_LIGHT_BLUE(0.7)];
        
        
        
        break;
        
      case STAGE_HAND_DETECTED:
        if (currentStage != STAGE_CAMERA_STARTED && currentStage != STAGE_HAND_DETECTION && currentStage != STAGE_HAND_DETECTED)
          return;
            
        if (currentStage == STAGE_HAND_DETECTION)
        {
            [self macroFocusKnock];
        }
        
        DebugLog(@"STAGE_HAND_DETECTED, fromStage: %d",currentStage);
        canProcessNext = YES;
        [self showROIs];
        
        
        self.bottomMessageLabel.text = @"Hand found!";
        
        if(YES){
          [self showLivenessHint:Local(@"Keep your hand still!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
            
            const FourF::Interface::ROI::ShouldTakePictureAnswer shouldTake = FourF::Interface::ROI::shouldTakePicture();
            AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
            
            switch (shouldTake) {
                case FourF::Interface::ROI::ShouldTakePictureAnswer::OutOfFocus:
                    
                    if(firstOutOfFocusTime < 0.0) firstOutOfFocusTime = CACurrentMediaTime();
                    
                    if((CACurrentMediaTime() - firstOutOfFocusTime) < 1.5) {
                        break;
                    }
                    
                    firstOutOfFocusTime = -1.0;
                    
                    // Case fall-through if timer is longer than this, leading to the picture being taken anyway
                    // by the effect of the next case.
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::Yes:
                    if((failureMoratorium == NO) && !device.adjustingFocus){
                        [self onStart:nil];
                    }
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::RoiTooBig:
                    NSLog(@"ROIs too big.");
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Your hand is too close!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::RoiTooSmall:
                    NSLog(@"ROIs too small.");
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Your hand is too far!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::FingersTooFarApart:
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Keep your fingers together!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::FingersHigh:
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Your hand is too high!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::FingersLow:
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Your hand is too low!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::FingersFarLeft:
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Your hand is too far left!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::FingersFarRight:
                    roisColor = [UIColor redColor];
                    [self showLivenessHint:Local(@"Your hand is too far right!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
                    break;
                    
                case FourF::Interface::ROI::ShouldTakePictureAnswer::No:
                    [self drawSearchRoisWithParentView: self.roisView];
                    firstOutOfFocusTime = -1.0;
                    break;
            }
            
        }else{
          [self drawSearchRoisWithParentView:self.roisView];
          self.startButton.enabled = YES;
          [self showLivenessHint:Local(@"Tap to capture!") textColor:nil bgColor:COLOR_DETECTED_YELLOW_ALPHA(0.5)];
        }
        
        break;
      case STAGE_PHOTO_STARTED:
        NSLog(@"STAGE_PHOTO_STARTED");
        self.cancelButton.hidden = YES;
        self.startButton.enabled = NO;
        canProcessNext = NO;
        self.bottomMessageLabel.text = @"Capturing...";
        [self showLivenessHint:Local(@"Keep your hand still!") textColor:nil bgColor:COLOR_CAPTURE_GREEN_ALPHA(0.5)];
        //      [self hideROIs];
        //      [self hideLivenessHint];
        break;
      case STAGE_PHOTO_PROCESSING:
        NSLog(@"STAGE_PHOTO_PROCESSING");
        canProcessNext = NO;
        [self hideLivenessHint];
        [self.progressView startLoader];
        self.bottomMessageLabel.text = @"Processing...";
        break;
    }
    currentStage = stage;
  }
}



#define CAPTURE_FRAMES_PER_SECOND 30
#pragma mark - av foundation
- (void)startCapturing
{
  //  self.circularProgress.progress = 0.0;
  
  AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
  AVCaptureDeviceInput* deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:NULL];
  
  
  NSDictionary* settings = @{(id)kCVPixelBufferPixelFormatTypeKey:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]};
  AVCaptureVideoDataOutput* dataOutput = [[AVCaptureVideoDataOutput alloc] init];
  dataOutput.videoSettings = settings;
  [dataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
  
  NSDictionary* photosettings = @{(id)kCVPixelBufferPixelFormatTypeKey:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]};
  photoDataOutput = [[AVCaptureStillImageOutput alloc] init];
  photoDataOutput.outputSettings = photosettings;
  
  if([device lockForConfiguration:nil])
  {

      device.activeVideoMinFrameDuration = CMTimeMake(2, CAPTURE_FRAMES_PER_SECOND);
      device.activeVideoMaxFrameDuration = CMTimeMake(2, CAPTURE_FRAMES_PER_SECOND);
      NSLog(@"frame rates: %@",device.activeFormat.videoSupportedFrameRateRanges);
      device.torchMode = AVCaptureTorchModeOn;
      //  device.flashMode = AVCaptureFlashModeOn;
        
      //set cam params
        CGPoint point = CGPointMake(0.5f, 0.67f); // focus and exposure point
        if([device isExposurePointOfInterestSupported])
        {
            [device setExposurePointOfInterest: point];
        }
        
        if([device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
        {
            [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        }
        
        if([device isAutoFocusRangeRestrictionSupported])
        {
            [device setAutoFocusRangeRestriction:AVCaptureAutoFocusRangeRestrictionNear];
        }
        
        /*
        if([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
        {
            [device setFocusPointOfInterest: point];
            [device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        }
        */

      [device unlockForConfiguration];
  }
  else
  {
       NSLog(@"Failed to configure device!");
  }
    
  self.session = [[AVCaptureSession alloc] init];
  
  [self.session addInput:deviceInput];
  [self.session addOutput:dataOutput];
  [self.session addOutput:photoDataOutput];
  
  //add preview
  
  previewLayer = [AVCaptureVideoPreviewLayer  layerWithSession:self.session];
  previewLayer.frame = self.videoView.bounds;
  DebugLog(@"Video View bounds: %@ PreviewLayer frame: %@", NSStringFromCGRect(self.videoView.bounds), NSStringFromCGRect(previewLayer.frame));
  [self.videoView.layer addSublayer:previewLayer];
  
  //end add preview
  
  self.session.sessionPreset = AVCaptureSessionPreset640x480;//AVCaptureSessionPreset1280x720;
  
  NSLog(@"CAN SET TO PHOTO: %@",[self.session canSetSessionPreset:AVCaptureSessionPresetPhoto] ? @"YES" : @"NO");
  
  AVCaptureConnection *videoConnection = NULL;
  
  [self.session beginConfiguration];
  
  for ( AVCaptureConnection *connection in [dataOutput connections] )
  {
    for ( AVCaptureInputPort *port in [connection inputPorts] )
    {
      if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
      {
        videoConnection = connection;
        
      }
    }
  }
  if([videoConnection isVideoOrientationSupported]) // **Here it is, its always false**
  {
    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
  }
  
  [self.session commitConfiguration];
  
  [self.session startRunning];
  
  self.roisView.hidden = NO;
  self.overlayView.hidden = NO;
    
  [self macroFocusKnock];
}

- (void) macroFocusKnock {
// Force the lens to reset to a macro position - this is an attempt to knock it out of the
    // bad focus "loops" we've seen.
  AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    if([device lockForConfiguration: &error])
    {
         [device setFocusModeLockedWithLensPosition: 0.0f completionHandler:^(CMTime syncTime) {
             NSLog(@"Lens position set, reenabling continuous autofocus.");
             AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
             if([device lockForConfiguration:nil])
             {
                 
                 if([device isAutoFocusRangeRestrictionSupported])
                 {
                     [device setAutoFocusRangeRestriction:AVCaptureAutoFocusRangeRestrictionNear];
                 }
                 
                 if([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
                 {
                     //CGPoint point = CGPointMake(0.5f, 0.67f); // focus and exposure point
                     //[device setFocusPointOfInterest: point];
                     [device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
                 }
                 
                [device unlockForConfiguration];
             }
             else
             {
                 NSLog(@"Failed to configure device!");
             }
         }];
        
        [device unlockForConfiguration];
    }
    else
    {
        NSLog(@"Failed to configure device!");
    }
}

- (void) startHandSearch {
  canProcessNext = YES;
}

- (void) stopCapturing
{
  canProcessNext = NO;
  [previewLayer removeFromSuperlayer];
  if([self.session isRunning]){
    [self.session stopRunning];
    AVCaptureInput* input = [self.session.inputs objectAtIndex:0];
    [self.session removeInput:input];
    for ( AVCaptureOutput* output in self.session.outputs){
      [self.session removeOutput:output];
    }
    [previewLayer removeFromSuperlayer];
  }
  dispatch_sync(backgroundROISQueue,^{});
}


-(void) goToHandDetection {
  dispatch_async(dispatch_get_main_queue(), ^{
  });
}



- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
  //  [connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
  
  if (currentStage == STAGE_INIT){
    [self goToStage:STAGE_CAMERA_STARTED];
  }
  if (canProcessNext && currentStage < STAGE_PHOTO_STARTED) {
      
    // This conversion is a perf bottleneck, but can't avoid it without copying the sampleBuffer.
    //__block Mat bgr = [self bgrMatFromSampleBGRASampleBuffer:sampleBuffer];
      
      FourF::Interface::ImageHandle hImage = [FourFCommonViewController imageHandleFromSampleBuffer: sampleBuffer];
      
    // __block Mat y = [self grayMatFromBGRAMat:[self cvMatFromSamplBGRASampleBuffer:sampleBuffer]];
    canProcessNext = NO;
    dispatch_async(backgroundROISQueue, ^{
      
      [self processBGRImage: hImage isPreview: YES
                             onOK:^{
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                 [self goToStage:STAGE_HAND_DETECTED];
                               });
                               
                             }
                           onFail:^{
                             dispatch_async(dispatch_get_main_queue(), ^{
                               [self goToStage:STAGE_HAND_DETECTION];
                             });
                           }];
      
    });
  }
}


-(void) processBGRImage:(FourF::Interface::ImageHandle) h isPreview: (BOOL) isPreview onOK:(voidBlock) onOK onFail:(voidBlock) onFail{
  
  FourF::Interface::ImagedHand hand = useLeftHand ? FourF::Interface::ImagedHand::Left : FourF::Interface::ImagedHand::Right;

  // NSLog(@"COMMON USE LEFT HAND: %@",useLeftHand?@"YES":@"NO");
  //[MatcherUtil writePNGImage:UIImageFromCVMat(gray) toFile:@"GrayImage.png"];
  //[MatcherUtil writePNGImage:UIImageFromCVMat(y) toFile:@"yImage.png"];
    
  int rois_score;
  bool success = false;
    
  FourF::GetImageInfoResult result = FourF::GetImageInfoResult::FAIL_NOTIMPLEMENTED;
    FourF::Interface::ROI::SubmitFrameResult previewResult = FourF::Interface::ROI::SubmitFrameResult::FAIL_NOT_INITIALISED;
    
  const FourF::Lux lightLocation = [[UIScreen mainScreen] brightness] > 0.8 ? FourF::Lux::HIGH : FourF::Lux::LOW;
    
  // Separated the code-paths for preview and for final photo submit
  // This is to allow different behaviour in the library for preview
  // For the photos we always use ground-truth cascade-generated ROIs, for video previews we may use
  // optical-flow tracking and post-processing to smooth ROIs
  if(isPreview == YES) {
      previewResult = FourF::Interface::ROI::submitFrameForROIs(h, hand, lightLocation);
      success = previewResult == FourF::Interface::ROI::SubmitFrameResult::SUCCESS;
  } else {
      if (true /*FourF::Interface::LoadRGBMat(img) */){
          result = FourF::Interface::getROIs(h, lightLocation, hand, photoRois, rois_score);
          if (FourF::GetImageInfoResult::SUCCESS == result && FourF::Interface::isRoiThresholdPass(rois_score)) {
              success = true;
              
              int focus_score;
              
              FourF::GetImageInfoResult focusResult = FourF::Interface::getFocus(h, photoRois, focus_score);
              
              if(focusResult != FourF::GetImageInfoResult::SUCCESS || !FourF::Interface::isFocusScorePass(focus_score, false))
              {
                  NSLog(@"Rejected image because of bad focus.");
                  success = false;
                  failureMoratorium = YES;
                  [self macroFocusKnock];
                  
                  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                      failureMoratorium = NO;
                  });
              }
          }
          
      }else{
          NSLog(@"Error on LoadGreyscaleMat");
          success = false;
      }
  }
  
  if(success) {
      onOK();
  } else  {
      
      NSString* humanReadableStatus = @"";
      
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ ":" TOSTRING(__LINE__)
      
      if(isPreview == YES) {
          
          std::string error_string = FourF::Interface::ROI::to_string(previewResult);
          humanReadableStatus = [NSString stringWithUTF8String:error_string.c_str()];
          
          /*
          switch(previewResult) {
            case FourF::Interface::ROI::SubmitFrameResult::FAIL_INVALID_HAND:
              humanReadableStatus = @"FAIL_INVALID_HAND";
              break;
            default:
              humanReadableStatus = @"Preview failure for which the string hasn't been written. See " AT;
              break;
          }
          */
      } else {
          switch (result) {
            case FourF::GetImageInfoResult::FAIL_IMGPROCESSING:
              humanReadableStatus = @"FAIL_IMGPROCESSING";
              break;
            case FourF::GetImageInfoResult::FAIL_ROINULLPTR:
              humanReadableStatus = @"FAIL_ROINULLPTR";
              break;
            case FourF::GetImageInfoResult::FAIL_ROIS_INVALID:
              humanReadableStatus = @"FAIL_ROIS_INVALID";
              break;
            case FourF::GetImageInfoResult::FAIL_NOTIMPLEMENTED:
              humanReadableStatus = @"FAIL_NOTIMPLEMENTED";
              break;
            case FourF::GetImageInfoResult::FAIL_GREYSCALE_NOT_LOADED:
              humanReadableStatus = @"FAIL_GREYSCALE_NOT_LOADED";
              break;
            case FourF::GetImageInfoResult::FAIL_RGB_NOT_LOADED:
              humanReadableStatus = @"FAIL_RGB_NOT_LOADED";
              break;
            default:
              break;
          }
      }
      
      NSLog(@"ROIs error: %d %@", result, humanReadableStatus);
      onFail();
  }
}

-(void) continueByRestartingVideo {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self goToStage:STAGE_INIT];
  });
}



//-(void) setProgress:(CGFloat)progress duration:(CGFloat) duration {
//  [self.circularProgress startLoader];
////  [self.circularProgress setProgress:progress timing:TPPropertyAnimationTimingLinear duration:duration delay:0];
//}

-(void) showLivenessHint:(NSString*)text textColor:(UIColor*) textColor bgColor:(UIColor*) bgColor {
  self.centerHintLabel.hidden = NO;
  self.centerHintLabel.text = text;
  self.centerHintLabel.textColor = nil == textColor ? [UIColor whiteColor] : textColor;
  self.centerHintLabel.backgroundColor = nil == bgColor ? [UIColor blackColor] : bgColor;
}

-(void) hideLivenessHint{
  self.centerHintLabel.hidden = YES;
}

-(void) showROIs {
//  for(UIView* roi in roisViews){
//    roi.hidden = NO;
//  }

  roisShouldDraw = YES;
    
  for(CALayer* layer in roisLayers){
    layer.opacity = 0.5;
  }
}

-(void) hideROIs {
  DebugLog(@"HIDE ROIS");
//  for(UIView* roi in roisViews){
//    roi.hidden = YES;
//  }
    
  roisShouldDraw = NO;
  for(CALayer* layer in roisLayers){
    layer.opacity = 0.0;
  }
}

-(void) resetLastRois {
    
    FourF::Interface::ROI::reset();
}

-(void) roisDraw {
    int rois_score;
    FourF::Interface::ROI::getROIArray(rois, rois_score);
    
    if(roisShouldDraw)
    {
        [self drawRoisFromSelf];
    }
}

-(void)drawSearchRoisWithParentView:(UIView*)parentView {
  DebugLog(@"DRAW SEARCH ROIS");
  roiSourceSize = CGSizeMake(640.0,480.0);
  roisColor = [UIColor blueColor];
  //[self drawRoisFromSelf];
}

-(void)drawFreezedRoisWithParentView:(UIView*)parentView {
  DebugLog(@"DRAW FREEZE ROIS");
  roiSourceSize = CGSizeMake(640.0,480.0);
  roisColor = [UIColor yellowColor];
}

-(void)drawProcessingRoisWithParentView:(UIView*)parentView sourceSize:(CGSize) sourceSize {
  DebugLog(@"DRAW PROCESSING ROIS");
  roiSourceSize = sourceSize;
  roisColor = [UIColor greenColor];
    
  // Probably a bit of a hack.
  roisShouldDraw = NO;
  std::copy(photoRois, photoRois + 16, rois);
  [self drawRoisFromSelf];
}

-(void)drawRoisFromSelf {
    [self drawRoisWithParentView:self.roisView sourceSize: roiSourceSize color: roisColor];
}

-(void)drawRoisWithParentView:(UIView*)parentView sourceSize:(CGSize)sourceSize color:(UIColor*) color
{
  CGFloat CAMW = sourceSize.width;// 640.0;
  CGFloat CAMH = sourceSize.height;// 480.0;
  
  CGRect parentFrame = parentView.bounds;
  CGFloat displayHRatio = CAMW / CGRectGetHeight(parentFrame);
  CGFloat displayVRatio = CAMH / CGRectGetWidth(parentFrame);
  
  
  //NSLog(@"Parent Frame: %@ Display Ratios: H: %f V: %f ", NSStringFromCGRect(parentFrame), displayHRatio, displayVRatio);
  
  
  for (int i=0; i<16; i+=4) {
    
    //CGFloat rotatedMirroredX = useLeftHand ? CAMH - rois[i+3] : rois[i+1];
    CGFloat rotatedMirroredX = CAMH - rois[i+3];
    CGFloat rotatedMirroredY = rois[i];
    
    CGFloat w = rois[i+3] - rois[i+1];
    CGFloat h = rois[i+2] - rois[i];
    
    CGRect scaledRotatedMirroredRect = CGRectMake(rotatedMirroredX / displayHRatio, rotatedMirroredY / displayVRatio, w / displayHRatio, h / displayVRatio);
    
    //NSLog(@"FINGER: %i\nROIS left: %i top: %i right: %i bottom: %i ",i/4+1,rois[i],rois[i+1],rois[i+2],rois[i+3]);
    //NSLog(@"ScaledRotatedMirrored rect: %@ ", NSStringFromCGRect(scaledRotatedMirroredRect));

    CAShapeLayer* layer = (CAShapeLayer*)roisLayers[i/4];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:scaledRotatedMirroredRect cornerRadius:scaledRotatedMirroredRect.size.height / 2.0];
    [path closePath];
    layer.fillColor = color.CGColor;
    layer.path = path.CGPath;
  }
  
}


-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat
{
  NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
  CGColorSpaceRef colorSpace;
  
  if (cvMat.elemSize() == 1) {
    colorSpace = CGColorSpaceCreateDeviceGray();
  } else {
    colorSpace = CGColorSpaceCreateDeviceRGB();
  }
  
  CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
  // Creating CGImage from cv::Mat
  CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                      cvMat.rows,                                 //height
                                      8,                                          //bits per component
                                      8 * cvMat.elemSize(),                       //bits per pixel
                                      cvMat.step[0],                            //bytesPerRow
                                      colorSpace,                                 //colorspace
                                      kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                      provider,                                   //CGDataProviderRef
                                      NULL,                                       //decode
                                      false,                                      //should interpolate
                                      kCGRenderingIntentDefault                   //intent
                                      );
  
  
  // Getting UIImage from CGImage
  UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
  CGImageRelease(imageRef);
  CGDataProviderRelease(provider);
  CGColorSpaceRelease(colorSpace);
  
  return finalImage;
}

/*
- (void)convertYUVSampleBuffer:(CMSampleBufferRef)buf toGrayscaleMat:(cv::Mat &)mat {
  CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(buf);
  
  // lock the buffer
  CVPixelBufferLockBaseAddress(imgBuf, 0);
  
  // get the address to the image data
  void *imgBufAddr = CVPixelBufferGetBaseAddressOfPlane(imgBuf, 0);
  
  // get image properties
  int w = (int)CVPixelBufferGetWidth(imgBuf);
  int h = (int)CVPixelBufferGetHeight(imgBuf);
  
  // create the cv mat
  mat.create(h, w, CV_8UC1);              // 8 bit unsigned chars for grayscale data
  memcpy(mat.data, imgBufAddr, w * h);    // the first plane contains the grayscale data
                                          // therefore we use <imgBufAddr> as source
  
  // unlock again
  CVPixelBufferUnlockBaseAddress(imgBuf, 0);
}

-(Mat) bgrMatFromSampleBGRASampleBuffer:(CMSampleBufferRef)buf {
    
    CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(buf);
    
    // lock the buffer
    CVPixelBufferLockBaseAddress(imgBuf, 0);
    
    // get the address to the image data
    void *imgBufAddr = CVPixelBufferGetBaseAddressOfPlane(imgBuf, 0);
    
    // get image properties
    int w = (int)CVPixelBufferGetWidth(imgBuf);
    int h = (int)CVPixelBufferGetHeight(imgBuf);
    // NSLog(@"Convert buff to color mat %dx%d", w, h);
    // create the cv mat
    Mat colorMat(h, w, CV_8UC4, imgBufAddr);
    
    //memcpy(result.data, imgBufAddr, w * h * 4);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source
    
    Mat bgrMat;
    cvtColor(colorMat,bgrMat,CV_BGRA2BGR);
    // bgrMat = colorMat;
    
    // unlock again
    CVPixelBufferUnlockBaseAddress(imgBuf, 0);
    
    return bgrMat;
}

-(Mat) greyMatFromSampleBGRASampleBuffer:(CMSampleBufferRef)buf {
    
    CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(buf);
    
    // lock the buffer
    CVPixelBufferLockBaseAddress(imgBuf, 0);
    
    // get the address to the image data
    void *imgBufAddr = CVPixelBufferGetBaseAddressOfPlane(imgBuf, 0);
    
    // get image properties
    int w = (int)CVPixelBufferGetWidth(imgBuf);
    int h = (int)CVPixelBufferGetHeight(imgBuf);
    NSLog(@"Convert buff to color mat %dx%d", w, h);
    // create the cv mat
    Mat colorMat(h, w, CV_8UC4, imgBufAddr);
    
    //memcpy(result.data, imgBufAddr, w * h * 4);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source
    
    Mat grayMat;
    cvtColor(colorMat,grayMat,CV_BGR2GRAY);
    
    // unlock again
    CVPixelBufferUnlockBaseAddress(imgBuf, 0);
    
    return grayMat;
}
 
*/

-(Mat) cvMatFromSamplBGRASampleBuffer:(CMSampleBufferRef)buf {
  CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(buf);
  
  // lock the buffer
  CVPixelBufferLockBaseAddress(imgBuf, 0);
  
  // get the address to the image data
  void *imgBufAddr = CVPixelBufferGetBaseAddressOfPlane(imgBuf, 0);
  
  // get image properties
  int w = (int)CVPixelBufferGetWidth(imgBuf);
  int h = (int)CVPixelBufferGetHeight(imgBuf);
  NSLog(@"Convert buff to color mat %dx%d", w, h);
  // create the cv mat
  Mat result(h, w, CV_8UC4);
  
  
  memcpy(result.data, imgBufAddr, w * h * 4);    // the first plane contains the grayscale data
                                                 // therefore we use <imgBufAddr> as source
  
  // unlock again
  CVPixelBufferUnlockBaseAddress(imgBuf, 0);
  return result;
}
/*
-(Mat) grayMatFromBGRAMat:(Mat) colorMat {
  Mat grayMat;
  cvtColor(colorMat,grayMat,CV_BGR2GRAY);
  return grayMat;
}
 
*/

@end
