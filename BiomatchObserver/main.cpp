//
//  main.cpp
//  BiomatchObserver
//
//  Created by Andrew Simpson on 10/11/2016.
//  Copyright © 2016 aiusepsi. All rights reserved.
//

#include <iostream>
#include <memory>
#include <grpc++/grpc++.h>
#include "biomatch.grpc.pb.h"
#include "MappedFileData.h"
#include <thread>

int main(int argc, const char * argv[]) {
    // Set up RPC channel
    grpc::SslCredentialsOptions ssl_options;
    MappedFileData root_cert("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/pki/ca.crt");
    ssl_options.pem_root_certs = std::string(reinterpret_cast<char*>(root_cert.getData()), root_cert.getSize());
    auto channel = grpc::CreateChannel("localhost:50051", grpc::SslCredentials(ssl_options));
    
    std::unique_ptr<Veridium::PoliceDemo::Observer::Stub> stub = Veridium::PoliceDemo::Observer::NewStub(channel);
    
    while(true)
    {
        grpc::ClientContext ctx;
        Veridium::PoliceDemo::ObserveRequest request;
        std::unique_ptr<grpc::ClientReaderInterface<Veridium::PoliceDemo::Observation>> observation_reader = stub->StartObserving(&ctx, request);
        Veridium::PoliceDemo::Observation observation;
        
        std::cout << "Starting read..." << std::endl;
        while(observation_reader->Read(&observation))
        {
            std::cout << "Got observation!" << std::endl;
            
            // Show some sort of UI here!
            std::cout << "Claimed identity: " << observation.name() << " " << observation.dob() << std::endl;
            std::cout << "Matched: " << (observation.identify_response().success() ? "YES" : "NO") << std::endl;
        }
        
        grpc::Status status = observation_reader->Finish();
        
        if(status.ok())
        {
            std::cout << "Finished OK!" << std::endl;
        }
        else
        {
            std::cerr << "Finished with error." << std::endl;
            std::cerr << "Error code: " << status.error_code() << " " << status.error_message() << std::endl;
            std::cerr << "Retrying in a few seconds..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(5));
        }
    }
}
