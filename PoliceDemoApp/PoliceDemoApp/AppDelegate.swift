//
//  AppDelegate.swift
//  PoliceDemoApp
//
//  Created by Andrew Simpson on 11/11/2016.
//  Copyright © 2016 Veridium. All rights reserved.
//

import UIKit
import VeridiumCore
import Veridium4FBiometrics
import VeridiumDefault4FUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        VeridiumSDK.setup("SQAAAAAAAABAAAAAAAAAAEEBAAAAAAAAUAEAAAAAAAADAHR97ZCEh7ItN998OufQ3DaQbKVZIo5Hv/9uIzMQkiaWiMPSpiO3Rg1xXOTzBHgX7TDA5uz4Wsg2q16FYQgu2ZsAh938Pep1RltwTCpAwTycw+4Y04JHYTNLUxU7lSlnoBnglW/EHoEZDRkd9bbejWMtYhJtydauH4gz4U4XWYLYvnOUeq8gbZO4hosmteggLkloszReaxJcffR88R3LWTF1QbK54XhkI2EIolXyqlhNPrU9ZUzEqx+MRgV957FbzuHWMQ2yhUp6z/eFEYjpDcQVsRJSIIJBntJrGmqU1nseNkM14aw6FCVNHgAXgJVjYfHHtxISLJ27tEpGozJslEsDoEZALLoxmTT3YyEIRxx7u4TOR8PmvDsWDAezzBTBwZfmcBraLl6vL0vAgt3k6PWH/9ijZyHzl2EVO+Tt3CXUS8bjWPQws3eZJasvYYXEgbGcsXB7Ajn89rnZz1dr3758wEGWJohNGsD1PQkHcWBOXxBhJV++FqDT6o5BtwKxd2cDLPjmcmwBXA5nftT1DzzzPmdc96m+O6XzRv07i8EmzD9rg8zUhE+72UPMqXDfw6bRnXTpwdl2qcgcp4Th+gtboTwAnEWuwiQ6ihzhNHFYCXbw5NNxYQ==")
        
        VeridiumSDK.shared().registerDefault4FEnroller()
        VeridiumSDK.shared().registerDefault4FAuthenticator()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

