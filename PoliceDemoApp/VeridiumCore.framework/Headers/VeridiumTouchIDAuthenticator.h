//
//  BiometricsTouchIDAuthenticator.h
//  VeridiumCore
//
//  Created by razvan on 11/25/15.
//  Copyright © 2015 HoyosLabs. All rights reserved.
//

#import "VeridiumBiometricsProtocols.h"

#define kVeridiumBiometricEngineNameTouchID @"TOUCHID"
/**
 *  BiometricsAuthenticatorProtocol implementation for TouchID
 */
@interface VeridiumTouchIDAuthenticator: NSObject<VeridiumBioAuthenticator>
/*!
 @return `true` if TouchID is available on the user's device
 */
@end
