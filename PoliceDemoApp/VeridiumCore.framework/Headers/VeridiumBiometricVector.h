//
//  VeridiumBiometricVector.h
//  VeridiumCore
//
//  Created by razvan on 5/11/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kVeridiumBiometricVectorFormatFullVector @"FULL_VECTOR"
#define kVeridiumBiometricVectorFormatEncryptedVisualCrypto @"ENCRYPTED_VC"

/*!
 Data class for holding a biometric vector and its metadata.
 */
@interface VeridiumBiometricVector : NSObject

/*!
 Vector type - produced by the biometric engine (and is equal with the engine name)
 */
@property (readonly) NSString* type;

/*!
 Vector version - produced by the biometric engine
 */
@property (readonly) NSString* version;

/*!
 Vector format - custom string interpreted by the biometric engine
 */
@property (readonly) NSString* format;

/*!
 The actual binary data of the biometric vector (encrypted)
 */
@property (readonly) NSData* biometricData;


/*!
 Convenience property for getting the vector with its metadata in a JSON format
 */
@property (readonly) NSString* asJSON;

/*!
 Convenience property for getting the vector with its metadata as a NSDictionary
 */
@property (readonly) NSDictionary<NSString*,id>* asDictionary;


/*!
 Designated initializer
 
 @param type                   type
 @param version                version
 @param format                 format
 @param encryptedBiometricData biometric data
 
 @return The initialized vector
 */
-(instancetype)initWithType:(NSString*) type
                    version:(NSString*) version
                     format:(NSString*) format
              biometricData:(NSData*) encryptedBiometricData;

@end

/*!
 Extension of the BiometricVector class that is used to store the public half of a split vector together with its `matchingPairTag`
 
 When a vector is split, one half (aka the "public half") goes to the server along with a "tag" (actually a hash) of its matching private half that remains on the mobile device
 
 This "tag" is later used during serverside authentication to validate and select the corresponding half of a vector sent by the mobile device
 */
@interface VeridiumBiometricVectorWithMatchingPairTag : VeridiumBiometricVector

/*!
 The hash of the private half of this biometric vector (its "pair")
 */
@property (readonly) NSString* matchingPairTag;

/*!
 Designated initializer
 
 @param type                   type
 @param version                version
 @param format                 format
 @param encryptedBiometricData biometric data
 @param matchingPairTag        matching pair tag
 
 @return The initialized vector
 */
-(instancetype)initWithType:(NSString*) type
                    version:(NSString*) version
                     format:(NSString*) format
              biometricData:(NSData*) encryptedBiometricData
           matchingPairTag:(NSString*) matchingPairTag;

@end


/*!
 Utility class for holding a collection of multiple vectors from different engines. Used by the enrollment/authentication aggregation helpers.
 */
@interface VeridiumAggregatedBiometricVector : VeridiumBiometricVector

/*!
 Adds a vector to the collection
 
 @param newVector a new vector
 */
-(void) addVector:(VeridiumBiometricVector*) newVector;

/*!
 Returns all vectors in the collection
 */
@property (readonly) NSArray<VeridiumBiometricVector*>* allVectors;
/*!
 Returns all vectors in the collection serialized as an array of dictionaries (for easier serialization)
 */
@property (readonly) NSArray<NSDictionary<NSString*,id>*>* asArrayOfDictionaries;

@end
