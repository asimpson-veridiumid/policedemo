//
//  VeridiumSDK+TouchID.h
//  VeridiumCore
//
//  Created by Catalin Stoica on 06/09/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import "VeridiumSDK.h"

@interface VeridiumSDK (TouchIDAdditions)

@property (readonly) BOOL touchIDCanBeAttempted;
@property (readonly) BOOL touchIDAvailable;
@property (readonly) BOOL touchIDEnrolled;
@property (readonly) BOOL touchIDPasscodeSet;

-(void) checkTouchID;

@end
