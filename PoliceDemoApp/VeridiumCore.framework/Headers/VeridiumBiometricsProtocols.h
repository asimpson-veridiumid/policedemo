//
//  BiometricsProtocols.h
//  VeridiumCore
//
//  Created by razvan on 11/23/15.
//
//

#import "VeridiumUtils.h"
#import "VeridiumBiometricVector.h"
#import "VeridiumAccount.h"

#pragma mark Biometric interface protocols

/*!
 Protocol implemented by external biometric matching providers like the server side BOPS matcher
 
 An implementor of this protocol can be configured for an authentication engine that supports it via [VeridiumBioAuthenticator configureExternalMatchProvider:] and its implemented methods will be called during the matching process by the authenticator
 */
@protocol VeridiumBioMatchExternalProvider <NSObject>

@optional
/*!
 Ask the external mathching provider to perform a biometric match against a locally stored splitted enrollment vector asynchronously
 
 The whenDone completion block aside from the boolean outcome receives a NSData argument that some biometric engines can use to provide an updated private enrollment vector half (e.g. 4F updates its enrollment vector during authentication to improve the
 
 If you integrate a 3rd party biometric into VeridiumSDK you can use this data param to update the enrollment vector and must handle the saving inside your VeridiumBioAuthenticator implementation
 
 @param toBeVerified   the VeridiumBiometricVector to be verified (this is provided
 @param enrolledVector the enrolled VeridiumBiometricVector stored on the device
 @param whenDone       completion callback boolAndDataBlock; receives the matching outcome  as a boolean and optionally the updated private half of the enrollment biometric data
 @param onError   the error callback stringBlock; receives the error message
 */
-(void) performMatchOf:(VeridiumBiometricVector* _Nonnull) toBeVerified against:(VeridiumBiometricVector* _Nonnull) enrolledVector whenDone:(boolAndDataBlock _Nonnull) whenDone onError:(stringBlock _Nonnull) onError;

/*!
 Ask the external matching provider to perform a biometric match against a remotely stored full enrollment vector
 
 This method is to be implemented by the integrations where storage of the enrollment vector is assured by your app (eg. store the vector on a server)
 
 
 @param toBeVerified   the VeridiumBiometricVector to be verified (this is provided
 @param whenDone       completion callback boolBlock; receives the matching outcome  as a boolean
 @param onError   the error callback stringBlock; receives the error message
 */
-(void) performMatchOf:(VeridiumBiometricVector* _Nonnull) toBeVerified whenDone:(boolBlock _Nonnull) whenDone onError:(stringBlock _Nonnull) onError;

@end


/*!
 Protocol adopted by an external storage system for updating an externally stored splitted enrollment biometric vector
 */
@protocol VeridiumBioMatchPublicVectorUpdateHandler <NSObject>

/*!
 Ask the external storage to update the public half of a splitted enrollment biometric vector asynchronously
 
 @param publicVector the VeridiumBiometricVectorWithMatchingPairTag instance that holds the public biometric vector half and a tag (a hash) of the private half for pairing them later during matching
 @param onSuccess the success callback voidBlock
 @param onError   the error callback stringBlock; receives the error message
 */
-(void) updateBiometricVector:(VeridiumBiometricVectorWithMatchingPairTag* _Nonnull) publicVector onSuccess:(voidBlock _Nonnull) onSuccess onError:(stringBlock _Nonnull) onError;

@end

/*!
 Protocol adopted by biometric engines to provide support for biometric enrollment
 
 Biometric engines usually implement also the BioAuthenticator protocol for handling authentication.
 
 However not all of them do, e.g. `TouchID` whose enrollment is handled by the system
 
 */
@protocol VeridiumBioEnroller <NSObject>


/*!
 The engine name property uniquely identifies biometric engines
 */
@property (readonly, nonatomic, nonnull) NSString* engineName;


///---------------------
/// @name Enrollment
///---------------------

/*!
 Ask the biometric engine to perform biometric enrollment
 
 @param onSuccess the success callback voidBlock
 @param onFail    the fail callback stringBlock; receives the failure reason
 @param onCancel  the cancell callback voidBlock
 @param onError   the error callback stringBlock; receives the error message
 */
-(void) enroll:(voidBlock _Nonnull) onSuccess
        onFail:(stringBlock _Nonnull) onFail
      onCancel:(voidBlock _Nonnull) onCancel
       onError:(stringBlock _Nonnull) onError;


///---------------------
/// @name Post enrollment commiting
///---------------------

/*!
 Ask the biometric engine to save the enolled biometric data (after successful enrollment)
 
 This method is called is rarely called directly since [AccountService's registerAccount]([AccountService registerAccount:onSuccess:onFail:onCancel:]) coordinates the enrollment for multiple engines and calls this internally
 
 @param dataStore the KeyValueStore instance where the enrolled biometric data is to be stored
 */
-(void) commitEnrollment:(id<VeridiumKeyValueStore> _Nonnull) dataStore;//TO DO change it to VeridiumKeyChainValueStore
@optional

///---------------------
/// @name Vector splitting support
///---------------------

/*!
 Engines that support BOPS server-side matching expose this property as a way for supplying the public part of the biometric enrollment that is used during BOPSAccount registration.
 
 The vector splitting operation splits the enrolled vector data (using visual cryptography) into a public-private matching pair.
 
 Since only the public part goes to the server this public half of the biometric vector is bundled with a hash of the private half that is also transmitted to the server backend to be used for proper pairing during server-side authentication
 
 @warning This property is not intended to be called directly from the client app. The vector splitting is used internally be the [BOPSAccountService registerAccount]([BOPSAccountService registerAccount:onSuccess:onFail:onCancel:])
 */
@property (readonly, nullable) VeridiumBiometricVectorWithMatchingPairTag* publicVector;

@end

/*!
 Protocol adopted by biometric engines to provide authentication support
 
 Biometric engines usually implement also the BioAuthenticator protocol for handling authentication.
 
 However not all of them do, e.g. `TouchID` whose enrollment is handled by the system
 */
@protocol VeridiumBioAuthenticator <NSObject>

/*!
 The engine name property uniquely identifies biometric engines
 */
@property (readonly, nonatomic, nonnull) NSString* engineName;

/*!
 Ask the engine whether it can perform an authentication for a VeridiumAccount (i.e. the account has biometric vectors enrolled for this engine)
 
 @param account the account
 
 @return `true` if the account has biometric data enrolled
 */
-(BOOL) canAuthenticateForAccount:(VeridiumAccount* _Nonnull) account;

/*!
 Ask the biometric engine to perform the biometric authentication
 
 @param reason         the authentication reason
 @param onSuccess the success callback voidBlock
 @param onFail    the fail callback stringBlock; receives the failure reason
 @param onCancel  the cancell callback voidBlock
 @param onError   the error callback stringBlock; receives the error message
 */
-(void) authenticate:(NSString* _Nullable)reason
           onSuccess:(voidBlock _Nonnull) onSuccess
              onFail:(stringBlock _Nonnull) onFail
            onCancel:(voidBlock _Nonnull) onCancel
             onError:(stringBlock _Nonnull) onError;

@optional

/*!
 Configures the VeridiumKeyValueStore instance that holds the enrollment data
 
 @param dataStore the data store
 */
-(void) configureDataStore:(id<VeridiumKeyValueStore> _Nonnull) dataStore;

/*!
 Ask the engine to provide the enrollment vector private half's tag to be used during the external matching for pairing with the public half
 
 **_Note_** Only for engines that support vector splitting
 
 @param account the VeridiumAccount instance
 
 @return the private vector half's tag
 */
-(NSString* _Nonnull) privateVectorTag:(VeridiumAccount* _Nonnull) account;


/*!
 Configure an external matcher (eg. BOPS server-side matching)
 
 @param externalMatcher the VeridiumBioMatchExternalProvider instance
 */
-(void)configureExternalMatchProvider:(id<VeridiumBioMatchExternalProvider> _Nonnull) externalMatcher;

/*!
 Configure the enrollment biometric vector public half for local matching on engines that support vector splitting
 
 This is used for local matching when the enrollment vector is splitted (eg. BOPS client-side authentication)
 
 @param publicVector the public biometric vector raw data
 */
-(void)configurePublicVector:(NSData* _Nonnull)publicVector;

/*!
 Configures a enrollment biometric vector public half async storage that handles enrollment vector update (for engines that support it - eg 4F) for the client-side matching + server-side storage
 
 @param publicTemplateUpdateHandler the VeridiumBioMatchPublicVectorUpdateHandler instance
 */
-(void)configurePublicVectorUpdateHandler:(id<VeridiumBioMatchPublicVectorUpdateHandler> _Nonnull) publicTemplateUpdateHandler;
@end


#pragma mark UI Delegate protocols

/*!
 Base protocol used to connect the biometric engine and it's hangling UI implementation.
 
 It specifies the contract for common UI traits shared by all biometric engine types. This protocol is extended by each biometric implementation that adds specific UI methods specs.
 */
@protocol VeridiumBiometricsUIDelegate <NSObject>

/*!
 Ask the UI to present itself and waits for completion
 
 @param doneBlock the completion callback voidBlock
 */
-(void) show:(voidBlock _Nonnull) doneBlock;

/*!
 Ask the UI to handle the successful outcome of the auth/enroll operation (e.g. show message and remove itself from the UI stack) and waits for completion
 
 @param doneBlock the completion callback voidBlock
 */
-(void) handleSuccess:(voidBlock _Nonnull)doneBlock;

/*!
 Ask the UI to handle the failure of the auth/enroll operation (e.g. show the failure reason and remove itself from the UI stack) and waits for completion
 
 @param message   the failure reason (e.g. match fail, timeout)
 @param doneBlock the completion callback voidBlock
 */
-(void) handleFail:(NSString* _Nonnull) message whenDone:(voidBlock _Nonnull)doneBlock;

/*!
 Ask the UI to handle the successful outcome of the auth/enroll operation (e.g. remove itself from the UI stack) and waits for completion
 
 @param doneBlock the completion callback voidBlock
 */
-(void) handleCancel:(voidBlock _Nonnull)doneBlock;

/*!
 Ask the UI to handle the erroneous outcome of the auth/enroll operation (e.g. show the error message and remove itself from the UI stack) and waits for completion
 
 @param message   the failure message
 @param doneBlock the completion callback voidBlock
 */
-(void) handleError:(NSString* _Nonnull) message whenDone:(voidBlock _Nonnull)doneBlock;

@optional

/*!
 By calling this function the engine provides a block to be called by the UI when the user initiates a cancel action (e.g. when the user hits a cancel button).
 
 The block is to be stored by the implementor of the protocol and used later when the user initiates the cancel action.
 
 The engine will do all the internal operations pertaining to the cancel procedure (ie. turn off camera, release allocated resources,etc..) and then call the cancel the handleCancel: method of the delegate to allow the UI to peform its post cancel UI actions
 
 @param cancelActionBlock The cancel callback action block to be stored and called by the delegate when the user initiates a cancel action
 */
-(void) connectCancelAction:(voidBlock _Nonnull) cancelActionBlock;

/*!
 Ask the UI to handle the state in which the engine is waiting for the user to manually start the operation
 
 You can skip the implementation of this method if you always configure the engine to start automatically
 
 Usually this method makes available to the user a start button (or a tap recognizer) to manually start the operation and calls the callback block in the UI handler of the tap event.
 
 Calling the callback block begins the enroll/auth operation and starts the timeout timer
 
 @param manualStartBlock the manual start callback voidBlock
 */
-(void) handleWaitingForManualStart:(voidBlock _Nonnull) manualStartBlock;

/*!
 Ask UI to handle the overall progress update.
 The UI usually uses the passed value to update a progress bar or a label displaying the progress
 
 @param progressPercent progress value between 0 and 1 (1 = 100%)
 */
-(void) handleNormalizedProgressUpdate:(float) progressPercent;

/*!
 Ask UI to handle the progress update with exact seconds passeed out of total seconds
 
 The UI usually uses the passed values to display a countdown or a x/y seconds
 
 @param progressPassedSeconds the passed seconds
 @param totalSeconds the total number of seconds until operation times out
 */
-(void) handleProgressUpdateWithPassedSeconds:(float) progressPassedSeconds outOfTotalSeconds:(float) totalSeconds;


@end
