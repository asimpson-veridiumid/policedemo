//
//  VeridiumBiometricsOnlyAccountService.h
//  VeridiumCore
//
//  Created by razvan on 3/9/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VeridiumLocalAccount.h"

@import VeridiumCore;


/*!
 Alias for blocks receiving a VeridiumLocalAccount argument
 */
typedef void(^localAccountBlock)(VeridiumLocalAccount* _Nonnull);

/*!
 Data class for holding the Biometrics-Only config
 */
@interface VeridiumBiometricsOnlyRegistrationConfig : NSObject

/*!
 Designated initializer
 
 The VeridiumBiometricsOnlyAccountService will use the provide biometric methods to create an aggregated enroller
 
 @param accountName the desired account name
 @param biometrics  the biometric methods (engine names) to be used for biometric enrollment
 */
-(instancetype _Nonnull) initForAccount:(NSString* _Nonnull) accountName biometrics:(NSArray<NSString*>* _Nonnull) biometrics;

@end


/*!
 Subclass of VeridiumAccountService that adds account registration functionality for local accounts (ie. no BOPS functionality)
 
 **IMPORTANT!** an instance of this class is automatically created and made available via the [VeridiumSDK(BiometricsOnly) accountService] property upon a successful call to [VeridiumSDK setup:]
 */
@interface VeridiumBiometricsOnlyAccountService : VeridiumAccountService

@property (readonly, nullable) VeridiumLocalAccount* activeAccount;
@property (readonly, nonnull) NSArray<VeridiumLocalAccount*>* registeredAccounts;

/*!
 Registers a VeridiumLocalAccount using the requested biometric enrollment methods
 
 @param config       VeridiumBiometricsOnlyRegistrationConfig filled in with the registration information
 @param successBlock success callback accountBlock
 @param failBlock    fail callback stringBlock; receives the failure reason
 @param cancelBlock  (optional) cancel callback
*/
-(void)registerAccount:(VeridiumBiometricsOnlyRegistrationConfig * _Nonnull)config
             onSuccess:(localAccountBlock _Nonnull) successBlock
                onFail:(stringBlock _Nonnull) failBlock
              onCancel:(voidBlock _Nullable) cancelBlock;

@end



