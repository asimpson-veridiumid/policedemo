//
//  VeridiumSDK+BiometricsOnly.h
//  VeridiumBiometricsOnly
//
//  Created by razvan on 7/12/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
@import VeridiumCore;
#import "VeridiumBiometricsOnlyAccountService.h"

@interface VeridiumSDK (BiometricsOnly)

@property (readonly, nonnull) VeridiumBiometricsOnlyAccountService* accountService;
@end
