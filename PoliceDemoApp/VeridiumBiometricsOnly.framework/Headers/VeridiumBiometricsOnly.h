//
//  VeridiumBiometricsOnly.h
//  VeridiumBiometricsOnly
//
//  Created by razvan on 7/12/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VeridiumBiometricsOnly.
FOUNDATION_EXPORT double VeridiumBiometricsOnlyVersionNumber;

//! Project version string for VeridiumBiometricsOnly.
FOUNDATION_EXPORT const unsigned char VeridiumBiometricsOnlyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VeridiumBiometricsOnly/PublicHeader.h>


#import "VeridiumBiometricsOnlyAccountService.h"
#import "VeridiumSDK+BiometricsOnly.h"
#import "VeridiumLocalAccount.h"