//
//  LocalAccount.h
//  VeridiumBiometricsOnly
//
//  Created by razvan on 8/10/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

@import VeridiumCore;

/*!
 Subclass of VeridiumAccount for local accounts (ie. with just biometrics and no server functionality)
 */
@interface VeridiumLocalAccount : VeridiumAccount

/*!
 The account name (given upon registration)
 */
@property (readonly, nonnull) NSString* accountName;


/*!
 Method for checking if the account can authenticate with the specified biometric methods (i.e. the engines specified have data enrolled for the account)
 
 @param methods array of strings with the biometric methods (engine names)
 
 @return `true` if the account can authenticate for the queried biomtric methods, `false` otherwise
 */
-(BOOL) canAuthenticateWithMethods:(NSArray <NSString*>* _Nonnull) methods;


/*!
 Perform a local authentication
 
 @param reason         the reason the user is prompted to authenticate
 @param methods        string array with the authentication methods - aka engine names (authenticators for all methods must be registered with the SDK prior to this call in order for it to work)
 @param onSuccessBlock success callback voidBlock
 @param onFailBlock    fail callback stringBlock; receives the failure reason
 @param onCancelBlock  cancel callback voidBlock
 @param onErrorBlock   error callback stringBlock; receives the error message
 */
-(void) authenticate:(NSString* _Nullable)reason
         withMethods:(NSArray<NSString*>* _Nonnull)methods
           onSuccess:(voidBlock _Nonnull) onSuccessBlock
              onFail:(stringBlock _Nonnull) onFailBlock
            onCancel:(voidBlock _Nonnull) onCancelBlock
             onError:(stringBlock _Nonnull) onErrorBlock;

@end
