//
//  Constants.h
//  Veridium4FBiometrics
//
//  Created by razvan on 9/12/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <Foundation/Foundation.h>

#define kVeridiumBiometricEngineName4F @"4F"

/*!
 Defines the 4F fingers assessment comming from the 4F library on each frame and capture
 */
typedef enum : NSUInteger {
  Veridium4FFingersAssessmentNotOK = 0,
  Veridium4FFingersAssessmentOutOfFocus,
  Veridium4FFingersAssessmentStable,
  Veridium4FFingersAssessmentTooClose,
  Veridium4FFingersAssessmentTooFar,
  Veridium4FFingersAssessmentTooFarApart,
  Veridium4FFingersAssessmentTooHigh,
  Veridium4FFingersAssessmentTooLow,
  Veridium4FFingersAssessmentTooFarLeft,
  Veridium4FFingersAssessmentTooFarRight
} Veridium4FFingersAssessment;


#define Veridium4FFingersAssessmentStrings @[\
@"Please keep your hand still with the fingers inside the guide",\
@"Your fingers are out of focus.\nPlease adjust your hand distance from the camera",\
@"Perfect.\nKeep your hand still.",\
@"Your hand is too close to the camera.\nPlease move your hand farther.",\
@"Your hand is too far from the camera.\nPlease move you hand closer.",\
@"Please keep your fingers closer together.",\
@"Your hand is too high.\nPlease move your hand towards the center.",\
@"Your hand is too low.\nPlease move your hand towards the center.",\
@"Your hand is too far to the left.\nPlease move your hand towards the center.",\
@"Your hand is too far to the right.\nPlease move your hand towards the center."]

#define Veridium4FFingersAssessmentColors @[\
[UIColor colorWithRed:0 green:0 blue:1 alpha:0.5],\
[UIColor colorWithWhite:0 alpha:0.5],\
[UIColor colorWithRed:0 green:1 blue:0 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:1 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:1 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:1 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:1 alpha:0.5],\
[UIColor colorWithRed:1 green:0 blue:1 alpha:0.5]]

@interface Veridium4FFingerCoordinates: NSObject

/*!
 The 2D coordinates for the 1st finger.
 */
@property CGRect finger1;
/*!
 The 2d coordinates for the 2nd finger.
 */
@property CGRect finger2;
/*!
 The 2d coordinates for the 3rd finger.
 */
@property CGRect finger3;
/*!
 The 2d coordinates for the 4th finger.
 */
@property CGRect finger4;

@end

/*!
 Data class used by 4F to send the finger ROIs (Region Of Interest) and additional info during the finger finding phase and in the capture phase to its UI delegate.
 
 The UI should use this information to display the fingers information and display finger overlays
 
 */

@interface Veridium4FFingersInfo: NSObject

/*!
 The recommended color for the finger rectangles ROI display.
 */
@property (nonnull) UIColor* color;
/*!
 Finger ROIs assesment mesage to provide hints to the user (hand to close, hand too far, etc.)
 */
@property (nullable) NSString* assessmentMessage;

/*!
 A constant value of finger finder assessment on the capture
 */
@property Veridium4FFingersAssessment assessment;

/*!
 The size of the image the finger data is based upon
 */
@property CGSize handPictureSize;

-(instancetype _Nonnull)initWithFingerROIs:(int[16]) rois
                         fingersAssessment:(Veridium4FFingersAssessment) assessment
                           handPictureSize:(CGSize) handPictureSize;


/*!
 Returns the finger rects converted from the coordinates of the source picture (handPictureSize) to coordinates in the frame of the target view size (i.e. the view that displays the picture and the finger overlays)
 
 This method should be used by the UI delegates to display the finger overlays on both video and picture updates.
 
 @param targetSize the size of the target view's frame
 
 @return an instance of the Veridium4FFingerCoordinates with the finger rects coordinates in the target view's frame
 */
-(Veridium4FFingerCoordinates* _Nonnull) fingerCoordinatesForTargetSize:(CGSize) targetSize;

@end

#endif /* Constants_h */
