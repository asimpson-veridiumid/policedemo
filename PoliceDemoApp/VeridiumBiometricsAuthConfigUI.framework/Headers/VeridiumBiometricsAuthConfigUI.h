//
//  VeridiumBiometricsAuthConfigUI.h
//  VeridiumBiometricsAuthConfigUI
//
//  Created by razvan on 12/20/15.
//  Copyright © 2015 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VeridiumBiometricsAuthConfigUI.
FOUNDATION_EXPORT double VeridiumBiometricsAuthConfigUIVersionNumber;

//! Project version string for VeridiumBiometricsAuthConfigUI.
FOUNDATION_EXPORT const unsigned char VeridiumBiometricsAuthConfigUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VeridiumBiometricsAuthConfigUI/PublicHeader.h>

#import "AuthConfigTableViewController.h"
