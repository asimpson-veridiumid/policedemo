//
//  AuthConfigTableViewController.h
//  VeridiumBiometricsAuthConfigUI
//
//  Created by razvan on 10/1/15.
//  Copyright © 2015 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kFaceRLConfigKey @"FACERandomLiveness"

/*!
 A convenience alias for the save callback of the AuthConfigTableViewController
 
 @param updatedConfig the biometric methods that are on
 */
typedef void(^authConfigSaveBlock)(NSDictionary* _Nullable updatedConfig);

/**
 *  This class will be inherited by a class handling the Authentication options
 */
@interface AuthConfigTableViewController : UITableViewController


@property (weak, nonatomic, nullable) IBOutlet UISwitch *swi4F;
@property (weak, nonatomic, nullable) IBOutlet UISwitch *swiFace;
@property (weak, nonatomic, nullable) IBOutlet UISwitch *swiFaceRandomLiveness;
@property (weak, nonatomic, nullable) IBOutlet UISwitch *swiTouchID;
@property (weak, nonatomic, nullable) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic, nullable) IBOutlet UIButton *resetButton;

/*!
 The default convenience static initializer that instantiates the AuthConfigTableViewController from the AuthConfig.storyboard included in the framework

@param config    the config key-value params

@return the initalized AuthConfigTableViewController
*/
+(instancetype _Nonnull) createDefaultWithInitialConfig:(NSDictionary* _Nullable) config;

/*!
 Convenience static factory initializer for instantiating from a custom storyboard with a custom identifier
 
 This is usesful for cases when you copy/paste the controller into your own storyboard and customize it. You can then create it easily by calling this initializer with your storyboard and your identifier.
 
 @param storyboard your custom storyboard (if `nil` it will default to the included storyboard)
 @param identifier your custom identifier (if `nil` it will default to `"AuthConfig"` - yo can pass nil if you copied the controller and left its storyboard identifier intact)
 @param config    arbitrary data you can use to pass additional configs
 
 @return the initialized AuthConfigTableViewController (or nil if could not be created)
 */
+(instancetype _Nullable) createFromStoryboard:(UIStoryboard* _Nullable)storyboard withIdentifier:(NSString* _Nullable)identifier andInitialConfig:(NSDictionary* _Nullable) config;

/*!
 The bioAuthMethodsBlock callback for the save action that gets passe an array with all the biometric methods that are on
 */
@property (nonnull) authConfigSaveBlock onSave;

/*!
 Exposed the inner config working copy for subclasses
 */
@property (readonly, nonnull) NSDictionary* configWorkingCopy;
@property (nonnull) NSDictionary* initialConfig;

-(void) updateUI;
-(IBAction)on4FChanged:(UISwitch * _Nullable)sender;
-(IBAction)onFaceChanged:(UISwitch * _Nullable)sender;
-(IBAction)onFaceRandomLivenessChanged:(UISwitch * _Nullable)sender;
-(IBAction)onTouchIDChanged:(UISwitch * _Nullable)sender;
-(void) reset;
-(IBAction)saveButtonTapped:(id _Nullable)sender;
-(IBAction)resetTapped:(id _Nullable)sender;
@end
