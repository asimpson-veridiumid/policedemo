//
//  VeridiumDefault4FAuthViewController.h
//  Default4FUI
//
//  Created by razvan on 10/9/15.
//  Copyright © 2015 HoyosLabs. All rights reserved.
//
@import VeridiumCore;
@import Veridium4FBiometrics;

@interface VeridiumDefault4FAuthViewController : UIViewController <Veridium4FAuthenticatorUIDelegate>


+(instancetype _Nullable) createFromStoryboard:(UIStoryboard* _Nullable)storyboard withIdentifier:(NSString* _Nullable)identifier;

+(instancetype _Nullable) createDefault;

@end
