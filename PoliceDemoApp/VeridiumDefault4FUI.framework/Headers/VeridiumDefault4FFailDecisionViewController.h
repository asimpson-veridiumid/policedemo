//
//  VeridiumDefault4FFailDecisionViewController.h
//  VeridiumDefault4FUI
//
//  Created by Paul Paul on 9/15/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
@import VeridiumCore;

@interface VeridiumDefault4FFailDecisionViewController : UIViewController
@property (strong, nonatomic) voidBlock onCancel;
@property (strong, nonatomic) voidBlock onRetry;
@property (strong, nonatomic) voidBlock onRestart;
@end
