//
//  VeridiumDefault4FEnrollViewController.h
//  Default4FUI
//
//  Created by razvan on 3/14/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
@import VeridiumCore;
@import Veridium4FBiometrics;

@interface VeridiumDefault4FEnrollViewController : UIViewController <Veridium4FEnrollerUIDelegate>


+(instancetype _Nullable) createFromStoryboard:(UIStoryboard* _Nullable)storyboard withIdentifier:(NSString* _Nullable)identifier;

+(instancetype _Nullable) createDefault;

@end
