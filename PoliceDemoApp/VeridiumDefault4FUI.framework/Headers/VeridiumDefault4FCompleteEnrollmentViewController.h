//
//  VeridiumDefault4FCompleteEnrollmentViewController.h
//  VeridiumDefault4FUI
//
//  Created by Paul Paul on 9/15/16.
//  Copyright © 2016 HoyosLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
@import VeridiumCore;

@interface VeridiumDefault4FCompleteEnrollmentViewController : UIViewController
@property (strong, nonatomic) voidBlock onOk;
@end
