#ifndef MAPPEDFILEDATA_H
#define MAPPEDFILEDATA_H

#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>

class MappedFileData
{
public:
    
    MappedFileData(const char* filename) : data(nullptr), size(0)
    {
        const auto filesize = getFilesize(filename);
        auto fd = open(filename,  O_RDWR, 0);
        
        size = static_cast<size_t>(filesize);
        data = reinterpret_cast<unsigned char*>(mmap(nullptr, size, PROT_READ, MAP_PRIVATE, fd, 0));
    }
    
    MappedFileData(std::string s) : MappedFileData(s.c_str())
    {
    }
    
    ~MappedFileData()
    {
        if(data != nullptr)
        {
            munmap(data, size);
        }
    }
    
    MappedFileData(MappedFileData&& o) : data(o.data), size(o.size)
    {
        o.data = nullptr;
    }
    
    MappedFileData& operator=(MappedFileData&& o)
    {
        std::swap(data, o.data);
        std::swap(size, o.size);
        return *this;
    }
    
    // Deleted to prevent copying.
    MappedFileData(MappedFileData& o) = delete;
    MappedFileData& operator=(MappedFileData& o) = delete;
    
    inline unsigned char* getData()
    {
        return data;
    }
    
    inline size_t getSize()
    {
        return size;
    }
    
private:
    
    using stat_struct = struct stat;
    inline decltype(stat_struct::st_size) getFilesize(const char* filename) {
        struct stat st;
        if(stat(filename, &st) != 0) {
            return 0;
        }
        
        return st.st_size;
    }
    
    unsigned char* data;
    size_t size;
};
#endif
