//
//  main.cpp
//  BiomatchServerClient
//
//  Created by Andrew Simpson on 09/11/2016.
//  Copyright © 2016 aiusepsi. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <grpc++/grpc++.h>
#include "biomatch.grpc.pb.h"
#include "MappedFileData.h"

int main(int argc, const char * argv[]) {
    
    for(size_t i = 0; i < argc; ++i)
    {
        std::cout << " \"" << argv[i] << "\" ";
    }
    std::cout << std::endl;
    
    if(argc != 4)
    {
        std::cerr << "Need 3 params, name, dob, template file" << std::endl;
    }
    
    std::string name(argv[1]);
    std::string dob(argv[2]);
    std::string template_file(argv[3]);
    
    std::cout << "Name: " << name << std::endl;
    std::cout << "DOB: " << dob << std::endl;
    std::cout << "Template: " << template_file << std::endl;
    
    MappedFileData template_data(template_file);
    
    // Set up RPC channel
    grpc::SslCredentialsOptions ssl_options;
    MappedFileData root_cert("/Users/andy/hoyos_code/BiomatchServer/BiomatchServer/pki/ca.crt");
    ssl_options.pem_root_certs = std::string(reinterpret_cast<char*>(root_cert.getData()), root_cert.getSize());
    auto channel = grpc::CreateChannel("localhost:50051", grpc::SslCredentials(ssl_options));
    std::unique_ptr<Veridium::PoliceDemo::Identifier::Stub> stub = Veridium::PoliceDemo::Identifier::NewStub(channel);
    
    grpc::ClientContext ctx;
    Veridium::PoliceDemo::IdentifyRequest req;
    Veridium::PoliceDemo::IdentifyResponse resp;
    
    req.set_name(name);
    req.set_dob(dob);
    req.set_biometric_template(template_data.getData(), template_data.getSize());
    
    grpc::Status status = stub->Identify(&ctx, req, &resp);
    
    if (!status.ok())
    {
        std::cerr << "RPC failed" << std::endl;
        std::cerr << status.error_code() << ": " << status.error_message()
        << std::endl;
        return 1;
    }
    
    if(resp.success())
    {
        std::cout << "Identification success!" << std::endl;
        auto face_image = resp.face_image();
        if(face_image.image_data().size() != 0 && face_image.type().compare("png") == 0)
        {
            std::ofstream out_file("out.png", std::ios::binary);
            out_file.write(face_image.image_data().data(), face_image.image_data().size());
            out_file.close();
            
            system("open out.png");
        }
    }
    else
    {
        std::cerr << "Identification failed!" << std::endl;
    }
    
    return 0;
}
