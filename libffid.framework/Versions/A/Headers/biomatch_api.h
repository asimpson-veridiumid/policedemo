/** \file
 * biomatch_api.h, uniform biometric matching API.
 */

#ifndef BIOMATCH_API_H
#define BIOMATCH_API_H

#include <stddef.h>
#include <stdint.h>

#pragma GCC visibility push(default)

#ifdef __cplusplus
extern "C"
{
#endif
    
/// Max string length for biomatch API.
#define MAXSTRCLEN	32

/// Information about the biometric engine.
typedef struct bio_lib_info {
    /// Name of biometric engine.
    char name[MAXSTRCLEN];
    /// Type of biometric engine.
    char engine_type[MAXSTRCLEN];
    /// Version of biometric engine.
    char engine_version[MAXSTRCLEN];
} BioLibInfo;

/// Result structure for biometric matches.
typedef struct match_res {
    int authorized; ///< 1 - true, 0 - false
    int error_no; ///< for audit history
    float score; ///< for audit history
} BioMatchRes;

/// Required to be the first call to any function in this interface
/**
 * \param init_dir_path *directory* to search for configuration file
 * \returns 0 on success, any other value indicates failure.
 */
int init_callback(const char *init_dir_path);

/// Returns a BioLibInfo structure by value which contains information about the library.
BioLibInfo bio_lib_describe(void);
    
/// Performs biometric matching.
/** \returns pointer to a BioMatchRes structure allocated on the heap with malloc, ownership of which is transferred to caller.
*/
BioMatchRes* bio_match(const void *captured, size_t captured_len,const void *decrypted_reference, size_t decrypted_len);
    
/// Like bio_match(), but provides for the passed enrollement template to be replaced.
/** If passed enrollment template is not to be replaced, then void* pointed at by adapted_reference is set to NULL
    and size_t pointed to by adapted_len is set to 0.
    If it is to be replaced, the void* pointed at by adapted_reference is set to point to a buffer containing the new enrollment template;
    the memory for that buffer shall be allocated by the memory allocation function pointed to by allocator. If allocator == NULL, then malloc()
    shall be used. allocator shall return NULL if memory cannot be allocated.
    The returned BioMatchRes shall also be allocated with allocator, or malloc if allocator == NULL. It is owned by the caller.
*/
BioMatchRes* bio_match_adaptive(const void *captured, size_t captured_len,const void *decrypted_reference, size_t decrypted_len, void** adapted_reference, size_t* adapted_len, void*(*allocator)(size_t));


  /**
   Validates a biometric vector
   
   @param serialized_vector     the serialized vector data
   @param serialized_vector_len the data length
   
   @return `true` if the vector is valid or `false` otherwise
   */
  bool is_vector_valid(const void *serialized_vector, size_t serialized_vector_len);

/// Last call to any function in this interface
void destroy_callback(void);

/// Work out the storage necessary to store the passed templates.
/** Returns number of bytes required to store passed templates. */
size_t GallerySaveSize(void** templates, const size_t* template_sizes, const size_t template_count);
    
/// Save the templates into the provided store array, which will have a size of at least the size previously returned by GallerySaveSize()
/** Returns 0 on success, other return values indicate failure. */
int GallerySave(void** templates, const size_t* template_sizes, const size_t template_count, void* store, size_t store_size);
    
#ifdef __cplusplus
}
#endif

#pragma GCC visibility pop

#endif // Include guard.
