#ifndef FFID_BIO_MATCH_H
#define FFID_BIO_MATCH_H

#include <vector>

#include "FlatInterface.h"
#include "biomatch_api.h"

#include <stddef.h>

namespace FourF {
    
    enum class BioMatchError
    {
        NO_ERROR = 0,
        FFID_FATAL_ERROR = 1,
        INSUFFICIENT_TEMPLATES = 2,
        OPENCV_ERROR = 3,
        UNINITIALISED = 4,
        PROBE_LOAD_FAIL = 5,
        INVALID_TEMPLATE = 6,
        GALLERY_UNPACK_FAIL = 7,
        UNSPECIFIED_POLICY = 8,
        INVALID_SECURITY_LEVEL = 9,
        INVALID_NUMBER_OF_TEMPLATES = 10,
        GALLERY_PACK_FAIL = 11
    };
    
    namespace Biomatch {
        
    enum class GalleryLoadResult
    {
        SUCCESS, NOT_ENOUGH_DATA_FOR_HDR, INVALID_FILETYPE, STILL_BASE64, INVALID_HEADER_VERSION, NOT_ENOUGH_DATA,
        FAILED_CHECKSUM, VECTOR_LOAD_FAIL, INVALID_FILETYPE_SINGLE_TEMPLATE,  INVALID_FILETYPE_BUNDLE
    };

    GalleryLoadResult LegacyGalleryLoad(const void* store, size_t store_size, std::vector<FourF::Vector>& gallery);
        
    GalleryLoadResult GalleryLoad(const void* store, size_t store_size, std::vector<FourF::Vector>& gallery);
    
}}

#endif


