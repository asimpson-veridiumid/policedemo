/** \file
 * FlatInterface.h, main library interface.
 * 
 * This file contains the main interface for the library.
 * Documentation for each function is available in Doxygen code comments.
 */

#ifndef FFID_FLATINTERFACE_H_
#define FFID_FLATINTERFACE_H_

#include <iostream>
#include <functional>
#include <vector>
#include <memory>

// Detect features based on available C++ version.
#if __cplusplus < 201103L
#error "4F requires C++11 or newer."
#endif

#if defined(__GNUC__)
    #define FFID_PRAGMA(x) _Pragma(#x)
#else
    #define FFID_PRAGMA(x)
#endif

// Make interface symbols visible.
#pragma GCC visibility push(default)

// Macros for suppressing diagnostic warnings.
#define FFID_INTERFACE_PUSH_DIAGNOSTIC FFID_PRAGMA(GCC diagnostic push)
#define FFID_INTERFACE_IGNORE(warn) FFID_PRAGMA(GCC diagnostic ignored #warn)
#define FFID_INTERFACE_POP_DIAGNOSTIC FFID_PRAGMA(GCC diagnostic pop)

/** \def FUNC_CONSTEXPR
 *  \brief Can be prepended to functions which meet the definition of extended constexpr in C++14.
 */

/** \def FFID_DEPRECATED(message_string)
 *  \brief Deprecation macro.
 *  Marks members of the interface that have been deprecated. Uses standard deprecation attribute in C++14, or GCC attribute if available in C++11.
 *  \sa FourF::Interface::Deprecated
 */

#if __cplusplus >= 201402L
    #define FUNC_CONSTEXPR constexpr
    // Use standard deprecation attribute if available.
    #define FFID_DEPRECATED(message_string) [[deprecated(message_string)]]
#else
    #define FUNC_CONSTEXPR
    #ifdef __GNUC__
        // Use non-standard GCC deprecation attribute.
        #define FFID_DEPRECATED(message_string) [[gnu::deprecated]]
    #else
        #define FFID_DEPRECATED(message_string)
    #endif
#endif

// Set warning about deprecated declarations.
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

// Forward declarations of some OpenCV types.
// Define FFID_NO_OPENCV_FORWARD to not use forward declarations; you can instead include
// core.hpp from OpenCV before including this file.
// Only define FFID_NO_OPENCV if the library was built with FFID_NO_OPENCV defined.
#if !defined(FFID_NO_OPENCV_FORWARD) && !defined(FFID_NO_OPENCV)
/// Forward-declared OpenCV members.
namespace cv
{
    /// Forward declaration of cv::Mat
    class Mat;
    /// Forward declaration of cv::Rect_<T>
    template<typename T> class Rect_;
    /// Forward declaration of cv::Rect
    using Rect = Rect_<int>;
}
#endif

/// 4F namespace.
namespace FourF
{
    // Return codes.
    
    /// Image processing return codes.
    /** Used by some functions in FourF::Interface, namely GetROIs(), GetFocus() and GetLiveness()
     */
    enum class GetImageInfoResult
    {
        FAIL_IMGPROCESSING,
        FAIL_ROINULLPTR,
        FAIL_ROIS_INVALID,
        FAIL_NOTIMPLEMENTED,
        FAIL_GREYSCALE_NOT_LOADED,
        FAIL_RGB_NOT_LOADED,
        FAIL_INVALID_HAND,
        FAIL_FFID_FATAL_ERROR,
        FAIL_OPENCV_ERROR,
        FAIL_IMG_HASZEROAREA,
        FAIL_TIPFINDING,
        SUCCESS
    };
    
    /// Provides human-readable English descriptions of the GetImageInfoResult codes.
    inline std::string to_string(const GetImageInfoResult r)
    {
        #define GIIR(x, msg) case GetImageInfoResult::x: return msg;
        
        switch(r) {
                GIIR(FAIL_IMGPROCESSING, "Image processing fail.")
                GIIR(FAIL_ROINULLPTR, "Passed ROI array pointer is null.")
                GIIR(FAIL_ROIS_INVALID, "Passed ROIs are invalid.")
                GIIR(FAIL_NOTIMPLEMENTED, "This is not implemented yet.")
                GIIR(FAIL_GREYSCALE_NOT_LOADED, "Greyscale image not loaded.")
                GIIR(FAIL_RGB_NOT_LOADED, "RGB image not loaded.")
                GIIR(FAIL_INVALID_HAND, "Passed an invalid hand value.")
                GIIR(FAIL_FFID_FATAL_ERROR, "A fatal 4F error occurred.")
                GIIR(FAIL_OPENCV_ERROR, "An OpenCV error occurred.")
                GIIR(FAIL_IMG_HASZEROAREA, "Image has zero area.")
                GIIR(FAIL_TIPFINDING, "Failed to find fingertips")
                GIIR(SUCCESS, "Success.")
        }
    }
    
    /// Return codes for FourF::Interface::GetVector()
    enum class GetVectorResult
    {
        FAIL_IMGPROCESSING,
        FAIL_ROINULLPTR,
        FAIL_GREYSCALE_NOT_LOADED,
        FAIL_ROIS_TOO_SMALL,
        FAIL_ROIS_INVALID,
        FAIL_FFID_FATAL_ERROR,
        FAIL_OPENCV_ERROR,
        FAIL_PRINT_NOT_CONTINUOUS,
        FAIL_EXTRACT_MINUTIAE,
        FAIL_EARLY_TERMINATION,
        FAIL_IMG_HASZEROAREA,
        FAIL_INVALID_HAND,
        SUCCESS
    };
    
    /// Provides human-readable English descriptions of the GetVectorResult codes.
    inline std::string to_string(const GetVectorResult r)
    {
        #define GVR(x, msg) case GetVectorResult::x: return msg;
        
        switch(r) {
            GVR(FAIL_IMGPROCESSING, "Image processing fail.")
            GVR(FAIL_ROINULLPTR, "Passed ROI array pointer is null.")
            GVR(FAIL_GREYSCALE_NOT_LOADED, "Greyscale image not loaded.")
            GVR(FAIL_ROIS_TOO_SMALL, "Passed ROIs are too small.")
            GVR(FAIL_ROIS_INVALID, "Passed ROIs are invalid.")
            GVR(FAIL_FFID_FATAL_ERROR, "A fatal 4F error occurred.")
            GVR(FAIL_OPENCV_ERROR, "An OpenCV error occurred.")
            GVR(FAIL_PRINT_NOT_CONTINUOUS, "Print image not a continuous matrix.")
            GVR(FAIL_EXTRACT_MINUTIAE, "Failed to extract minutiae from image.")
            GVR(FAIL_EARLY_TERMINATION, "Minutiae creation was told to terminate early.")
            GVR(FAIL_IMG_HASZEROAREA, "Image has zero area.")
            GVR(FAIL_INVALID_HAND, "Invalid ImagedHand passed")
            GVR(SUCCESS, "Success.")
        }
    }
    
    /// Return codes for match functions, e.g. FourF::Interface::GetMatch()
    enum class GetMatchResult
    {
        SUCCESS,
        FAIL_INSUFFICIENT_TEMPLATES,
        FAIL_INVALID_NUMBER_TEMPLATES,
        FAIL_INVALID_TEMPLATE,
        FAIL_FFID_FATAL_ERROR,
        FAIL_OPENCV_ERROR,
        FAIL_UNSPECIFIED_POLICY,
        FAIL_UNSPECIFIED_LED_MODE,
        FAIL_INVALID_SECURITY_LEVEL
    };
    
    /// Provides human-readable English descriptions of the GetMatchResult codes.
    inline std::string to_string(const GetMatchResult r)
    {
        #define GMR(x, msg) case GetMatchResult::x: return msg;
        
        switch(r)
        {
            GMR(SUCCESS, "Success.")
            GMR(FAIL_INSUFFICIENT_TEMPLATES, "Insufficient number of templates supplied.")
            GMR(FAIL_INVALID_NUMBER_TEMPLATES, "An invalid number of templates supplied.")
            GMR(FAIL_INVALID_TEMPLATE, "A passed template was invalid.")
            GMR(FAIL_FFID_FATAL_ERROR, "A fatal 4F error occurred.")
            GMR(FAIL_OPENCV_ERROR, "An OpenCV error occurred.")
            GMR(FAIL_UNSPECIFIED_POLICY, "An unspecified policy error occurred.")
            GMR(FAIL_UNSPECIFIED_LED_MODE, "An unspecified led mode error occurred.")
            GMR(FAIL_INVALID_SECURITY_LEVEL, "Security level was invalid.")
        }
    }
    
    /// Values for specifying ambient lighting conditions.
    /**
     * This is phone-dependant, but > 1000 lux usually considered HIGH.
     * LOW is usually a safe default.
     * For more details contact Richard Tyson.
     */
    enum class Lux
    {
        LOW = 0,
        HIGH = 1,
        INVALID
    };
    
    /// Type for progress callbacks.
    /**
     * Used when calling some overloads of FourF::Interface::GetMatch()
     * The callback is called when progress is made in the operation. Can be used
     * to implement progress bars.
     * Compatible with functions with the signature:
     * \code
     * void function_name(int progressIncrement, int overallProgress)
     * \endcode
     * Also accepts functors, lambdas, etc.
     */
    typedef std::function<void(int,int)> Progress;
    
    /// Forward declaration of container for 4F template information.
    /// See below for full declaration.
    class Vector;
  
    class Nist;
  
    /// 4F library interface.
    /**
     *  This namespace provides the interface for apps which want to implement 4F.
     *  The basic flow is to get an ImageHandle using the static methods of ImageHandle,
     *  then pass those images to the various interface functions.
     *  GetROIs() is used to extract the ROIs.
     *  GetFocus() is used to determine if the focus quality on the ROIs is good enough to proceed with matching.
     *  Template vectors can be acquired using GetVector(), and then getMatch() used to
     *  perform a match. See those functions for a more detailed description of what they do.
     */
    namespace Interface
    {
        // Initialisation and shutdown functions.
        
        /// Initialisation settings for the 4F library.
        /** 
         *  This is versioned so that we can add/remove settings at a later date.
         *  \sa FourF::Interface::init()
         */
        
        FFID_INTERFACE_PUSH_DIAGNOSTIC
        FFID_INTERFACE_IGNORE(-Wpadded) // InitSettings003 is padded to align 'resourcesPath'
        struct InitSettings003
        {
            // Paths to fingertip cascades.
            // The cascade files are distributed with the library.
            // By default, in repository_root/cascade_detectors
            
            /// Path to fingertip cascade file
            const std::string tipCascadePathA;
            /// Path to second fingertip cascade file
            const std::string tipCascadePathB;
            /// Path to 3rd fingertip cascade file
            const std::string tipCascadePathC;
            /// Path to 4th fingertip cascade file
            const std::string tipCascadePathD;
            /// Path to hand cascade file
            const std::string handCascadePath;
            /// flash or torch
            const int led_mode;
            /// Path to all library resources
            const std::string resourcesPath;
        };
        FFID_INTERFACE_POP_DIAGNOSTIC
        
        struct InitSettings004
        {
            /// flash or torch
            const int& led_mode;
            /// Path to all library resources
            const std::string& resourcesPath;
        };
        
        /// Initialiser, must be called before calling any function in this interface.
        /**
         \param settings The initialisation settings to use.
         \sa init_noFingerDetector(), shutdown()
         \return True if inititialisation was successful, false otherwise.
         */
        bool init(const InitSettings004& settings);
        
        /// DEPRECATED Initialiser, must be called before calling any function in this interface.
        /**
         \deprecated Use init(const InitSettings004& settings)
         */
        FFID_DEPRECATED("Use init(const InitSettings004& settings)")
        bool init(const InitSettings003& settings);
        
        /// Initialiser, must be called before calling any function in this interface.
        /** Doesn't initialise the finger detector, therefore no function which detects finger ROIs
         *  will work correctly. Use the regular init() if you want to detect finger ROIs.
            \sa init(), shutdown()
            \return True if inititialisation was successful, false otherwise.
         */
        bool init_noFingerDetector();

        /// startTracking, call before sending any preview frames.
        /** Call this to set up tracking fingers via preview frames, pre-computes values
            based on preview frame width, height, camera fov, and required distance of the hand
         */
        void startTracking(int imgWidth, int imgHeight, float horz_fov, float hand_dist);
        
        /// Shutdown, call after you're done with this interface to clean up the resources used.
        /** \sa init(), init_noFingerDetector()
         */
        void shutdown();
        
        /// Values to designate what hand is being imaged.
        /** i.e. the hand depicted in the image.
         */
        enum class ImagedHand
        {
            Invalid, Left, Right, Unknown
        };
        
        /// Returns string values to describe ImagedHand values.
        inline std::string to_string(const ImagedHand hand)
        {
            switch(hand)
            {
                case ImagedHand::Invalid:
                    return "Invalid hand";
                case ImagedHand::Left:
                    return "Left hand";
                case ImagedHand::Right:
                    return "Right hand";
                case ImagedHand::Unknown:
                    return "Unknown hand";
            }
        }

        /// Used to set a path to which BMP files are written.
        /** This path is used by the GetVector() functions to write an image with the ROIs drawn on.
         *  This function should be considered deprecated; generally it should be preferable for the app
         *  itself to draw these images.
         *  \param path the path to which BMP images will be written.
         *  \deprecated Do not use. Draw the images with ROIs directly in app code.
         */
        FFID_DEPRECATED("Do not use. Draw the images with ROIs directly in app code.")
        void setBmpDumpPath(const std::string path);
        
        /// Do not use.
        FFID_DEPRECATED("This might not even do anything.")
        void setGuideDumpPath(const std::string path);
        
        void generateDefaultGuide(const char* path, float distance, float x_offset, float y_offset);
      
        void generateDefaultGuideWithMats(cv::Mat& guide_img, cv::Mat img_guide_fingers, cv::Mat img_single_tip, float fov, float offset_x, float offset_y, float distance, float screen_aspect);
      
        void generateCustomGuideWithMats(cv::Mat& guide_img, cv::Mat img_guide_fingers, cv::Mat img_single_tip, float fov, float offset_x, float offset_y, float distance, int rois[16], float screen_aspect, const ImagedHand hand, int imageHeight);
      
        /// Set the default level of security used when doing matches.
        /**  Please consult the documentation for the definition of these security levels.
         *  \param level The security level to use as the default.
         *  \sa getMatch()
         */
        void setDefaultSecurityLevel(unsigned int level);
        
        /// Supported image formats.
        /**
         *  For each of these formats, all image channels are 8 bits.
         */
        enum class ImageFormat : int
        {
            INVALID = 0, GREY, RGB, BGR, RGBA, BGRA, HSV, MAX
        };
        
        /// \name ImageFormat utilities.
        ///@{
        
        /// Utility for determining if the ImageFormat represents a colour image.
        inline constexpr bool isColour(ImageFormat f)
        {
            return f == ImageFormat::RGB || f == ImageFormat::BGR || f == ImageFormat::RGBA
                || f == ImageFormat::BGRA;
        }
            
        /// Utility for determining the index of the red channel in an image format.
        inline int redChannelIndex(ImageFormat f)
        {
            switch(f)
            {
                case ImageFormat::INVALID:
                case ImageFormat::GREY:
                case ImageFormat::HSV:
                case ImageFormat::MAX:
                    // No red channel for these formats!
                    return -1;
                case ImageFormat::RGBA:
                case ImageFormat::RGB:
                    return 0;
                case ImageFormat::BGR:
                case ImageFormat::BGRA:
                    return 2;
            }
        }
            
        ///@}
        
        /// Encapsulation of an ROI (Region Of Interest)
        /** Same convention as OpenCV, origin is top-left corner.
         */
        class Roi
        {
        private:
            int _roi[4] = {-1, -1, -1, -1};
        public:
            
            /// Constructor of ROI from coordinates.
            Roi(int left, int top, int right, int bottom);
            
            /// Constructor of ROI from coordinates.
            /** same order as other constructor: left, top, right, bottom.
             */
            Roi(int roi[4]);
            
            /// Construct a default invalid ROI.
            Roi() = default;
            
            /// \name Accessors for the coordinates.
            ///@{
            
            inline int left() const { return _roi[0]; }     ///< Gets left coordinate.
            inline int top() const { return _roi[1]; }      ///< Gets top coordinate.
            inline int right() const { return _roi[2]; }    ///< Gets right coordinate.
            inline int bottom() const { return _roi[3]; }   ///< Gets bottom coordinate.
            
            ///@}
            
            /// \name Utilities to get x, y, width, height representation.
            ///@{
            
            inline int x() const { return left(); }                 ///< Gets x coordinate.
            inline int y() const { return top(); }                  ///< Gets y coordinate.
            inline int width() const { return right() - left();}    ///< Gets width.
            inline int height() const { return bottom() - top();}   ///< Gets height.
            
            ///@}
            
            /// Returns if the ROI is valid, e.g. can only be true if the width isn't negative.
            bool isValid() const;
            
            #ifndef FFID_NO_OPENCV
            /// \name Allow conversions to and from cv::Rect.
            ///@{
            
            /// Implicit converting constructor from cv::Rect.
            Roi(const cv::Rect&);
            
            /// Operator for implicit conversions to cv::Rect.
            operator cv::Rect() const;
            
            ///@}
            #endif
        };
            
        /// Wrapper for a set of four ROIs.
        /* This is used to represent the four finger ROIs in a single image.
         */
        class RoiSet
        {
        private:
            Roi internal_rois[4];
        public:
            
            /// Determines if the RoiSet is valid.
            /** Checks if all four of the constituent Roi objects are valid.
             */
            inline bool isValid() const
            {
                for(const Roi& r : internal_rois)
                {
                    if(!r.isValid()) return false;
                }
                
                return true;
            }
            
            /// Gets Roi from the set.
            /** Throws std::out_of_range if you attempt to access an element with index > 3
             */
            inline const Roi& getRoi(size_t i) const {
                if(i >= 4) throw std::out_of_range("Max index of RoiSet::getRoi() is 3!");
                return internal_rois[i];
            }
            
            /// Gets rois as a const array of 16 ints.
            /** Individual coordinates are laid out as: left, top, right, bottom
             */
            inline const int* getRois() const { return reinterpret_cast<const int*>(internal_rois); }
            
            /// Gets rois as an array of 16 ints.
            /** Individual coordinates are laid out as: left, top, right, bottom
             */
            inline int* getRois() { return reinterpret_cast<int*>(internal_rois); }
            
            /// Gets individual roi coordinate from the set.
            inline int& operator[](size_t i) {
                if(i >= 16) throw std::out_of_range("Max index of RoiSet::operator[] is 15!");
                return getRois()[i];
            }
            
            /// \name Iteration
            ///@{
            
            /// Returns an interator to the first Roi in the RoiSet.
            inline auto begin() -> decltype(std::begin(internal_rois)) { return std::begin(internal_rois); }
            
            /// Returns an iterator to the element following the last element of the RoiSet.
            /** This is a placeholder. Accessing the value of end() directly leads to undefined behaviour.
             */
            inline auto end() -> decltype(std::end(internal_rois)) { return std::end(internal_rois); }
            
            ///@}
            
            /// Constructs a default RoiSet.
            RoiSet() {}
            
            /// Creates an RoiSet from an old-style roi array.
            RoiSet(const int rois[16])
            {
                int* _rois = getRois();
                std::copy(rois, rois + 16, _rois);
            }
            
            /// Constructor to initialise from any iterable of Roi, e.g. std::vector<Roi>, Roi rois[4], etc.
            template<typename T, typename = std::iterator_traits< decltype( std::begin(std::declval<T>()) ) >>
            RoiSet(const T& t)
            {
                using iterator_val_type = typename std::iterator_traits<decltype(std::begin(t)) >::value_type;
                static_assert(std::is_same<Roi, iterator_val_type>::value, "Iterator on type needs to yield an Roi");
                
                size_t counter = 0;
                
                for(const Roi& roi : t)
                {
                    internal_rois[counter] = roi;
                    ++counter;
                }
            }
        };
            
        // Forward declaration of the internal opaque Image class.
        class InternalImage;
        
        /// Handle to an image resource.
        /**
         *  A handle to an image resource which can be passed to many of the interface methods in FourF::Interface.
         *  Handles reference-counting of the underlying image resource, and will deallocate it when the last handle to it is destroyed.
         *  Handles provide access to the format of the image (represented as an ImageFormat) and to the image data as an OpenCV mat.
         *  There are static methods provided for loading Greyscale and RGBA raw arrays into an ImageHandle, and getting an ImageHandle from
         *  an OpenCV mat.
         *  There are some other utility methods provided.
         */
        class ImageHandle
        {
        public:
            
            /// Creates a null handle.
            ImageHandle();
            
            #ifndef FFID_NO_OPENCV
            
            /// Accessor for the underlying image data as OpenCV mat.
            cv::Mat& getMat();
            
            /// Accessor for the underlying image data as a const OpenCV mat.
            const cv::Mat& getConstMat() const;
            
            #endif
            
            /// Accessor for the image format.
            ImageFormat getFormat() const;
            
            /// Releases the image resource being held by this handle.
            /** After call, the handle becomes a null handle.
             */
            void release();
            
            /// Tests for null handle.
            bool isNullHandle();
            
            /// Get RoiSet associated with this image.
            inline RoiSet& getRoiSet() { return rois; }
			
            /// Get old-style roi array associated with this image.
			inline int* getInternalRois() { return rois.getRois(); }
            
            /// Explicitly create a null handle.
            static ImageHandle createNullHandle();
            
            /// Creates an ImageHandle for an RGBA8888 image from a pre-existing array.
            /**
             *  This function effectively takes ownership of the memory; a callback must be provided to perform the necessary cleanup of
             *  this memory.
             *  \param imagedata A pointer to the raw image data.
             *  \param width The width of the image.
             *  \param height The height of the image.
             *  \param state A pointer to user-defined data that is provided to the deallocation callback.
             *  \param dealloc_callback A callback function with a signature like: \code void dealloc(void* imagedata, void* state) \endcode
             *                          This is called when there are no outstanding ImageHandles, and the image data has to be cleaned up.
             */
            static ImageHandle loadRGBA8888Image(void* imagedata, int width, int height, void* state, std::function<void(void*, void*)> dealloc_callback);
            
            /// Creates an ImageHandle for an BGRA8888 image from a pre-existing array.
            /**
             *  This function effectively takes ownership of the memory; a callback must be provided to perform the necessary cleanup of
             *  this memory.
             *  \param imagedata A pointer to the raw image data.
             *  \param width The width of the image.
             *  \param height The height of the image.
             *  \param state A pointer to user-defined data that is provided to the deallocation callback.
             *  \param dealloc_callback A callback function with a signature like: \code void dealloc(void* imagedata, void* state) \endcode
             *                          This is called when there are no outstanding ImageHandles, and the image data has to be cleaned up.
             */
            static ImageHandle loadBGRA8888Image(void* imagedata, int width, int height, void* state, std::function<void(void*, void*)> dealloc_callback);
            
            /// Creates an ImageHandle for an 8-bit greyscale image from a pre-existing array.
            /**
             *  This function effectively takes ownership of the memory; a callback must be provided to perform the necessary cleanup of
             *  this memory.
             *  \param imagedata A pointer to the raw image data.
             *  \param width The width of the image.
             *  \param height The height of the image.
             *  \param state A pointer to user-defined data that is provided to the deallocation callback.
             *  \param dealloc_callback A callback function with a signature like: \code void dealloc(void* imagedata, void* state) \endcode
             *                          This is called when there are no outstanding ImageHandles, and the image data has to be cleaned up.
             */
            static ImageHandle loadGreyscale8Image(void* imagedata, int width, int height, void* state, std::function<void(void*, void*)> dealloc_callback);
            
            /// Creates an ImageHandle for an image with the given format from a pre-existing array.
            /**
             *  This function effectively takes ownership of the memory; a callback must be provided to perform the necessary cleanup of
             *  this memory.
             *  \param imagedata A pointer to the raw image data.
             *  \param width The width of the image.
             *  \param height The height of the image.
             *  \param state A pointer to user-defined data that is provided to the deallocation callback.
             *  \param dealloc_callback A callback function with a signature like: \code void dealloc(void* imagedata, void* state) \endcode
             *                          This is called when there are no outstanding ImageHandles, and the image data has to be cleaned up.
             *  \param format An ImageFormat giving the image format of \a imagedata.
             */
            static ImageHandle loadImageWithFormat(void* imagedata, int width, int height, void* state, std::function<void(void*, void*)> dealloc_callback, ImageFormat format);
            
            /// Returns a new ImageHandle to a greyscale version of the passed ImageHandle.
            static ImageHandle getGreyscaleFromColor(ImageHandle& color);
            
            // OpenCV-specific:
            #ifndef FFID_NO_OPENCV
            
            /// Get a cropped version of the image represented by this handle.
            ImageHandle operator()(cv::Rect& roi);
            
            /// Creates an ImageHandle for a pre-existing OpenCV mat.
            /**
             *  The OpenCV mat must remain valid for the lifetime of this ImageHandle, so don't use an OpenCV Mat created from a
             *  user-supplied data pointer. Use loadRGBA8888Image() or loadGreyscale8Image() if you want to do that.
             */
            static ImageHandle loadCvMat(cv::Mat& m, ImageFormat f);
            
            /// Converts an OpenCV mat from one format to another.
            /** The source and destination image can refer to the same cv::Mat.
             *  \param src Source image.
             *  \param src_format Format of the source image.
             *  \param dest Destination image
             *  \param dest_format Desired destination image format.
             */
            static bool convertCvMat(const cv::Mat& src, const ImageFormat src_format, cv::Mat& dest, const ImageFormat dest_format);
            
            #endif
            
        private:
            ImageHandle(std::shared_ptr<InternalImage>&& img);
            std::shared_ptr<InternalImage> img = nullptr;
            RoiSet rois;
        };
        
        /// Token that is used to refer to stored ImageHandles in the ImageStore namespace.
        using StoreToken = int32_t;
        
        /// Result codes when retrieving ImageHandles from the ImageStore namespace.
        enum class StoreResult
        {
            SUCCESS = 0,
            FAIL_INVALID_TOKEN,
            FAIL_NO_STORE_FOR_TOKEN
        };
        
        /// Functions for globally storing image data.
        /** Store is cleared when shutdown() is called.
         */
        namespace ImageStore
        {
            /// Stores an ImageHandle in the global image store.
            /** Image can later be retrieved using the retrieve() function, 
             *  passing the returned token.
             \param h Handle to image to be stored.
             \return A token to be used to retrieve or release the stored image.
             */
            StoreToken store(ImageHandle h);
            
            /// Releases a stored ImageHandle.
            /** \param token Token returned from a call to store().
             */
            void release(StoreToken token);
            
            /// Retrieves a stored image from the store.
            /** This function can fail if the passed token is invalid or represents an already
             *  released image. Therefore, this returns a StoreResult-ImageHandle pair. The 
             *  ImageHandle is only valid if the StoreResult is SUCCESS.
             \param token Token returned from a call to store().
             \return Pair containing call result code and ImageHandle. ImageHandle only valid if result code is SUCCESS.
             */
            std::pair<StoreResult,ImageHandle> retrieve(StoreToken token);
        }

        /// getROIs, outputs the rois and an ROI quality score for the passed ImageHandle.
        /**
         \param img The image to get the ROIs from.
         \param lux The light level when the picture was taken.
         \param hand The hand (right or left) that this picture is of.
         \param[out] rois Output parameter; 16 element long int array, with ROI coordinates relative to the provided image, in the order (left, top, right, bottom)
         \param[out] rois_score ROI quality score (lower is better, < FourF::ROI_THRESHOLD && > 0 is pass.)
         */
        GetImageInfoResult getROIs(ImageHandle img, const Lux lux, const ImagedHand hand, int rois[16], int& rois_score);
            
        /// Determines if the passed rois_score represents a pass.
        bool isRoiThresholdPass(const int rois_score);
        
        /// getFocus, gets the focus score for the passed ImageHandle, with provided ROIs.
        /**
         \param[in] img The image to calculate the focus score of.
         \param[in] rois ROI array retrieved from GetROIs()
         \param[out] focus_score focus quality score, use isFocusScorePass() to determine if this score represents a pass.
         **/
        GetImageInfoResult getFocus(ImageHandle img, const int rois[16], int& focus_score);
            
        /// Determines if a focus score from getFocus() represents a passing focus score.
        /**
         \param focus_score A focus score generated by getFocus()
         \param isPreview Pass true if the image for which the focus was determined was a low-resolution preview image, false otherwise.
         **/
        bool isFocusScorePass(const int focus_score, bool isPreview);

        /// Allows modification of behaviour of FourF::ROI::ShouldTakePicture() to enable stereo liveness.
        /** When option is set to true, FourF::ROI::ShouldTakePicture() will not indicate a picture should be taken
         *  unless the hand has moved horizontally.
         */
        void setPreventLocalRecapture(bool option);
        
        // LIVENSS METHDOS
        static FourF::Interface::StoreToken token_livenessImg1; // token for storing first liveness image
        
        /// Gets the result of the last getStereoLiveness() call.
        /** Only the first call to getLivenessResult() following a getStereoLiveness() call will return the result; subsequent calls to getLivenessResult() will return false.
            \returns true if liveness detected, otherwise false.
         */
        bool getLivenessResult();
            
        /// Gets a liveness score based on analysing images taken with flash and without flash. Still somewhat experimental.
        GetImageInfoResult getLiveness(ImageHandle verify_image, ImageHandle nonflash_image, const ImagedHand hand, const int rois[16], int64_t livenessDelayDuration, double* liveness_score, int &isLive);
           
        /// Analyses two images of a hand, with the hand horizontall displaced in each image, to determine liveness.
        /** The two images should be taken sequentially, with setPreventLocalRecapture() used before taking the second image to ensure the second image is sufficiently horizontally displaced from the first image. 
         *  \param image1 The first image.
         *  \param image2 The second image.
         *  \param hand An ImagedHand value representing the hand depicted in image1 and image2
         *  \param rois1 The rois of the fingertips in image1
         *  \param rois2 The rois of the fingertips in image2
         *  \param[out] isLive Set to true the subject is live, false otherwise.
         *  \returns A GetImageInfoResult which indicates the success or failure of this operation.
         */
        GetImageInfoResult getStereoLiveness(ImageHandle image1, ImageHandle image2, const ImagedHand hand,
                                             int rois1[16], int rois2[16], bool& isLive);
        
        /// getMatch, matches the passed ImageHandle against a gallery of enrolled fingerprint vectors.
        /**
         * Returns true if it's a match, false otherwise.
         \param img The verify image.
         \param[in] rois ROI array retrieved from GetROIs()
         \param[in] galleries An array of fingerprint vectors captured at enrol time using GetVector() (typically 3)
         \param[in] progress A callback which reports units of progress. At time of writing, there are 20 progress units.
         \param[out] match The matching result. Set to true on match, false otherwise.
         \param[out] raw_score The raw matching score. May be negative in error cases.
         \param[in] securityLevel Configurable level of match security. -1 uses the currently configured default setting.
         \return Returns GetMatchResult::SUCCESS if the match operation occurred without error. Use the match parameter to determine if this was a match.
         **/
        GetMatchResult getMatch(ImageHandle img, const int rois[16], const std::vector<FourF::Vector>& galleries, FourF::Progress progress, bool& match, int& raw_score, int securityLevel = -1);
            
        /// getMatch, matches the passed vector to be verified against a gallery of enrolled fingerprint vectors.
        /**
         * Returns true if it's a match, false otherwise.
         \param v The vector to be verified, captured using GetVector()
         \param[in] galleries An array of fingerprint vectors captured at enrol time using GetVector() (typically 3)
         \param[in] progress A callback which reports units of progress.
         \param[out] match The matching result. Set to true on match, false otherwise.
         \param[out] raw_score The raw matching score. May be negative in error cases.
         \param[in] securityLevel Configurable level of match security. -1 uses the currently configured default setting.
         \return Returns GetMatchResult::SUCCESS if the match operation occurred without error. Use the match parameter to determine if this was a match.        
         **/
        GetMatchResult getMatch(FourF::Vector v, const std::vector<FourF::Vector>& galleries, FourF::Progress progress, bool& match, int& raw_score, int securityLevel = -1);
        
        /// getMatch, matches the passed vector to be verified against a gallery of enrolled fingerprint vectors.
        /**
         * Returns true if it's a match, false otherwise.
         \param v The vector to be verified, captured using GetVector()
         \param[in] galleries An array of fingerprint vectors captured at enrol time using GetVector() (typically 3)
         \param[in] progress A callback which reports units of progress.
         \param[out] match The matching result. Set to true on match, false otherwise.
         \param[out] raw_score The raw matching score. May be negative in error cases.
         \param[out] fusion_probability The value generated by the probablistic score fusion system.
         \param[in] securityLevel Configurable level of match security. -1 uses the currently configured default setting.
         \return Returns GetMatchResult::SUCCESS if the match operation occurred without error. Use the match parameter to determine if this was a match.
         **/
        GetMatchResult getMatch(FourF::Vector v, const std::vector<FourF::Vector>& galleries, FourF::Progress progress, bool& match, int& raw_score, double& fusion_probability, int securityLevel = -1);
            
        /// getVector, gets a fingerprint biometric vector, using the provided ROIs and the passed ImageHandle.
        /**
         \param img The image from which to generate a vector
         \param hand The hand being imaged
         \param[in] rois ROI array retrieved from GetROIs()
         \param[out] minutiae fingerprint vector
         **/
        GetVectorResult getVector(ImageHandle img, ImagedHand hand, const int rois[16], FourF::Vector& minutiae);

        /// getVector, gets a fingerprint biometric vector, using the provided ROIs and the passed ImageHandle.
        /**
         \param img The image from which to generate a vector
         \param[in] rois ROI array retrieved from GetROIs()
         \param[out] minutiae fingerprint vector
         \deprecated Use getVector(ImageHandle img, ImagedHand hand, const int rois[16], FourF::Vector& minutiae)
         **/
        FFID_DEPRECATED("Use getVector(ImageHandle img, ImagedHand hand, const int rois[16], FourF::Vector& minutiae)")
        GetVectorResult getVector(ImageHandle img, const int rois[16], FourF::Vector& minutiae);
            
        /// getNist, gets a fingerprint minutiae points and image in nist-ansi itl 2007 format, using the provided ROIs , left/right hand and the passed ImageHandle.
        /**
        \param img The image from which to generate a vector
        \param[in] hand left or right hand
        \param[in] rois ROI array retrieved from GetROIs()        
        \param[out] nist image and minutiae data in nist format
        **/
        GetVectorResult getNist(ImageHandle img, const ImagedHand hand, const int rois[16], Nist& nist);
        
        /// Realtime ROI detection.
        /**
         *  The function of this system is twofold; to administer the process of taking in preview frames from the camera and producing ROI information that the consumer can display, and to give the app guidance on if now is a good moment to take a full-resolution flash image.
         */
        namespace ROI
        {
            /// Return codes for submitFrameForROIs().
            enum class SubmitFrameResult
            {
                SUCCESS, FAIL_FRAME_NOT_COLOUR, FAIL_INVALID_HAND, FAIL_INVALID_SCORE_NOT_HAND_OR_GARBLED,
                FAIL_REALTIME_ROI, FAIL_NOT_INITIALISED, FAIL_OPENCV_ERROR, FAIL_FFID_FATAL_ERROR,
                FAIL_SIMULTANEOUS_CALL
            };
            
            /// Provides human-readable English descriptions of the SubmitFrameResult codes.
            inline std::string to_string(SubmitFrameResult r)
            {
                #define SFR(x, msg) case SubmitFrameResult::x: return msg;
                switch(r) {
                    SFR(FAIL_FRAME_NOT_COLOUR, "Submitted frame not in colour.")
                    SFR(FAIL_INVALID_HAND, "Hand wasn't valid.")
                    SFR(FAIL_INVALID_SCORE_NOT_HAND_OR_GARBLED, "Invalid score, not a hand or garbled.")
                    SFR(FAIL_REALTIME_ROI, "Realtime ROI failure")
                    SFR(FAIL_NOT_INITIALISED, "Not initialised.")
                    SFR(FAIL_OPENCV_ERROR, "OpenCV error")
                    SFR(FAIL_FFID_FATAL_ERROR, "4F fatal  error")
                    SFR(FAIL_SIMULTANEOUS_CALL, "Called while frame is already being processed.")
                    SFR(SUCCESS, "Success")
                }
            }
            
            /// Output stream operator for SubmitFrameResult
            inline std::ostream& operator<<(std::ostream& o, const SubmitFrameResult r)
            {
                return o << to_string(r);
            }
            
            /// Submit a preview frame to the preview ROI system.
            /**
             * This function takes a preview frame, and performs the necessary computations to extract ROIs
             * from it. This function is **not** guaranteed to be cheap, and so probably can't be called with
             * every single available preview frame unless you have extremely powerful hardware available.
             *
             * Because this function is not cheap, it **should not** be called from the main thread.
             *
             * The intended flow is: submitFrameForROIs() is called from a background thread, passing the
             * latest available preview frame. While it is performing computations, any subsequent preview
             * frames arriving from the system camera API should be dropped. Attempts to call
             * submitFrameForROIs() while it is being called from another thread will fail.
             *
             * Once submitFrameForROIs() returns, it should be called again as soon as possible with the most
             * recent available preview frame. One way to implement this is to dedicate a background thread
             * to the task, calling submitFrameForROIs() in a loop.
             *
             * Note that this function should not be called synchronously with getROIArray(). The latency
             * implied by the expensiveness of this function does not lead to a high-quality user experience 
             * if it's synchronised with the display of ROI positions.
             *
             * \param colourPreviewFrame A handle to a colour preview frame.
             * \param hand The hand which this image is of.
             * \param lux Ambient lighting conditions.
             */
            SubmitFrameResult submitFrameForROIs(ImageHandle colourPreviewFrame, const ImagedHand hand, const Lux lux = Lux::LOW);
            
            /// Retrieves a set of ROIs and an ROI quality score
            /**
             * This function is used to retrieve a set of ROIs to be used to draw previews onto the display.
             * When called frequently and regularly (at >= 30 FPS), it provides a smoothly animated stream of ROI
             * positions suitable for display to the user.
             *
             * This function is guaranteed to be cheap, and so can be called frequently. The recommended
             * usage is to call in sync with the display's refresh rate (e.g. by using
             * [CADisplayLink](https://developer.apple.com/reference/quartzcore/cadisplaylink ) on iOS.)
             * Otherwise, call at regular intervals (e.g. 33.3ms to get ~30 FPS)
             *
             * This function should not be called synchronously with submitFrameForROIs(). There is logic
             * inside getROIArray() to process the raw ROI positions which are derived from frames supplied
             * to submitFrameForROIs(). This processing compensates for the latency created by
             * submitFrameForROIs(), and generates a stream of ROI positions suitable for display. Calling in
             * sync with submitFrameForROIs() defeats all that latency-hiding.
             *
             * \param[out] array Array which will receive the ROI values. In the same format as FourF::Interface::GetROIs()
             * \param[out] score ROI quality score. May not actually produce very useful values here.
             * \returns false if an error occurred, true otherwise
             * \sa FourF::Interface::GetROIs()
             */
            bool getROIArray(int* array, int& score);
            
            /// Possible responses to shouldTakePicture()
            enum class ShouldTakePictureAnswer : int
            {
                No = 0, OutOfFocus, Yes, RoiTooBig, RoiTooSmall, FingersTooFarApart, FingersHigh, FingersLow, FingersFarLeft, FingersFarRight, ImageRequestedWaiting
                // Do not renumber or reorder
            };
            
            /// Provides guidance on if the ROIs are in a valid position
            /**
             *  The app is recommended to provide feedback to the user based on the value returned by this function. For example, if the return value is ShouldTakePictureAnswer::RoiTooSmall, tell the user their hand is too far away. Will never recommend to take a picture.
             */
            ShouldTakePictureAnswer getROIstatus();
                
            /// Provides guidance on if now is an appropriate moment to take a full-size flash photo.
            /**
             *  The app is recommended to provide feedback to the user based on the value returned by this function. For example, if the return value is ShouldTakePictureAnswer::RoiTooSmall, tell the user their hand is too far away. If the response is ShouldTakePictureAnswer::Yes, the app should proceed to taking the picture as soon as possible.
             */
            ShouldTakePictureAnswer shouldTakePicture();
            
            /// Resets the internal state of the ROI detection system.
            /**
             *  This should be used if there's a discontinuity in the previews that system ought to be aware of; for example, if the preview window has been off-screen without being destroyed.
             */
            void reset();
                
            // Utilities only used in JNIInterface.
            /// Get the preview frame height set in FourF::Interface::init(). Only used by JNIInterface.
            int getFrameHeight();
            /// Get the preview frame width set in FourF::Interface::init(). Only used by JNIInterface.
            int getFrameWidth();
                
        } // namespace ROI
            
        /// Generic output operator template
        template<typename T, typename = typename std::enable_if<std::is_same<std::string, decltype(to_string(std::declval<T>()))>::value>::type>
        inline std::ostream& operator<<(std::ostream& o, const T t)
        {
            return o << to_string(t);
        }
            
        /// Deprecated interface methods and structures.
        namespace Deprecated{
            
            /// Deprecated initialisation settings for the 4F library.
            struct initSettings001
            {
                // Paths to fingertip cascades are found in repository_root/cascade_detectors
                /// Path to fingertip cascade file
                const std::string& tipCascadePathA;
                /// Path to second fingertip cascade file
                const std::string& tipCascadePathB;
                /// Path to 3rd fingertip cascade file
                const std::string& tipCascadePathC;
                /// Path to 4th fingertip cascade file
                const std::string& tipCascadePathD;
                /// Path to hand cascade file
                const std::string& handCascadePath;
                /// Width of preview frames
                const int width;
                /// Height of preview frames.
                const int height;
            };
            
            #pragma GCC diagnostic push
            #pragma GCC diagnostic ignored "-Wpadded"
            /// Deprecated initialisation settings for the 4F library.
            struct InitSettings002
            {
                // Paths to fingertip cascades are found in repository_root/cascade_detectors
                /// Path to fingertip cascade file
                const std::string& tipCascadePathA;
                /// Path to second fingertip cascade file
                const std::string& tipCascadePathB;
                /// Path to 3rd fingertip cascade file
                const std::string& tipCascadePathC;
                /// Path to 4th fingertip cascade file
                const std::string& tipCascadePathD;
                /// Path to hand cascade file
                const std::string& handCascadePath;
                /// Width of preview frames
                int width;
                /// Height of preview frames.
                int height;
                /// Horizontal field of view
                float horz_fov;
                /// Distance hand should be from camera (cm)
                float hand_dist;
            };
            #pragma GCC diagnostic pop
            
            /// A deprecated version of FourF::Interface::init()
            FFID_DEPRECATED("Upgrade to passing InitSettings003")
            void init(const initSettings001& settings);
            
            /// A deprecated version of FourF::Interface::init()
            FFID_DEPRECATED("Upgrade to passing InitSettings003")
            void init(const InitSettings002& settings);
            
            /// DEPRECATED getMatch, matches the passed ImageHandle against a gallery of enrolled fingerprint vectors.
            /**
             \deprecated Use the version of getMatch() which returns a GetMatchResult
             Returns true if it's a match, false otherwise.
             \param img The verify image.
             \param[in] rois ROI array retrieved from GetROIs()
             \param[in] galleries An array of fingerprint vectors captured at enrol time using GetVector() (typically 3)
             \param[in] progress A callback which reports units of progress. At time of writing, there are 20 progress units.
             \param[out] raw_score The raw matching score. May be negative in error cases.
             \param[in] securityLevel Configurable level of match security. -1 uses the currently configured default setting.
             **/
            FFID_DEPRECATED("Use the version of getMatch() which returns a GetMatchResult")
            inline bool getMatch(ImageHandle img, const int rois[16], const std::vector<FourF::Vector>& galleries, FourF::Progress progress, int& raw_score, int securityLevel = -1)
            {
                bool result;
                GetMatchResult match_return = getMatch(img, rois, galleries, progress, result, raw_score, securityLevel);
                return (match_return == GetMatchResult::SUCCESS) && result;
            }
            
            /// DEPRECATED getMatch, matches the passed vector to be verified against a gallery of enrolled fingerprint vectors.
            /**
             \deprecated Use the version of getMatch() which returns a GetMatchResult
             Returns true if it's a match, false otherwise.
             \param v The vector to be verified, captured using GetVector()
             \param[in] galleries An array of fingerprint vectors captured at enrol time using GetVector() (typically 3)
             \param[in] progress A callback which reports units of progress.
             \param[out] raw_score The raw matching score. May be negative in error cases.
             \param[in] securityLevel Configurable level of match security. -1 uses the currently configured default setting.
             **/
            FFID_DEPRECATED("Use the version of getMatch() which returns a GetMatchResult")
            bool getMatch(FourF::Vector v, const std::vector<FourF::Vector>& galleries, FourF::Progress progress, int& raw_score, int securityLevel = -1);
            
            /// DEPRECATED: getMatchSerial, matches the passed auth vector against a set of three enrol vectors.
            /**
             \deprecated In most circumstances, you should use getMatch() instead.
             Does this serially rather than in parallel across multiple threads, and performs no image processing.
             \param verify A vector to be tested to see if it matches the galleries set of templates.
             \param galleries A set of vectors all from the same hand of a given individual
             \param[out] match Set to true if the verify vector matches the galleries
             \param[out] raw_score The raw score generated by the matching process.
             **/
            FFID_DEPRECATED("In most circumstances, you should use getMatch() instead.")
            GetMatchResult getMatchSerial(const FourF::Vector& verify, const std::vector<FourF::Vector>& galleries, bool& match, int& raw_score);
            
        } // namespace Deprecated
        
        /// A deprecated initSettings. Use FourF::Interface::initSettings003 instead.
        using initSettings001 = Deprecated::initSettings001;
            
        /// A deprecated initSettings. Use FourF::Interface::initSettings003 instead.
        using InitSettings002 = Deprecated::InitSettings002;
            
        using Deprecated::init;
        using Deprecated::getMatch;
        using Deprecated::getMatchSerial;
            
    } // namespace Interface
            
    // Output operator
    using FourF::Interface::operator<<;

    /// Internal-use declarations.
    namespace Internal
    {
        class FfidMinutiae;
        template<typename TMinutiae>
        class FfidVector;
        
        /// For internal use only.
        using InternalVec = Internal::FfidVector<Internal::FfidMinutiae>;
        
        /// For internal use only.
        std::shared_ptr<InternalVec> getInternalVector(const FourF::Vector& v);
    }
    class FfidAnsiNistItl;
    
    FFID_INTERFACE_PUSH_DIAGNOSTIC
    FFID_INTERFACE_IGNORE(-Wpadded)
            
    /// Container for 4F template information.
    /**
     * Opaque container for 4F templates. 
     * These can be converted to and from byte arrays using the save() and load() functions.
     * A Vector can be filled in with a valid template by using FourF::Interface::getVector()
     */
    class Vector
    {
    public:
        
        /// Construct new blank Vector instance.
        Vector();
        
        /// Obtain the size in bytes that will be necessary to save this Vector using save().
        /** 
         * \deprecated Use the save() method which returns a std::vector<char>.
         * \return The length of array (i.e. number of bytes) needed for the array passed to save().
         */
        FFID_DEPRECATED("Use the save() method which returns a std::vector<char>.")
        size_t saveSize() const;
        
        /// Save the Vector to a byte array.
        /**
         * \deprecated Use the save() method which returns a std::vector<char>.
         * \param array An array which is at least as long as the return value of saveSize()
         * \param array_length Length of array.
         * \return Number of bytes written.
         */
        FFID_DEPRECATED("Use the save() method which returns a std::vector<char>.")
        size_t save(void* array, const size_t array_length) const;
        
        /// Save the Vector to a byte array.
        /**
         * Capable of saving the Vector using the new Bundle format; will only do so if the hand type was set with
         * getVector(ImageHandle img, ImagedHand hand, const int rois[16], FourF::Vector& minutiae).
         * \return Vector of bytes containing the saved Vector.
         */
        std::vector<char> save() const;
        
        /// Saves a gallery of multiple Vectors.
        /**
         * Used to save multiple Vectors taken of a single subject into a single Bundle file.
         */
        static std::vector<char> saveGallery(const std::vector<Vector>& vectors);
        
        /// Loads a gallery of multiple Vectors from a std::vector<char>.
        /**
         * Supports both old-style galleries and new Bundles.
         */
        static std::vector<Vector> loadGallery(const std::vector<char>& array);
        
        /// Loads a gallery of multiple Vectors from a char array of a given length.
        /**
         * Supports both old-style galleries and new Bundles.
         */
        static std::vector<Vector> loadGallery(const char* array_ptr, size_t array_size);
        
        /// Load a template into this Vector from array argument.
        /**
         * This is typically used to load a Vector previously saved using save()
         * \param array An array to load from.
         * \return true if load successful, false otherwise.
         */
        bool load(const std::vector<char>& array);
        
        /// Load a template into this Vector from array argument.
        /**
         * This is typically used to load a Vector previously saved using save()
         * \param array An array to load from.
         * \param array_size The size of array in bytes.
         * \return true if load successful, false otherwise.
         */
        bool load(const void* array, size_t array_size);
        
        /// Load a template into this Vector from FfidMinutiae
        bool load(std::vector<std::shared_ptr<Internal::FfidMinutiae>> ffid_minutiaes);
        
        /// Determines if this Vector contains a valid template.
        bool isValid() const;
        
        /// Gets which hand was imaged to generate this vector.
        inline FourF::Interface::ImagedHand getHand() const
        {
            return hand;
        }

    private:
        std::shared_ptr<Internal::InternalVec> internal_vector;
        FourF::Interface::ImagedHand hand = FourF::Interface::ImagedHand::Invalid;

        friend std::shared_ptr<Internal::InternalVec> FourF::Internal::getInternalVector(const FourF::Vector& v);
        friend GetVectorResult FourF::Interface::getVector(Interface::ImageHandle img, const int rois[16], FourF::Vector& minutiae);
        friend GetVectorResult FourF::Interface::getVector(Interface::ImageHandle img, Interface::ImagedHand hand, const int rois[16], FourF::Vector& minutiae);
        friend GetVectorResult FourF::Interface::getNist(Interface::ImageHandle h, FourF::Interface::ImagedHand hand, const int rois[16], FourF::Nist& nist);
    };
            
    class Nist
    {
    public:
      /// Construct new nist instance
      Nist();
      
      /// build nist data
      /**
       * Build ansi_nist from img and vector
       * \param[in] vec minutiae
       * \param[in] imgs enhanced image of finger
       * \param[in] hand left or right hand
       * \param[in] ppm capture image ppm
       * \return 0 if success
       */
      int load(Vector vec, std::vector<cv::Mat> imgs, Interface::ImagedHand hand, double ppm);
      
      /// load nist data
      /**
       * Load ansi_nist from buffer
       * \param[in] ibufp input buffer
       * \param[in] isizep input buffer size
       * \return 0 if success
       */
      int load(char *ibufp, const size_t isizep);
        
      /// get image, vec from nist file
      /**
       * Load ansi_nist from buffer
       * \param[in] ifilename input filename
       * \param[out] vec finger print vector
       * \param[out] imgs finger print enhanced image
       * \param[out] hand left or right hand
       * \return 0 if success
       */
      int get(const std::string ifilename, Vector& vec, std::vector<cv::Mat>& imgs, Interface::ImagedHand &hand);
      
      /// get image, vec from nist buffer
      /**
       * Load ansi_nist from buffer
       * \param[in] ibufp input buffer
       * \param[in] isizep input buffer size
       * \param[out] vec finger print vector
       * \param[out] imgs finger print enhanced image
       * \param[out] hand left or right hand
       * \return 0 if success
       */
      int get(char *ibufp, const size_t isizep, Vector& vec, std::vector<cv::Mat>& imgs, Interface::ImagedHand &hand);
      /// get nist data
      /**
       * Save ansi_nist data to buffer
       * \param obufp output buffer
       * \param osizep output buffer size
       * \return 0 if success
       */
      int save(char **obufp, size_t &osizep) const;
      
      /// add info to nist field-2
      /**
       * Add information
       * \param first_name personal information : first_name
       * \param last_name personal information : last_name
       * \param dob personal information : dob
       * \return 0 if success
       */
      int update_info(const std::string first_name, const std::string last_name, const std::string dob);
      
      /// get info
      /**
       * Get info from nist type-2
       * \param first_name personal information : first_name
       * \param last_name personal information : last_name
       * \param dob personal information : dob
       * \return 0 if success
       */
      int get_info(std::string& first_name, std::string& last_name, std::string& dob);

    private:
      std::shared_ptr<FfidAnsiNistItl> ansi_nist;
    };
          
    FFID_INTERFACE_POP_DIAGNOSTIC
            
} // namespace FourF
    
// Temp backwards compat:
        
FFID_DEPRECATED("Use FourF::Lux")
/// \deprecated Use FourF::Lux
typedef FourF::Lux Lux;
    
FFID_DEPRECATED("Use FourF::GetVectorResult")
/// \deprecated Use FourF::GetVectorResult
typedef FourF::GetVectorResult GetVectorResult;
        
FFID_DEPRECATED("Use FourF::GetImageInfoResult")
/// \deprecated Use FourF::GetImageInfoResult
typedef FourF::GetImageInfoResult GetImageInfoResult;
        
#pragma GCC visibility pop
#endif
